package com.cs.ess.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cs.ess.Activites.AnnoucementsActivity;
import com.cs.ess.Activites.ApprovalActivity;
import com.cs.ess.Activites.EmepolyReportsActivity;
import com.cs.ess.Activites.ExpensesActivity;
import com.cs.ess.Activites.MainActivity;
import com.cs.ess.Activites.PersonalActivity;
import com.cs.ess.Activites.RequestsActivity;
import com.cs.ess.JSONParser;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class DashBoardFragment extends Fragment {

    RelativeLayout personalinfo_layout, requst_layout, report_layout, expenses_layout, approvals_layout, announcement_layout;
    ImageView menu_btn;
    TextView userName, pos;
    View rootView;
    String userId;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.dashboard_activity, container, false);

        personalinfo_layout = (RelativeLayout) rootView.findViewById(R.id.personalinfo_layout);
        requst_layout = (RelativeLayout) rootView.findViewById(R.id.requst_layout);
        report_layout = (RelativeLayout) rootView.findViewById(R.id.report_layout);
        expenses_layout = (RelativeLayout) rootView.findViewById(R.id.expenses_layout);
        approvals_layout = (RelativeLayout) rootView.findViewById(R.id.approvals_layout);
        announcement_layout = (RelativeLayout) rootView.findViewById(R.id.announcement_layout);
        menu_btn = (ImageView) rootView.findViewById(R.id.menu_btn);
        userName = (TextView) rootView.findViewById(R.id.username);
        pos = (TextView) rootView.findViewById(R.id.pos);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");


        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        personalinfo_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PersonalActivity.class);
                startActivity(intent);
            }
        });
        requst_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RequestsActivity.class);
                startActivity(intent);
            }
        });
        approvals_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ApprovalActivity.class);
                startActivity(intent);
            }
        });
        announcement_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AnnoucementsActivity.class);
                startActivity(intent);
            }
        });
        report_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EmepolyReportsActivity.class);
                startActivity(intent);
            }
        });

        expenses_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ExpensesActivity.class);
                startActivity(intent);
            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

            new PersonalApi().execute(Constants.getpersonDetails + "?UserId=" + userId);

        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

        setTypeface();
        return rootView;
    }

    private void setTypeface() {

        ((TextView) rootView.findViewById(R.id.personalinfotext)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) rootView.findViewById(R.id.requesttext)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) rootView.findViewById(R.id.reporttext)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) rootView.findViewById(R.id.expensestext)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) rootView.findViewById(R.id.approvalstext)).setTypeface(Constants.getarbooldTypeFace(getActivity()));
        ((TextView) rootView.findViewById(R.id.annoucementtext)).setTypeface(Constants.getarbooldTypeFace(getActivity()));


    }

    public class PersonalApi extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus, response;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getActivity(), "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i(TAG, "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetPersonalInfo");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    String personalNumber = jo2.getString("PersonalNumber");
                                    workerName = jo2.getString("WorkerName");
                                    String workerNameAR = jo2.getString("WorkerNameAR");
                                    String job = jo2.getString("Job");
                                    description = jo2.getString("Description");
                                    String empstartdate = jo2.getString("Empstartdate");
                                }

                                Log.i(TAG, "onPostExecute: " + workerName);

                                userPrefsEditor.putString("Empname", workerName);
                                userPrefsEditor.putString("Emppos", description);
                                userPrefsEditor.commit();

                                userName.setText("" + workerName);
                                pos.setText("" + description);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(getActivity(), "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
