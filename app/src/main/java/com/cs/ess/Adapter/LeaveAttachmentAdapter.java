package com.cs.ess.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Activites.LeaveAttachmentActivity;
import com.cs.ess.Activites.LeaveRequestActivity;
import com.cs.ess.Models.AttachmentJSONlist;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.cs.ess.Activites.LeaveAttachmentActivity.image_size;
import static com.cs.ess.Activites.LeaveAttachmentActivity.mstr_comment;

public class LeaveAttachmentAdapter extends RecyclerView.Adapter<LeaveAttachmentAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<AttachmentJSONlist> attachmentJSONlists = new ArrayList<>();
    private ArrayList<Drawable> attachment = new ArrayList<>();
    private ArrayList<String> file_name = new ArrayList<>();
    private ArrayList<String> file_type = new ArrayList<>();
    int pos = 0;
    String userid;
    boolean medit = false;

    public LeaveAttachmentAdapter(Context context, ArrayList<AttachmentJSONlist> attachmentJSONlists, ArrayList<Drawable> attachment, ArrayList<String> file_name, ArrayList<String> filetype, Activity activity, String userid) {
        this.context = context;
        this.activity = activity;
        this.attachmentJSONlists = attachmentJSONlists;
        this.attachment = attachment;
        this.userid = userid;
        this.file_name = file_name;
        this.file_type = filetype;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_attachment_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.attactment_img.setImageDrawable(attachment.get(position));
        if (file_name.get(position).equals("")) {

            holder.file_name_layout.setVisibility(View.GONE);
            holder.remove.setVisibility(View.GONE);
            holder.file_type.setVisibility(View.GONE);

        } else {

            holder.file_name_layout.setVisibility(View.VISIBLE);
            holder.remove.setVisibility(View.VISIBLE);
            holder.file_name.setText("" + file_name.get(position));
        }

        holder.file_type.setVisibility(View.GONE);

        if (file_type.get(position).equals("")) {

            holder.file_type.setVisibility(View.GONE);

        } else if (file_type.get(position).equals("png")) {

            holder.file_type.setVisibility(View.GONE);

        } else if (file_type.get(position).equals("jpg")){

            holder.file_type.setVisibility(View.GONE);

        } else {

            holder.file_type.setVisibility(View.VISIBLE);
            holder.file_type.setText("." + file_type.get(position) + " File");

        }

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("TAG", "attachmentJSONlists before removing: " + attachmentJSONlists.size());

                Log.i("TAG", "image_size1: " + image_size);
                image_size = image_size - attachmentJSONlists.get(position - 1).getImage_size();
                Log.i("TAG", "image_size2: " + image_size);
                attachmentJSONlists.remove((position - 1));
                file_name.remove(position);
                attachment.remove(position);
                file_type.remove(position);


                Log.i("TAG", "attachmentJSONlists after removing: " + attachmentJSONlists.size());

                notifyDataSetChanged();


            }
        });

    }

    @Override
    public int getItemCount() {
        return file_name.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView file_name, file_type;
        ImageView attactment_img, remove;
        RelativeLayout file_name_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            file_name = (TextView) itemView.findViewById(R.id.file_name);
            attactment_img = (ImageView) itemView.findViewById(R.id.attactment_img);
            remove = (ImageView) itemView.findViewById(R.id.remove);
            file_name_layout = (RelativeLayout) itemView.findViewById(R.id.layout);
            file_type = (TextView) itemView.findViewById(R.id.file_type);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (getAdapterPosition() == 0) {

                        AlertDialog customDialog = null;
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = activity.getLayoutInflater();
                        int layout = R.layout.attachment_alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        EditText comment = (EditText) dialogView.findViewById(R.id.comments);
                        Button document = (Button) dialogView.findViewById(R.id.document);
                        Button gallery = (Button) dialogView.findViewById(R.id.gallery);
                        ImageView cancel = (ImageView) dialogView.findViewById(R.id.cancel);


                        customDialog = dialogBuilder.create();
                        customDialog.show();

                        final AlertDialog finalCustomDialog = customDialog;
                        document.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                String str_comment = comment.getText().toString();
                                mstr_comment = str_comment;

                                if (str_comment.length() == 0) {

                                    Constants.showOneButtonAlertDialog("Please enter comment", "ESS", "ok", activity);
                                    comment.requestFocus();

                                } else {
                                    finalCustomDialog.dismiss();
                                    if (context instanceof LeaveAttachmentActivity) {
                                        ((LeaveAttachmentActivity) context).initView1();
                                    }

                                }

                            }
                        });

                        gallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                String str_comment = comment.getText().toString();
                                mstr_comment = str_comment;


                                if (str_comment.length() == 0) {

                                    Constants.showOneButtonAlertDialog("Please enter comment", "ESS", "ok", activity);
                                    comment.requestFocus();

                                } else {
                                    finalCustomDialog.dismiss();
                                    if (context instanceof LeaveAttachmentActivity) {
                                        ((LeaveAttachmentActivity) context).initView2();
                                    }

                                }

                            }
                        });

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                finalCustomDialog.dismiss();

                            }
                        });

                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = activity.getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth * 0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    }

                }
            });

        }
    }

}