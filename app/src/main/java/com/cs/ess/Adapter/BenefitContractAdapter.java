package com.cs.ess.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Models.BenefitContractResponse;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class BenefitContractAdapter extends RecyclerView.Adapter<BenefitContractAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private List<BenefitContractResponse> benenfit_list = new ArrayList<>();
    int pos = 0;

    public BenefitContractAdapter(Context context, List<BenefitContractResponse> benenfit_list) {
        this.context = context;
        this.activity = activity;
        this.benenfit_list = benenfit_list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.benefit_contract_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.allowancededuction.setText("" + benenfit_list.get(position).getAllowancededuction());
        holder.percentage.setText("" + benenfit_list.get(position).getPERCENT());

        double basicsalary = benenfit_list.get(position).getAmount();

        holder.amount.setText("" + Constants.priceFormat1.format(basicsalary));
        holder.deserved_every.setText("" + benenfit_list.get(position).getDESERVEDEVERY());

    }

    @Override
    public int getItemCount() {
        return benenfit_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView allowancededuction, percentage, amount, deserved_every;

        public MyViewHolder(View itemView) {
            super(itemView);
            allowancededuction = (TextView) itemView.findViewById(R.id.allowancededuction);
            percentage = (TextView) itemView.findViewById(R.id.percentage);
            amount = (TextView) itemView.findViewById(R.id.amount);
            deserved_every = (TextView) itemView.findViewById(R.id.deserved_every);


//            storelayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(productArrayList.size()> 0) {
//                        Intent intent = new Intent(context, ProductStoresActivityStep1.class);
//                        intent.putExtra("stores", productArrayList);
//                        intent.putExtra("pos",getAdapterPosition());
//                        context.startActivity(intent);
//                    }
//                    else {
//                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });

        }
    }
}
