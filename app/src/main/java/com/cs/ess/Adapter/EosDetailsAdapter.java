package com.cs.ess.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Activites.EOSActivity1;
import com.cs.ess.Activites.EOSRequestActivity2;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.EOSDetailsResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class EosDetailsAdapter extends RecyclerView.Adapter<EosDetailsAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<EOSDetailsResponce> retundetailsarray = new ArrayList<>();
    int pos, submit_pos;
    String userid;
    boolean medit = false;

    public EosDetailsAdapter(Context context, ArrayList<EOSDetailsResponce> benenfit_list , Activity activity, String userid) {
        this.context = context;
        this.activity = activity;
        this.retundetailsarray = benenfit_list;
        this.userid = userid;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_eos_details, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.id.setText("ID : " + retundetailsarray.get(position).getDAPEOSREQID());

        if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("0")) {
            holder.workflowstatus.setText("Not submitted");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("1")) {
            holder.workflowstatus.setText("Submitted");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("2")) {
            holder.workflowstatus.setText("Pending approval");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("3")) {
            holder.workflowstatus.setText("Pending complete");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("4")) {
            holder.workflowstatus.setText("Approved");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("5")) {
            holder.workflowstatus.setText("Rejected");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("6")) {
            holder.workflowstatus.setText("Change requested");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("7")) {
            holder.workflowstatus.setText("Completed");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("8")) {
            holder.workflowstatus.setText("Completed");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("9")) {
            holder.workflowstatus.setText("Pending cancellation");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("10")) {
            holder.workflowstatus.setText("Canceled");
        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("11")) {
            holder.workflowstatus.setText("Rejected");
        }


        String date = retundetailsarray.get(position).getLASTWORKINGDATE();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy",Locale.US);
        try {
            Date datetime = sdf.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.retundate.setText(""+": "+date);

        if (retundetailsarray.get(position).getEOSTRANSTYPE().equals("1")){
            holder.type.setText(": End Of Contract");
        }
        else {
            holder.type.setText(": Resignation");
        }

// String date1 = retundetailsarray.get(position).getRETURNDATE();
//        SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
//        SimpleDateFormat sdf3 = new SimpleDateFormat("dd MMM yyyy",Locale.US);
//        try {
//            Date datetime = sdf.parse(date1);
//            type = sdf3.format(datetime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        holder.retundate.setText(""+": "+type);



        if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("0")) {

            holder.delete.setVisibility(View.VISIBLE);
            holder.submit.setVisibility(View.VISIBLE);

        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("11")) {

            holder.submit.setVisibility(View.VISIBLE);

        } else if (retundetailsarray.get(position).getWFDUMMIESWORKFLOWSTATENUM().equals("5")) {

            holder.submit.setVisibility(View.VISIBLE);

        } else {

            holder.delete.setVisibility(View.GONE);
            holder.submit.setVisibility(View.GONE);

        }

        if (position == Constants.meos_submit){

            holder.delete.setVisibility(View.GONE);
            holder.submit.setVisibility(View.GONE);

        }

        holder.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                if (bsubmit) {

                String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                    String service_url = Constants.geteosSubmit + "?dapEOSReqId=" + retundetailsarray.get(position).getDAPEOSREQID() + "&UserId=" + userid + "&Note=";
                    submit_pos = position;
                    String main_url = service_url.replaceAll(" ", "%20");
                    new SubmitRequestApi().execute(main_url);
                    Log.i("TAG", "onClick: " + main_url);
                } else {
                    Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }

//                } else {
//                    Constants.showOneButtonAlertDialog("Your request has been submited", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);
//                }
            }

        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                LayoutInflater inflater = activity.getLayoutInflater();
                int layout;
                layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                title.setText(context.getResources().getString(R.string.app_name));
                yes.setText("yes");
                no.setText("no");
                desc.setText("Do you want to delete");

                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                            pos = position;
                            new DeleteLeaveRequestApi().execute(Constants.geteosDelete + "?dapEOSReqId=" + retundetailsarray.get(position).getDAPEOSREQID() + "&UserId=" + userid );
                        } else {
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }
                        finalCustomDialog.dismiss();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finalCustomDialog.dismiss();

                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);


            }
        });

    }

    @Override
    public int getItemCount() {
        return retundetailsarray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id, workflowstatus, type, retundate;
        Button delete, submit;

        public MyViewHolder(View itemView) {
            super(itemView);
            id = (TextView) itemView.findViewById(R.id.id);
            workflowstatus = (TextView) itemView.findViewById(R.id.status);
            type = (TextView) itemView.findViewById(R.id.transtype);
            retundate = (TextView) itemView.findViewById(R.id.lastdate);

            delete = (Button) itemView.findViewById(R.id.delete);
            submit = (Button) itemView.findViewById(R.id.sumbit);

            ((TextView)itemView.findViewById(R.id.id)).setTypeface(Constants.getarbooldTypeFace(context));
            ((TextView)itemView.findViewById(R.id.status)).setTypeface(Constants.getarbooldTypeFace(context));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (retundetailsarray.size() > 0) {

                        if (retundetailsarray.get(getAdapterPosition()).getWFDUMMIESWORKFLOWSTATENUM().equals("0")  || retundetailsarray.get(getAdapterPosition()).getWFDUMMIESWORKFLOWSTATENUM().equals("11")) {

                            medit = true;

                        } else {

                            medit = false;

                        }

                        Intent intent = new Intent(context, EOSRequestActivity2.class);
                        intent.putExtra("stores", retundetailsarray);
                        intent.putExtra("pos", getAdapterPosition());
                        intent.putExtra("medit", medit);
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, "no stores available", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    public class DeleteLeaveRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    retundetailsarray.remove(pos);
                                    notifyDataSetChanged();
                                    Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
//                                    String networkStatus = NetworkUtil.getConnectivityStatusString(context);
//                                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
////                                        new LoanRequestApi().execute(Constants.getLoanview + "?UserId=" + userid);
//                                    } else {
//                                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                                    }
                                }


                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);
                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = activity.getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

                                    no.setVisibility(View.GONE);

                                    title.setText(context.getResources().getString(R.string.app_name));
                                    yes.setText(context.getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.meos_submit = submit_pos;
                                            finalCustomDialog.dismiss();
                                            if (context instanceof EOSActivity1) {
                                                ((EOSActivity1) context).initView();
                                            }
//                                            loandetailsarray.get(submit_pos).setmSubmit(false);
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = activity.getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = activity.getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(context.getResources().getString(R.string.app_name));
                                    yes.setText(context.getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = activity.getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
