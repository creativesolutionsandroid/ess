package com.cs.ess.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Activites.LeaveRequestActivity;
import com.cs.ess.Activites.LeaveRequestActivity1;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.LeaveBalanceResponse;
import com.cs.ess.Models.LeaveRequestGetResponse;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class LeaveRequestAdapter extends RecyclerView.Adapter<LeaveRequestAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<LeaveRequestGetResponse> couponArrayList = new ArrayList<>();
    int pos, submit_pos;
    String userid ;
    boolean medit = false;
    LeaveRequestAdapter leaveRequestAdapter;
//    ArrayList<LeaveRequestGetResponse> leaveRequestGetResponses = new ArrayList<>();


    public LeaveRequestAdapter(Context context, ArrayList<LeaveRequestGetResponse> storeArrayList, Activity activity, String userid) {
        this.context = context;
        this.activity = activity;
        this.couponArrayList = storeArrayList;
        this.userid = userid;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_request_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.leave_id.setText("ID : " + couponArrayList.get(position).getLEAVEREQUESTID());

        if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("0")) {
            holder.workflowstatus.setText("Not submitted");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("1")) {
            holder.workflowstatus.setText("Submitted");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("2")) {
            holder.workflowstatus.setText("Pending approval");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("3")) {
            holder.workflowstatus.setText("Pending complete");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("4")) {
            holder.workflowstatus.setText("Approved");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("5")) {
            holder.workflowstatus.setText("Rejected");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("6")) {
            holder.workflowstatus.setText("Change requested");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("7")) {
            holder.workflowstatus.setText("Completed");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("8")) {
            holder.workflowstatus.setText("Completed");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("9")) {
            holder.workflowstatus.setText("Pending cancellation");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("10")) {
            holder.workflowstatus.setText("Canceled");
        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("11")) {
            holder.workflowstatus.setText("Rejected");
        }

        holder.leave_type.setText(": " + couponArrayList.get(position).getLEAVEID());
        holder.leave_balance.setText(": " + couponArrayList.get(position).getLEAVEBALANCE());

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        String date = couponArrayList.get(position).getFROMDATE();
        String date1 = couponArrayList.get(position).getTODATE();

        try {
            Date datetime = sdf.parse(date);
            Date datetime1 = sdf.parse(date1);
            date = sdf1.format(datetime);
            date1 = sdf1.format(datetime1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.from_date.setText(": " + date);
        holder.to_date.setText(": " + date1);

        if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("0")) {

            holder.delete.setVisibility(View.VISIBLE);
            holder.submit.setVisibility(View.VISIBLE);

        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("11")) {

            holder.submit.setVisibility(View.VISIBLE);

        } else if (couponArrayList.get(position).getWORKFLOWSTATENUM().equals("5")) {

            holder.submit.setVisibility(View.VISIBLE);

        } else {

            holder.delete.setVisibility(View.GONE);
            holder.submit.setVisibility(View.GONE);

        }

        if (position == Constants.mleave_submit){

            holder.delete.setVisibility(View.GONE);
            holder.submit.setVisibility(View.GONE);

        }

        holder.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                if (bsubmit) {

                String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                    String service_url = Constants.getSubmit + "?TransactionID=" + couponArrayList.get(position).getLEAVEREQUESTID() + "&UserId=" + userid + "&Note=";
                    String main_url = service_url.replaceAll(" ", "%20");
                    submit_pos = position;
                    new SubmitRequestApi().execute(main_url);
                    Log.i("TAG", "onClick: " + main_url);
                } else {
                    Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                }

//                } else {
//                    Constants.showOneButtonAlertDialog("Your request has been submited", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);
//                }
            }

        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = activity.getLayoutInflater();
                int layout;
//                if (language.equalsIgnoreCase("En")) {
                layout = R.layout.alert_dialog;
//                } else {
//                    layout = R.layout.alert_dialog_arabic;
//                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

//                no.setVisibility(View.GONE);
//                vert.setVisibility(View.GONE);

//                if (language.equalsIgnoreCase("En")) {
                title.setText(context.getResources().getString(R.string.app_name));
                yes.setText("yes");
                no.setText("no");
                desc.setText("Do you want to delete");
//                } else {
//                    title.setText(getResources().getString(R.string.app_name_ar));
//                    yes.setText(getResources().getString(R.string.yes_ar));
//                    no.setText(getResources().getString(R.string.no_ar));
//                    desc.setText("هل ترغب فعلاً بألغاء طلبك؟");
//                }

                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            pos = position;
                            new DeleteLeaveRequestApi().execute(Constants.getDelete + "?TransactionID=" + couponArrayList.get(position).getLEAVEREQUESTID() + "&UserId=" + userid + "&Note=");
                        } else {
//                            if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                            }
                        }
                        finalCustomDialog.dismiss();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finalCustomDialog.dismiss();

                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);


                Log.i(TAG, "onClick: " + Constants.getDelete + "?TransactionID=" + couponArrayList.get(position).getLEAVEREQUESTID() + "&UserId=" + userid + "&Note=");

            }
        });

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "couponlistsize: " + couponArrayList.size());
        return couponArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView leave_id, workflowstatus, from_date, to_date, leave_type, leave_balance;
        Button delete, submit;


        public MyViewHolder(View itemView) {
            super(itemView);
            leave_id = (TextView) itemView.findViewById(R.id.leave_id);
            workflowstatus = (TextView) itemView.findViewById(R.id.workflowstatus);
            from_date = (TextView) itemView.findViewById(R.id.from_date);
            to_date = (TextView) itemView.findViewById(R.id.to_date);
            leave_type = (TextView) itemView.findViewById(R.id.leave_type);
            leave_balance = (TextView) itemView.findViewById(R.id.leave_balance);
            delete = (Button) itemView.findViewById(R.id.delete);
            submit = (Button) itemView.findViewById(R.id.sumbit);

            ((TextView)itemView.findViewById(R.id.leave_id)).setTypeface(Constants.getarbooldTypeFace(context));
            ((TextView)itemView.findViewById(R.id.workflowstatus)).setTypeface(Constants.getarbooldTypeFace(context));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (couponArrayList.get(getAdapterPosition()).getWORKFLOWSTATENUM().equals("0")  || couponArrayList.get(getAdapterPosition()).getWORKFLOWSTATENUM().equals("11")) {

                        medit = true;

                    } else {

                        medit = false;

                    }

                    Intent intent = new Intent(context, LeaveRequestActivity.class);
                    intent.putExtra("list", couponArrayList);
                    intent.putExtra("pos", getAdapterPosition());
                    intent.putExtra("medit", medit);
                    context.startActivity(intent);

                }
            });
        }
    }

    public class DeleteLeaveRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    couponArrayList.remove(pos);
                                    notifyDataSetChanged();
                                    Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();

//                                    Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
//                                    String networkStatus = NetworkUtil.getConnectivityStatusString(context);
//                                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                                        new HRGeneralRequestApi().execute(Constants.getleaveget + "?UserId=" + userid);
//                                    } else {
//                                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                                    }
                                }


                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class LeaveRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            couponArrayList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");

                                JSONArray ja = jo1.getJSONArray("GETLeaveRequestDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    LeaveRequestGetResponse leaveRequestGetResponse = new LeaveRequestGetResponse();
                                    leaveRequestGetResponse.setLEAVEREQUESTID(jo2.getString("LEAVEREQUESTID"));
                                    leaveRequestGetResponse.setFROMDATE(jo2.getString("FROMDATE"));
                                    leaveRequestGetResponse.setTODATE(jo2.getString("TODATE"));
                                    leaveRequestGetResponse.setLEAVEID(jo2.getString("LEAVEID"));
                                    leaveRequestGetResponse.setLEAVEBALANCE(jo2.getString("LEAVEBALANCE"));
                                    leaveRequestGetResponse.setCONTACTINFODURINGLEAVE(jo2.getString("CONTACTINFODURINGLEAVE"));
                                    leaveRequestGetResponse.setPROVIDEVISA(jo2.getString("PROVIDEVISA"));
                                    leaveRequestGetResponse.setNOTES(jo2.getString("NOTES"));
                                    leaveRequestGetResponse.setALTERNATIVEEMPLOYEEPN(jo2.getString("ALTERNATIVEEMPLOYEEPN"));
                                    leaveRequestGetResponse.setALTERNATIVEEMPLOYEENAME(jo2.getString("ALTERNATIVEEMPLOYEENAME"));
                                    leaveRequestGetResponse.setWORKFLOWSTATE(jo2.getString("WORKFLOWSTATE"));
                                    leaveRequestGetResponse.setWORKERNAME(jo2.getString("WORKERNAME"));
                                    leaveRequestGetResponse.setDATAAREAID(jo2.getString("DATAAREAID"));

                                    couponArrayList.add(leaveRequestGetResponse);
                                    Log.i("TAG", "leaverequest: " + couponArrayList.get(i).getLEAVEREQUESTID());
                                }

                                notifyDataSetChanged();

                                for (int i = 0; i < couponArrayList.size(); i++) {
                                    Log.i("TAG", "leaverequest: " + couponArrayList.get(i).getLEAVEREQUESTID());
                                }

//                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(context, leaveRequestGetResponses, context);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = new ACProgressFlower.Builder(context)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(context, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);
                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = activity.getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

                                    no.setVisibility(View.GONE);

                                    title.setText(context.getResources().getString(R.string.app_name));
                                    yes.setText(context.getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.mleave_submit = submit_pos;
                                            finalCustomDialog.dismiss();
                                            if (context instanceof LeaveRequestActivity1) {
                                                ((LeaveRequestActivity1) context).initView();
                                            }
//                                            loandetailsarray.get(submit_pos).setmSubmit(false);
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = activity.getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = activity.getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(context.getResources().getString(R.string.app_name));
                                    yes.setText(context.getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = activity.getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(context, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}


