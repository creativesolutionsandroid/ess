package com.cs.ess.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.ess.Activites.BusinessRetunActivity2;
import com.cs.ess.Activites.HourlyLeaveRequestActivity2;
import com.cs.ess.Models.RetunTypeResponce;
import com.cs.ess.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RetunTypeAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    int QTY = 0;
    String TAG = "TAG";
    private ArrayList<RetunTypeResponce> cartArryLists;
    PopupWindow popupWindow;


    public RetunTypeAdapter(Context context, ArrayList<RetunTypeResponce> cartArryLists, PopupWindow popupWindow) {
        this.context = context;
        this.cartArryLists = cartArryLists;
        this.popupWindow = popupWindow;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.i(TAG, "getCount: " + cartArryLists.size());
        return cartArryLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView request_id, from_date, to_date;
        RelativeLayout branch_layout;
        View view;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.return_list_child, null);
            holder.request_id = (TextView) convertView.findViewById(R.id.request_id);
            holder.from_date = (TextView) convertView.findViewById(R.id.from_date);
            holder.to_date = (TextView) convertView.findViewById(R.id.to_date);
            holder.branch_layout = (RelativeLayout) convertView.findViewById(R.id.branch_layout);
            holder.view = (View) convertView.findViewById(R.id.view);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.request_id.setText(cartArryLists.get(position).getLEAVETRANSID());
//        Log.d(TAG, "getView: " + cartArryLists.get(position));

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        try {
            Date datetime = sdf.parse(cartArryLists.get(position).getFROMDATE());
            Date datetime1 = sdf.parse(cartArryLists.get(position).getTODATE());
            holder.from_date.setText("" + sdf1.format(datetime));
            holder.to_date.setText("" + sdf1.format(datetime1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (cartArryLists.size() == (position + 1)) {

            holder.view.setVisibility(View.GONE);

        } else {

            holder.view.setVisibility(View.VISIBLE);

        }

        holder.branch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                BusinessRetunActivity2.hidden_layout.setVisibility(View.GONE);
                BusinessRetunActivity2.alternate_pos = position;

                if (context instanceof BusinessRetunActivity2) {
                    ((BusinessRetunActivity2) context).initView();
                }

                popupWindow.dismiss();

            }
        });
        return convertView;
    }

}

