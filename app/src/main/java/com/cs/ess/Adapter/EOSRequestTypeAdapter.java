package com.cs.ess.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.ess.Activites.EOSRequestActivity2;
import com.cs.ess.Activites.LoanReaquestActivity2;
import com.cs.ess.R;

import java.util.ArrayList;

public class EOSRequestTypeAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    int QTY = 0;
    String TAG = "TAG";
    private ArrayList<String> cartArryLists;
    PopupWindow popupWindow;


    public EOSRequestTypeAdapter(Context context, ArrayList<String> cartArryLists, PopupWindow popupWindow) {
        this.context = context;
        this.cartArryLists = cartArryLists;
        this.popupWindow = popupWindow;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.i(TAG, "getCount: " + cartArryLists.size());
        return cartArryLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView branch_name;
        RelativeLayout branch_layout;
        View view;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.branch_list_child, null);
            holder.branch_name = (TextView) convertView.findViewById(R.id.branch_name);
            holder.branch_layout = (RelativeLayout) convertView.findViewById(R.id.branch_layout);
            holder.view = (View) convertView.findViewById(R.id.view);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.branch_name.setText(cartArryLists.get(position));
        Log.d(TAG, "getView: " + cartArryLists.get(position));

        if (cartArryLists.size() == (position + 1)) {

            holder.view.setVisibility(View.GONE);

        } else {

            holder.view.setVisibility(View.VISIBLE);

        }

        holder.branch_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EOSRequestActivity2.hidden_layout.setVisibility(View.GONE);
                EOSRequestActivity2.alternate_pos = position;

                if (context instanceof EOSRequestActivity2) {
                    ((EOSRequestActivity2) context).initView();
                }

                popupWindow.dismiss();

            }
        });
        return convertView;
    }
}

