package com.cs.ess.Rest;

import com.cs.ess.Models.BenefitContractResponse;
import com.cs.ess.Models.HourlyLeaveRequestResponse;
import com.cs.ess.Models.LeaveRequestGetResponse;
import com.cs.ess.Models.LeaveRequestResponse;
import com.cs.ess.Models.LoginResponse;
import com.cs.ess.Models.PersonalDetailsResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

//    @POST("UserAPI/GetMobileEmailVerify")
//    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @GET("RoutePlan/GetLoginValidation")
    Call<LoginResponse> getLogin (@Query("Username") String username, @Query("Password") String password, @Query("devicedatetime") String device_date_time, @Query("DeviceVersion") String device_version, @Query("AppVersion") String app_version );

    @GET("Inventory/GetPersonalInfo")
    Call<PersonalDetailsResponse> getpersonDetails (@Query("UserId") String userid);

    @GET("Inventory/GetBenefitContractsDetails")
    Call<BenefitContractResponse> getbenefit (@Query("UserId") String userid);

    @GET("Inventory/GetHourlyLeaveRequestDetails")
    Call<HourlyLeaveRequestResponse> gethourly (@Query("UserId") String userid);

    @GET("Inventory/GETLeaveRequestDetails")
    Call<LeaveRequestGetResponse> getleaveget (@Query("UserId") String userid);

    @GET("Inventory/GetLeaveIdDetails")
    Call<LeaveRequestResponse> getleave();



 }
