package com.cs.ess.Rest;

import android.util.Base64;
import android.util.Log;

import com.cs.ess.Utils.BasicAuthInterceptor;
import com.cs.ess.Utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.apache.http.BuildConfig;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static final String BASE_URL = "http://88.213.80.252:91/api/";

    private static Retrofit retrofit = null;
    static StringBuilder sb;

    public static Retrofit getClient() {

        if (retrofit == null) {

//            OkHttpClient client = new OkHttpClient.Builder()
//                    .addInterceptor(new BasicAuthInterceptor(Constants.API_USER_NAME, Constants.API_PASSWORD))
//                    .build();

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
//
                    .addInterceptor(new BasicAuthInterceptor(Constants.API_USER_NAME, Constants.API_PASSWORD))
                    .addInterceptor(new HttpInterceptor()) // This is used to add ApplicationInterceptor.
                    .addNetworkInterceptor(new HttpInterceptor()) //This is used to add NetworkInterceptor.
                    .build();

//
//            client = new OkHttpClient.Builder()
//                    .authenticator(new Authenticator() {
//                        @Override public Request authenticate(Route route, Response response) throws IOException {
//                            if (response.request().header("Authorization") != null) {
//                                return null; // Give up, we've already attempted to authenticate.
//                            }
//                            System.out.println("Authenticating for response: " + response);
//                            System.out.println("Challenges: " + response.challenges());
//                            String credential = Credentials.basic("admin", "admin");
//                            return response.request().newBuilder()
//                                    .header("Authorization", credential)
//                                    .build();
//                        }
//                    })
//                    .build();


//            OkHttpClient.Builder builder = new OkHttpClient.Builder();
//            builder.authenticator(getBasicAuth("admin", "admin"));

//            OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                    .addInterceptor(new CustomInterceptor()) // This is used to add ApplicationInterceptor.
//                    .addNetworkInterceptor(new CustomInterceptor()) //This is used to add NetworkInterceptor.
//                    .build();

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.interceptors().add(interceptor);
            httpClient.authenticator(getBasicAuth("admin", "admin"));


            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .addConverterFactory(ScalarsConverterFactory.create())
                    .client(okHttpClient)
                    .build();

        }
        return retrofit;
    }

    protected static Authenticator getBasicAuth(final String username, final String password) {
        return new Authenticator() {
            private int mCounter = 0;

            @Override
            public Request authenticate(Route route, Response response) throws IOException {
                Log.d("OkHttp", "authenticate(Route route, Response response) | mCounter = " + mCounter);
                if (mCounter++ > 0) {
                    Log.d("OkHttp", "authenticate(Route route, Response response) | I'll return null");
                    return null;
                } else {
                    Log.d("OkHttp", "authenticate(Route route, Response response) | This is first time, I'll try to authenticate");
                    String credential = Credentials.basic(username, password);
                    return response.request().newBuilder().header("Authorization", credential).build();
                }
            }
        };
    }

    public static class HttpInterceptor implements Interceptor {
        private final Charset UTF8 = Charset.forName("UTF-16LE");

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
//            Logger.getLogger(BuildConfig.APPLICATION_ID, "call ==> " + request.url());
            Response response = chain.proceed(request);
            ResponseBody responseBody = response.body();

            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE); // Buffer the entire body.
            Buffer buffer = source.buffer();
            buffer.clone().readString(UTF8).toString();
            Log.i("TAG", "intercept: " + buffer.clone().readString(UTF8).toString());
//            Logger.getLogger(BuildConfig.APPLICATION_ID, "ret ==> " + buffer.clone().readString(UTF8).toString());
            return response;
        }
    }

    private static class CustomInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
        /*
        chain.request() returns original request that you can work with(modify, rewrite)
        */
            Request request = chain.request();
            // Here you can rewrite the request
        /*
        chain.proceed(request) is the call which will initiate the HTTP work. This call invokes the
        request and returns the response as per the request.
        */
            Response response = chain.proceed(request);
            try ( ResponseBody responseBody = response.body() ) {
                InputStream is = responseBody.byteStream();
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-16LE"), 8);
                    sb = new StringBuilder();
                    sb.append(reader.readLine() + "\n");

                    String line = "0";
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
//                    json = sb.toString();
                } catch (Exception e) {
                    Log.e("log_tag", "Error converting result " + e.toString());
                }
            }
            //Here you can rewrite/modify the response
            return response;
        }
    }
}
