package com.cs.ess.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


import com.cs.ess.R;

import java.text.DecimalFormat;
import java.util.LinkedHashSet;
import java.util.Set;

public class Constants {
    public static String Country_Code = "+966 ";
    public static String STORE_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/stores/";
    public static String USERS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/Users/";
    public static String PRODUCTS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/products/";
    public static String ADS_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/Advertisements/";
    public static String RETURN_IMAGE_URL = "http://csadms.com/CheckClickAPI/Uploads/ReturnImages/";
    public static final DecimalFormat priceFormat1 = new DecimalFormat("##,##,##0.00");
    public static AlertDialog customDialog;

    public static String CityName = " ";
    public static String DistrictName = " ";
    public static double Latitude = 0.0;
    public static double Longitude = 0.0;


    public static String API_USER_NAME = "admin", API_PASSWORD = "admin";

    static String local_url = "http://88.213.80.252:91/api/";

    public static String getpersonDetails = local_url + "Inventory/GetPersonalInfo";
    public static String getbenefitcontract = local_url + "Inventory/GetBenefitContractsDetails";
    public static String getleaveget = local_url + "Inventory/GETLeaveRequestDetails";
    public static String getleave = local_url + "Inventory/GetLeaveIdDetails";
    public static String getleaveupate = local_url + "Inventory/CreateLeaveRequest";
    public static String getAlterateget = local_url + "Inventory/GetAlternativeEmployeeDetails";
    public static String getLeaveBalance = local_url + "Inventory/GetEmployeeBalanceDetails";
    public static String getovertimetpes = local_url+"Inventory/GetOvertimeTypeDetails";
    public static String getovertimrview = local_url+"Inventory/GetOverTimeRequestDetails";
    public static String getLoanview = local_url+"Inventory/GetLoanRequestDetails";
    public static String getSubmit = local_url+"Inventory/SubmitLeaveRequest";
    public static String getDelete = local_url+"Inventory/DeleteLeaveRequest";
    public static String getLoneDelete = local_url+"Inventory/DeleteLoanRequest";
    public static String getLonetypedetails = local_url+"Inventory/GetLoanTypeDetails";
    public static String getLoanBalancedetails = local_url+"Inventory/GetEmployeeLoanBalanceDetails";
    public static String getLoanCreate = local_url+"Inventory/createLoanRequest";
    public static String getLoanSubmit = local_url+"Inventory/SubmitLoanRequest";









    public static Typeface getarbooldTypeFace(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "Helvetica Bold.ttf");
        return typeface;
    }


    public static String getDeviceType(Context context){
        String device = "Andr v";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            device = device+version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return device;
    }
    public static void showOneButtonAlertDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


        no.setVisibility(View.GONE);

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }



    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void requestEditTextFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    /* The configuration values to change across the app */
    public static class Config {

        /* The payment brands for Ready-to-Use UI and Payment Button */
        public static final Set<String> PAYMENT_BRANDS;

        static {
            PAYMENT_BRANDS = new LinkedHashSet<>();

            PAYMENT_BRANDS.add("VISA");
            PAYMENT_BRANDS.add("MASTER");
        }
    }
    public static final String SHOPPER_RESULT_URL = "com.cs.checkclickuser.payments://result";

}
