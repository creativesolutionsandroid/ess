package com.cs.ess.Utils;

import com.cs.ess.Models.PersonalDetailsResponse;
import com.cs.ess.Rest.APIInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceHelper {

    private static final String ENDPOINT = "http://88.213.80.252:91/api/";

    private static OkHttpClient httpClient = new OkHttpClient();
    private static ServiceHelper instance = new ServiceHelper();
    private APIInterface service;

    String userid = "ialsulta";


    private ServiceHelper() {

        Retrofit retrofit = createAdapter().build();
        service = retrofit.create(APIInterface.class);
    }

    public static ServiceHelper getInstance() {
        return instance;
    }

    private Retrofit.Builder createAdapter() {

//        httpClient.setReadTimeout(60, TimeUnit.SECONDS);
//        httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpClient.interceptors().add(interceptor);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(Constants.API_USER_NAME, Constants.API_PASSWORD))
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    public Call<PersonalDetailsResponse> getAllCategory() {
        return service.getpersonDetails(userid);
    }
}

