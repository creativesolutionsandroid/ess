package com.cs.ess.Models;

import java.io.Serializable;

public  class RetunTypeResponce implements Serializable {

     String TODATE;
     String FROMDATE;
     String LeaveType;
     String LEAVETRANSID;

    public String getTODATE() {
        return TODATE;
    }

    public void setTODATE(String TODATE) {
        this.TODATE = TODATE;
    }

    public String getFROMDATE() {
        return FROMDATE;
    }

    public void setFROMDATE(String FROMDATE) {
        this.FROMDATE = FROMDATE;
    }

    public String getLeaveType() {
        return LeaveType;
    }

    public void setLeaveType(String LeaveType) {
        this.LeaveType = LeaveType;
    }

    public String getLEAVETRANSID() {
        return LEAVETRANSID;
    }

    public void setLEAVETRANSID(String LEAVETRANSID) {
        this.LEAVETRANSID = LEAVETRANSID;
    }
}
