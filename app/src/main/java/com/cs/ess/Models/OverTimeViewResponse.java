package com.cs.ess.Models;

import java.io.Serializable;

public class OverTimeViewResponse implements Serializable {

     String WORKERNAME;
     String WORKFLOWSTATE, WORKFLOWSTATENUM;
     String TRASNACTIONTXT;
     String NUMBEROFHOURS;
     String AMOUNT;
     String OVERTIMETYPE;
     String PERSONNELNUMBER;
     String TRANSDATE;
     String OVERTIMETRANSID;

    public String getWORKERNAME() {
        return WORKERNAME;
    }

    public void setWORKERNAME(String WORKERNAME) {
        this.WORKERNAME = WORKERNAME;
    }

    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getTRASNACTIONTXT() {
        return TRASNACTIONTXT;
    }

    public void setTRASNACTIONTXT(String TRASNACTIONTXT) {
        this.TRASNACTIONTXT = TRASNACTIONTXT;
    }

    public String getNUMBEROFHOURS() {
        return NUMBEROFHOURS;
    }

    public void setNUMBEROFHOURS(String NUMBEROFHOURS) {
        this.NUMBEROFHOURS = NUMBEROFHOURS;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }

    public String getOVERTIMETYPE() {
        return OVERTIMETYPE;
    }

    public void setOVERTIMETYPE(String OVERTIMETYPE) {
        this.OVERTIMETYPE = OVERTIMETYPE;
    }

    public String getPERSONNELNUMBER() {
        return PERSONNELNUMBER;
    }

    public void setPERSONNELNUMBER(String PERSONNELNUMBER) {
        this.PERSONNELNUMBER = PERSONNELNUMBER;
    }

    public String getTRANSDATE() {
        return TRANSDATE;
    }

    public void setTRANSDATE(String TRANSDATE) {
        this.TRANSDATE = TRANSDATE;
    }

    public String getOVERTIMETRANSID() {
        return OVERTIMETRANSID;
    }

    public void setOVERTIMETRANSID(String OVERTIMETRANSID) {
        this.OVERTIMETRANSID = OVERTIMETRANSID;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }
}
