package com.cs.ess.Models;

import java.io.Serializable;

public class HRGeneralDetailsResponse implements Serializable {


    String WORKFLOWSTATE;
    String PersonnalNumber;
    String DESCRIPTION;
    String COMMENTS;
    String TRANSDATE;
    String DOCID;
    String DAPDOCREQID;
    String WORKFLOWSTATENUM;


    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getPersonnalNumber() {
        return PersonnalNumber;
    }

    public void setPersonnalNumber(String PersonnalNumber) {
        this.PersonnalNumber = PersonnalNumber;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getCOMMENTS() {
        return COMMENTS;
    }

    public void setCOMMENTS(String COMMENTS) {
        this.COMMENTS = COMMENTS;
    }

    public String getTRANSDATE() {
        return TRANSDATE;
    }

    public void setTRANSDATE(String TRANSDATE) {
        this.TRANSDATE = TRANSDATE;
    }

    public String getDOCID() {
        return DOCID;
    }

    public void setDOCID(String DOCID) {
        this.DOCID = DOCID;
    }

    public String getDAPDOCREQID() {
        return DAPDOCREQID;
    }

    public void setDAPDOCREQID(String DAPDOCREQID) {
        this.DAPDOCREQID = DAPDOCREQID;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }
}
