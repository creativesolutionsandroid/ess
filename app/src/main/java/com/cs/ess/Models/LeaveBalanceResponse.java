package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeaveBalanceResponse {

    String TotalleaveBalance;
    String PersonnalNumber;
    String LeaveTypeId;

    public String getTotalleaveBalance() {
        return TotalleaveBalance;
    }

    public void setTotalleaveBalance(String TotalleaveBalance) {
        this.TotalleaveBalance = TotalleaveBalance;
    }

    public String getPersonnalNumber() {
        return PersonnalNumber;
    }

    public void setPersonnalNumber(String PersonnalNumber) {
        this.PersonnalNumber = PersonnalNumber;
    }

    public String getLeaveTypeId() {
        return LeaveTypeId;
    }

    public void setLeaveTypeId(String LeaveTypeId) {
        this.LeaveTypeId = LeaveTypeId;
    }
}
