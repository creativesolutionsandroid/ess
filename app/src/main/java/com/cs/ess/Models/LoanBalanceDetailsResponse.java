package com.cs.ess.Models;

import java.io.Serializable;
import java.util.List;

public class LoanBalanceDetailsResponse implements Serializable {

    String TotalLoanBalance;
    String PersonnalNumber;
    String LoanTypeId;

    public String getTotalLoanBalance() {
        return TotalLoanBalance;
    }

    public void setTotalLoanBalance(String TotalLoanBalance) {
        this.TotalLoanBalance = TotalLoanBalance;
    }

    public String getPersonnalNumber() {
        return PersonnalNumber;
    }

    public void setPersonnalNumber(String PersonnalNumber) {
        this.PersonnalNumber = PersonnalNumber;
    }

    public String getLoanTypeId() {
        return LoanTypeId;
    }

    public void setLoanTypeId(String LoanTypeId) {
        this.LoanTypeId = LoanTypeId;
    }
}
