package com.cs.ess.Models;

public class HRGeneralDocuResponse {


    String DESCRIPTION;
    String DOCTYPEID;
    String DOCID;

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getDOCTYPEID() {
        return DOCTYPEID;
    }

    public void setDOCTYPEID(String DOCTYPEID) {
        this.DOCTYPEID = DOCTYPEID;
    }

    public String getDOCID() {
        return DOCID;
    }

    public void setDOCID(String DOCID) {
        this.DOCID = DOCID;
    }
}
