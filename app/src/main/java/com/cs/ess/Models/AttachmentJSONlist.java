package com.cs.ess.Models;

import java.io.Serializable;

public class AttachmentJSONlist implements Serializable {

    String getByteArray, getFileExt, getFileName, getNotes, getRequestId, getRequestType;
    int image_size;

    public String getGetByteArray() {
        return getByteArray;
    }

    public void setGetByteArray(String getByteArray) {
        this.getByteArray = getByteArray;
    }

    public String getGetFileExt() {
        return getFileExt;
    }

    public void setGetFileExt(String getFileExt) {
        this.getFileExt = getFileExt;
    }

    public String getGetFileName() {
        return getFileName;
    }

    public void setGetFileName(String getFileName) {
        this.getFileName = getFileName;
    }

    public String getGetNotes() {
        return getNotes;
    }

    public void setGetNotes(String getNotes) {
        this.getNotes = getNotes;
    }

    public String getGetRequestId() {
        return getRequestId;
    }

    public void setGetRequestId(String getRequestId) {
        this.getRequestId = getRequestId;
    }

    public String getGetRequestType() {
        return getRequestType;
    }

    public void setGetRequestType(String getRequestType) {
        this.getRequestType = getRequestType;
    }

    public int getImage_size() {
        return image_size;
    }

    public void setImage_size(int image_size) {
        this.image_size = image_size;
    }
}
