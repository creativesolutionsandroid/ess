package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeaveRequestResponse {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("GetLeaveIdDetails")
        private List<GetLeaveIdDetails> GetLeaveIdDetails;

        public List<GetLeaveIdDetails> getGetLeaveIdDetails() {
            return GetLeaveIdDetails;
        }

        public void setGetLeaveIdDetails(List<GetLeaveIdDetails> GetLeaveIdDetails) {
            this.GetLeaveIdDetails = GetLeaveIdDetails;
        }
    }

    public static class GetLeaveIdDetails {
        @Expose
        @SerializedName("DESCRIPTION")
        private String DESCRIPTION;
        @Expose
        @SerializedName("LEAVEID")
        private String LEAVEID;

        public String getDESCRIPTION() {
            return DESCRIPTION;
        }

        public void setDESCRIPTION(String DESCRIPTION) {
            this.DESCRIPTION = DESCRIPTION;
        }

        public String getLEAVEID() {
            return LEAVEID;
        }

        public void setLEAVEID(String LEAVEID) {
            this.LEAVEID = LEAVEID;
        }
    }
}
