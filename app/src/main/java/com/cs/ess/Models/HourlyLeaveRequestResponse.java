package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HourlyLeaveRequestResponse {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private String Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("GetHourlyLeaveRequestDetails")
        private List<GetHourlyLeaveRequestDetails> GetHourlyLeaveRequestDetails;

        public List<GetHourlyLeaveRequestDetails> getGetHourlyLeaveRequestDetails() {
            return GetHourlyLeaveRequestDetails;
        }

        public void setGetHourlyLeaveRequestDetails(List<GetHourlyLeaveRequestDetails> GetHourlyLeaveRequestDetails) {
            this.GetHourlyLeaveRequestDetails = GetHourlyLeaveRequestDetails;
        }
    }

    public static class GetHourlyLeaveRequestDetails {
        @Expose
        @SerializedName("DATAAREAID")
        private String DATAAREAID;
        @Expose
        @SerializedName("NAME")
        private String NAME;
        @Expose
        @SerializedName("WORKFLOWSTATE")
        private String WORKFLOWSTATE;
        @Expose
        @SerializedName("TXLLEAVEBALANCEREAL")
        private String TXLLEAVEBALANCEREAL;
        @Expose
        @SerializedName("NUMOFHOURS")
        private String NUMOFHOURS;
        @Expose
        @SerializedName("TOTIME")
        private String TOTIME;
        @Expose
        @SerializedName("FROMTIME")
        private String FROMTIME;
        @Expose
        @SerializedName("TRANSDATE")
        private String TRANSDATE;
        @Expose
        @SerializedName("DAPABSENCETRANSTYPE")
        private String DAPABSENCETRANSTYPE;
        @Expose
        @SerializedName("DAPABSENCETRANS")
        private String DAPABSENCETRANS;

        public String getDATAAREAID() {
            return DATAAREAID;
        }

        public void setDATAAREAID(String DATAAREAID) {
            this.DATAAREAID = DATAAREAID;
        }

        public String getNAME() {
            return NAME;
        }

        public void setNAME(String NAME) {
            this.NAME = NAME;
        }

        public String getWORKFLOWSTATE() {
            return WORKFLOWSTATE;
        }

        public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
            this.WORKFLOWSTATE = WORKFLOWSTATE;
        }

        public String getTXLLEAVEBALANCEREAL() {
            return TXLLEAVEBALANCEREAL;
        }

        public void setTXLLEAVEBALANCEREAL(String TXLLEAVEBALANCEREAL) {
            this.TXLLEAVEBALANCEREAL = TXLLEAVEBALANCEREAL;
        }

        public String getNUMOFHOURS() {
            return NUMOFHOURS;
        }

        public void setNUMOFHOURS(String NUMOFHOURS) {
            this.NUMOFHOURS = NUMOFHOURS;
        }

        public String getTOTIME() {
            return TOTIME;
        }

        public void setTOTIME(String TOTIME) {
            this.TOTIME = TOTIME;
        }

        public String getFROMTIME() {
            return FROMTIME;
        }

        public void setFROMTIME(String FROMTIME) {
            this.FROMTIME = FROMTIME;
        }

        public String getTRANSDATE() {
            return TRANSDATE;
        }

        public void setTRANSDATE(String TRANSDATE) {
            this.TRANSDATE = TRANSDATE;
        }

        public String getDAPABSENCETRANSTYPE() {
            return DAPABSENCETRANSTYPE;
        }

        public void setDAPABSENCETRANSTYPE(String DAPABSENCETRANSTYPE) {
            this.DAPABSENCETRANSTYPE = DAPABSENCETRANSTYPE;
        }

        public String getDAPABSENCETRANS() {
            return DAPABSENCETRANS;
        }

        public void setDAPABSENCETRANS(String DAPABSENCETRANS) {
            this.DAPABSENCETRANS = DAPABSENCETRANS;
        }
    }
}
