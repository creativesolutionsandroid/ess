package com.cs.ess.Models;

import java.util.List;

public class LoanTypeDetailsResponse {

    String LOANDESCRIPTION;
    String LOANTYPEID;

    public String getLOANDESCRIPTION() {
        return LOANDESCRIPTION;
    }

    public void setLOANDESCRIPTION(String LOANDESCRIPTION) {
        this.LOANDESCRIPTION = LOANDESCRIPTION;
    }

    public String getLOANTYPEID() {
        return LOANTYPEID;
    }

    public void setLOANTYPEID(String LOANTYPEID) {
        this.LOANTYPEID = LOANTYPEID;
    }
}
