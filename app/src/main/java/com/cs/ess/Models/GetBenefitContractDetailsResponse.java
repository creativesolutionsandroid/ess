package com.cs.ess.Models;

public class GetBenefitContractDetailsResponse {

    String ARABICNAME;

    String ValidTo;

    String ValidFrom;

    String NAME;

    String PERSONNELNUMBER;

    String DATAAREAID;

    int BASICSALARY;

    String CONTRACTPERIODS;

    String CONTRACTNUM;

    public String getARABICNAME() {
        return ARABICNAME;
    }

    public void setARABICNAME(String ARABICNAME) {
        this.ARABICNAME = ARABICNAME;
    }

    public String getValidTo() {
        return ValidTo;
    }

    public void setValidTo(String ValidTo) {
        this.ValidTo = ValidTo;
    }

    public String getValidFrom() {
        return ValidFrom;
    }

    public void setValidFrom(String ValidFrom) {
        this.ValidFrom = ValidFrom;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPERSONNELNUMBER() {
        return PERSONNELNUMBER;
    }

    public void setPERSONNELNUMBER(String PERSONNELNUMBER) {
        this.PERSONNELNUMBER = PERSONNELNUMBER;
    }

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public int getBASICSALARY() {
        return BASICSALARY;
    }

    public void setBASICSALARY(int BASICSALARY) {
        this.BASICSALARY = BASICSALARY;
    }

    public String getCONTRACTPERIODS() {
        return CONTRACTPERIODS;
    }

    public void setCONTRACTPERIODS(String CONTRACTPERIODS) {
        this.CONTRACTPERIODS = CONTRACTPERIODS;
    }

    public String getCONTRACTNUM() {
        return CONTRACTNUM;
    }

    public void setCONTRACTNUM(String CONTRACTNUM) {
        this.CONTRACTNUM = CONTRACTNUM;
    }
}
