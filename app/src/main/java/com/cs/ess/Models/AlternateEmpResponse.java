package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlternateEmpResponse {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("EmployeeDetails")
        private List<EmployeeDetails> EmployeeDetails;

        public List<EmployeeDetails> getEmployeeDetails() {
            return EmployeeDetails;
        }

        public void setEmployeeDetails(List<EmployeeDetails> EmployeeDetails) {
            this.EmployeeDetails = EmployeeDetails;
        }
    }

    public static class EmployeeDetails {
        @Expose
        @SerializedName("NAME")
        private String NAME;
        @Expose
        @SerializedName("PERSONNELNUMBER")
        private String PERSONNELNUMBER;

        public String getNAME() {
            return NAME;
        }

        public void setNAME(String NAME) {
            this.NAME = NAME;
        }

        public String getPERSONNELNUMBER() {
            return PERSONNELNUMBER;
        }

        public void setPERSONNELNUMBER(String PERSONNELNUMBER) {
            this.PERSONNELNUMBER = PERSONNELNUMBER;
        }
    }
}
