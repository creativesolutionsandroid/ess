package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PersonalDetailsResponse {

    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Data")
    private Data Data;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("GetPersonalInfo")
        private List<GetPersonalInfo> GetPersonalInfo;

        public List<GetPersonalInfo> getGetPersonalInfo() {
            return GetPersonalInfo;
        }

        public void setGetPersonalInfo(List<GetPersonalInfo> GetPersonalInfo) {
            this.GetPersonalInfo = GetPersonalInfo;
        }
    }

    public static class GetPersonalInfo {
        @Expose
        @SerializedName("Empstartdate")
        private String Empstartdate;
        @Expose
        @SerializedName("Description")
        private String Description;
        @Expose
        @SerializedName("Job")
        private String Job;
        @Expose
        @SerializedName("WorkerNameAR")
        private String WorkerNameAR;
        @Expose
        @SerializedName("WorkerName")
        private String WorkerName;
        @Expose
        @SerializedName("PersonalNumber")
        private String PersonalNumber;

        public String getEmpstartdate() {
            return Empstartdate;
        }

        public void setEmpstartdate(String Empstartdate) {
            this.Empstartdate = Empstartdate;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getJob() {
            return Job;
        }

        public void setJob(String Job) {
            this.Job = Job;
        }

        public String getWorkerNameAR() {
            return WorkerNameAR;
        }

        public void setWorkerNameAR(String WorkerNameAR) {
            this.WorkerNameAR = WorkerNameAR;
        }

        public String getWorkerName() {
            return WorkerName;
        }

        public void setWorkerName(String WorkerName) {
            this.WorkerName = WorkerName;
        }

        public String getPersonalNumber() {
            return PersonalNumber;
        }

        public void setPersonalNumber(String PersonalNumber) {
            this.PersonalNumber = PersonalNumber;
        }
    }
}
