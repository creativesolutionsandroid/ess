package com.cs.ess.Models;

import java.io.Serializable;

public class RetunTripDetailsResponce implements Serializable {


    String DATAAREAID;
    String NAME;
    String WORKFLOWSTATENUM;
    String WORKFLOWSTATE;
    String REFTRANSID;
    String RETURNDATE;
    String TRANSDATE;
    String REQUESTID, Leave_Type;

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }

    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getREFTRANSID() {
        return REFTRANSID;
    }

    public void setREFTRANSID(String REFTRANSID) {
        this.REFTRANSID = REFTRANSID;
    }

    public String getRETURNDATE() {
        return RETURNDATE;
    }

    public void setRETURNDATE(String RETURNDATE) {
        this.RETURNDATE = RETURNDATE;
    }

    public String getTRANSDATE() {
        return TRANSDATE;
    }

    public void setTRANSDATE(String TRANSDATE) {
        this.TRANSDATE = TRANSDATE;
    }

    public String getREQUESTID() {
        return REQUESTID;
    }

    public void setREQUESTID(String REQUESTID) {
        this.REQUESTID = REQUESTID;
    }

    public String getLeave_Type() {
        return Leave_Type;
    }

    public void setLeave_Type(String leave_Type) {
        Leave_Type = leave_Type;
    }
}
