package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class LoanRequestResponce implements Serializable {

    String DATAAREAID;
    String NAME;
    String WORKFLOWSTATE, WORKFLOWSTATENUM;
    String LOANMONTHLYPAY;
    String LOANVALUE;
    String DEDUCTIONSTARTDATE;
    String LOANDATE;
    String LOANTYPE;
    String LOANREQUESTID;
    String PRPOSEOFLOAN;
    boolean mSubmit=true;

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getLOANMONTHLYPAY() {
        return LOANMONTHLYPAY;
    }

    public void setLOANMONTHLYPAY(String LOANMONTHLYPAY) {
        this.LOANMONTHLYPAY = LOANMONTHLYPAY;
    }

    public String getLOANVALUE() {
        return LOANVALUE;
    }

    public void setLOANVALUE(String LOANVALUE) {
        this.LOANVALUE = LOANVALUE;
    }

    public String getDEDUCTIONSTARTDATE() {
        return DEDUCTIONSTARTDATE;
    }

    public void setDEDUCTIONSTARTDATE(String DEDUCTIONSTARTDATE) {
        this.DEDUCTIONSTARTDATE = DEDUCTIONSTARTDATE;
    }

    public String getLOANDATE() {
        return LOANDATE;
    }

    public void setLOANDATE(String LOANDATE) {
        this.LOANDATE = LOANDATE;
    }

    public String getLOANTYPE() {
        return LOANTYPE;
    }

    public void setLOANTYPE(String LOANTYPE) {
        this.LOANTYPE = LOANTYPE;
    }

    public String getLOANREQUESTID() {
        return LOANREQUESTID;
    }

    public void setLOANREQUESTID(String LOANREQUESTID) {
        this.LOANREQUESTID = LOANREQUESTID;
    }

    public String getPRPOSEOFLOAN() {
        return PRPOSEOFLOAN;
    }

    public void setPRPOSEOFLOAN(String PRPOSEOFLOAN) {
        this.PRPOSEOFLOAN = PRPOSEOFLOAN;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }

    public boolean ismSubmit() {
        return mSubmit;
    }

    public void setmSubmit(boolean mSubmit) {
        this.mSubmit = mSubmit;
    }
}
