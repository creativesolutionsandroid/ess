package com.cs.ess.Models;

import java.io.Serializable;

public class LeaveRequestGetResponse implements Serializable {

    String DATAAREAID;

    String WORKERNAME;

    String WORKFLOWSTATE, WORKFLOWSTATENUM;

    String ALTERNATIVEEMPLOYEENAME;

    String ALTERNATIVEEMPLOYEEPN;

    String NOTES;

    String PROVIDEVISA;

    String CONTACTINFODURINGLEAVE;

    String LEAVEBALANCE;

    String LEAVEID;

    String TODATE;

    String FROMDATE;

    String LEAVEREQUESTID;

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public String getWORKERNAME() {
        return WORKERNAME;
    }

    public void setWORKERNAME(String WORKERNAME) {
        this.WORKERNAME = WORKERNAME;
    }

    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getALTERNATIVEEMPLOYEENAME() {
        return ALTERNATIVEEMPLOYEENAME;
    }

    public void setALTERNATIVEEMPLOYEENAME(String ALTERNATIVEEMPLOYEENAME) {
        this.ALTERNATIVEEMPLOYEENAME = ALTERNATIVEEMPLOYEENAME;
    }

    public String getALTERNATIVEEMPLOYEEPN() {
        return ALTERNATIVEEMPLOYEEPN;
    }

    public void setALTERNATIVEEMPLOYEEPN(String ALTERNATIVEEMPLOYEEPN) {
        this.ALTERNATIVEEMPLOYEEPN = ALTERNATIVEEMPLOYEEPN;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public String getPROVIDEVISA() {
        return PROVIDEVISA;
    }

    public void setPROVIDEVISA(String PROVIDEVISA) {
        this.PROVIDEVISA = PROVIDEVISA;
    }

    public String getCONTACTINFODURINGLEAVE() {
        return CONTACTINFODURINGLEAVE;
    }

    public void setCONTACTINFODURINGLEAVE(String CONTACTINFODURINGLEAVE) {
        this.CONTACTINFODURINGLEAVE = CONTACTINFODURINGLEAVE;
    }

    public String getLEAVEBALANCE() {
        return LEAVEBALANCE;
    }

    public void setLEAVEBALANCE(String LEAVEBALANCE) {
        this.LEAVEBALANCE = LEAVEBALANCE;
    }

    public String getLEAVEID() {
        return LEAVEID;
    }

    public void setLEAVEID(String LEAVEID) {
        this.LEAVEID = LEAVEID;
    }

    public String getTODATE() {
        return TODATE;
    }

    public void setTODATE(String TODATE) {
        this.TODATE = TODATE;
    }

    public String getFROMDATE() {
        return FROMDATE;
    }

    public void setFROMDATE(String FROMDATE) {
        this.FROMDATE = FROMDATE;
    }

    public String getLEAVEREQUESTID() {
        return LEAVEREQUESTID;
    }

    public void setLEAVEREQUESTID(String LEAVEREQUESTID) {
        this.LEAVEREQUESTID = LEAVEREQUESTID;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }
}
