package com.cs.ess.Models;

import java.io.Serializable;

public  class ClearanceDetailsResponce implements Serializable {

     String NOTES;
     String ARABICNAME;
     String NAME;
     String PERSONNELNUMBER;
     String DATAAREAID;
     String WORKFLOWSTATENUM;
     String UNPAIDVACATIONS;
     String UNPAIDTICKETSAMOUNT;
     String UNPAIDTICKETS;
     String UNPAIDSALARIES;
     String UNPAIDNOTICEPERIOD;
     String UNPAIDLOANS;
     String UNPAIDLEAVENUMBEROFDAYS;
     String UNPAIDLASTWORKINGDAYS;
     String TXLGRATUITY;
     String TXLEXPENSES;
     String TXLADVANCEHOUSING;
     String TICKETPAYMENTMETHOD;
     String EOSCOMPENSATION;
     String PAID;
     String POSTED;
     String EOSTRANSTYPE;
     String WORKFLOWSTATE;
     String TRANSDATE;
     String EOSTRANSID;

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public String getARABICNAME() {
        return ARABICNAME;
    }

    public void setARABICNAME(String ARABICNAME) {
        this.ARABICNAME = ARABICNAME;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPERSONNELNUMBER() {
        return PERSONNELNUMBER;
    }

    public void setPERSONNELNUMBER(String PERSONNELNUMBER) {
        this.PERSONNELNUMBER = PERSONNELNUMBER;
    }

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }

    public String getUNPAIDVACATIONS() {
        return UNPAIDVACATIONS;
    }

    public void setUNPAIDVACATIONS(String UNPAIDVACATIONS) {
        this.UNPAIDVACATIONS = UNPAIDVACATIONS;
    }

    public String getUNPAIDTICKETSAMOUNT() {
        return UNPAIDTICKETSAMOUNT;
    }

    public void setUNPAIDTICKETSAMOUNT(String UNPAIDTICKETSAMOUNT) {
        this.UNPAIDTICKETSAMOUNT = UNPAIDTICKETSAMOUNT;
    }

    public String getUNPAIDTICKETS() {
        return UNPAIDTICKETS;
    }

    public void setUNPAIDTICKETS(String UNPAIDTICKETS) {
        this.UNPAIDTICKETS = UNPAIDTICKETS;
    }

    public String getUNPAIDSALARIES() {
        return UNPAIDSALARIES;
    }

    public void setUNPAIDSALARIES(String UNPAIDSALARIES) {
        this.UNPAIDSALARIES = UNPAIDSALARIES;
    }

    public String getUNPAIDNOTICEPERIOD() {
        return UNPAIDNOTICEPERIOD;
    }

    public void setUNPAIDNOTICEPERIOD(String UNPAIDNOTICEPERIOD) {
        this.UNPAIDNOTICEPERIOD = UNPAIDNOTICEPERIOD;
    }

    public String getUNPAIDLOANS() {
        return UNPAIDLOANS;
    }

    public void setUNPAIDLOANS(String UNPAIDLOANS) {
        this.UNPAIDLOANS = UNPAIDLOANS;
    }

    public String getUNPAIDLEAVENUMBEROFDAYS() {
        return UNPAIDLEAVENUMBEROFDAYS;
    }

    public void setUNPAIDLEAVENUMBEROFDAYS(String UNPAIDLEAVENUMBEROFDAYS) {
        this.UNPAIDLEAVENUMBEROFDAYS = UNPAIDLEAVENUMBEROFDAYS;
    }

    public String getUNPAIDLASTWORKINGDAYS() {
        return UNPAIDLASTWORKINGDAYS;
    }

    public void setUNPAIDLASTWORKINGDAYS(String UNPAIDLASTWORKINGDAYS) {
        this.UNPAIDLASTWORKINGDAYS = UNPAIDLASTWORKINGDAYS;
    }

    public String getTXLGRATUITY() {
        return TXLGRATUITY;
    }

    public void setTXLGRATUITY(String TXLGRATUITY) {
        this.TXLGRATUITY = TXLGRATUITY;
    }

    public String getTXLEXPENSES() {
        return TXLEXPENSES;
    }

    public void setTXLEXPENSES(String TXLEXPENSES) {
        this.TXLEXPENSES = TXLEXPENSES;
    }

    public String getTXLADVANCEHOUSING() {
        return TXLADVANCEHOUSING;
    }

    public void setTXLADVANCEHOUSING(String TXLADVANCEHOUSING) {
        this.TXLADVANCEHOUSING = TXLADVANCEHOUSING;
    }

    public String getTICKETPAYMENTMETHOD() {
        return TICKETPAYMENTMETHOD;
    }

    public void setTICKETPAYMENTMETHOD(String TICKETPAYMENTMETHOD) {
        this.TICKETPAYMENTMETHOD = TICKETPAYMENTMETHOD;
    }

    public String getEOSCOMPENSATION() {
        return EOSCOMPENSATION;
    }

    public void setEOSCOMPENSATION(String EOSCOMPENSATION) {
        this.EOSCOMPENSATION = EOSCOMPENSATION;
    }

    public String getPAID() {
        return PAID;
    }

    public void setPAID(String PAID) {
        this.PAID = PAID;
    }

    public String getPOSTED() {
        return POSTED;
    }

    public void setPOSTED(String POSTED) {
        this.POSTED = POSTED;
    }

    public String getEOSTRANSTYPE() {
        return EOSTRANSTYPE;
    }

    public void setEOSTRANSTYPE(String EOSTRANSTYPE) {
        this.EOSTRANSTYPE = EOSTRANSTYPE;
    }

    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getTRANSDATE() {
        return TRANSDATE;
    }

    public void setTRANSDATE(String TRANSDATE) {
        this.TRANSDATE = TRANSDATE;
    }

    public String getEOSTRANSID() {
        return EOSTRANSID;
    }

    public void setEOSTRANSID(String EOSTRANSID) {
        this.EOSTRANSID = EOSTRANSID;
    }
}
