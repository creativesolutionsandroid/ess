package com.cs.ess.Models;

import java.io.Serializable;

public  class HourlyDetailsResponce implements Serializable {

     String DATAAREAID;
     String NAME;
     String WORKFLOWSTATE;
     String TXLLEAVEBALANCEREAL;
     String NUMOFHOURS;
     String TOTIME;
     String FROMTIME;
     String TRANSDATE;
     String DAPABSENCETRANSTYPE;
     String DAPABSENCETRANS;
     String WORKFLOWSTATENUM;
     String PERMISSIONLEAVETYPE;
     String DESCRIPTION;

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getWORKFLOWSTATE() {
        return WORKFLOWSTATE;
    }

    public void setWORKFLOWSTATE(String WORKFLOWSTATE) {
        this.WORKFLOWSTATE = WORKFLOWSTATE;
    }

    public String getTXLLEAVEBALANCEREAL() {
        return TXLLEAVEBALANCEREAL;
    }

    public void setTXLLEAVEBALANCEREAL(String TXLLEAVEBALANCEREAL) {
        this.TXLLEAVEBALANCEREAL = TXLLEAVEBALANCEREAL;
    }

    public String getNUMOFHOURS() {
        return NUMOFHOURS;
    }

    public void setNUMOFHOURS(String NUMOFHOURS) {
        this.NUMOFHOURS = NUMOFHOURS;
    }

    public String getTOTIME() {
        return TOTIME;
    }

    public void setTOTIME(String TOTIME) {
        this.TOTIME = TOTIME;
    }

    public String getFROMTIME() {
        return FROMTIME;
    }

    public void setFROMTIME(String FROMTIME) {
        this.FROMTIME = FROMTIME;
    }

    public String getTRANSDATE() {
        return TRANSDATE;
    }

    public void setTRANSDATE(String TRANSDATE) {
        this.TRANSDATE = TRANSDATE;
    }

    public String getDAPABSENCETRANSTYPE() {
        return DAPABSENCETRANSTYPE;
    }

    public void setDAPABSENCETRANSTYPE(String DAPABSENCETRANSTYPE) {
        this.DAPABSENCETRANSTYPE = DAPABSENCETRANSTYPE;
    }

    public String getDAPABSENCETRANS() {
        return DAPABSENCETRANS;
    }

    public void setDAPABSENCETRANS(String DAPABSENCETRANS) {
        this.DAPABSENCETRANS = DAPABSENCETRANS;
    }

    public String getWORKFLOWSTATENUM() {
        return WORKFLOWSTATENUM;
    }

    public void setWORKFLOWSTATENUM(String WORKFLOWSTATENUM) {
        this.WORKFLOWSTATENUM = WORKFLOWSTATENUM;
    }

    public String getPERMISSIONLEAVETYPE() {
        return PERMISSIONLEAVETYPE;
    }

    public void setPERMISSIONLEAVETYPE(String PERMISSIONLEAVETYPE) {
        this.PERMISSIONLEAVETYPE = PERMISSIONLEAVETYPE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }
}

