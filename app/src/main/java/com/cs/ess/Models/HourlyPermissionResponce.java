package com.cs.ess.Models;

import java.io.Serializable;

public  class HourlyPermissionResponce implements Serializable {


     String TotalleaveBalance;
     String PersonnalNumber;
     String PermissionTypeId;

    public String getTotalleaveBalance() {
        return TotalleaveBalance;
    }

    public void setTotalleaveBalance(String TotalleaveBalance) {
        this.TotalleaveBalance = TotalleaveBalance;
    }

    public String getPersonnalNumber() {
        return PersonnalNumber;
    }

    public void setPersonnalNumber(String PersonnalNumber) {
        this.PersonnalNumber = PersonnalNumber;
    }

    public String getPermissionTypeId() {
        return PermissionTypeId;
    }

    public void setPermissionTypeId(String PermissionTypeId) {
        this.PermissionTypeId = PermissionTypeId;
    }
}
