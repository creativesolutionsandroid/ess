package com.cs.ess.Models;

import java.io.Serializable;

public  class EOSDetailsResponce implements Serializable {

     String DATAAREAID;
     String NAME;
     String WFDUMMIESWORKFLOWSTATENUM;
     String WFDUMMIESWORKFLOWSTATE;
     String SUBMITTINGDATE;
     String PERSONNELNUMBER;
     String LASTWORKINGDATE;
     String EOSTRANSTYPE;
     String DAPEOSREQID;
     String NOTES;

    public String getDATAAREAID() {
        return DATAAREAID;
    }

    public void setDATAAREAID(String DATAAREAID) {
        this.DATAAREAID = DATAAREAID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getWFDUMMIESWORKFLOWSTATENUM() {
        return WFDUMMIESWORKFLOWSTATENUM;
    }

    public void setWFDUMMIESWORKFLOWSTATENUM(String WFDUMMIESWORKFLOWSTATENUM) {
        this.WFDUMMIESWORKFLOWSTATENUM = WFDUMMIESWORKFLOWSTATENUM;
    }

    public String getWFDUMMIESWORKFLOWSTATE() {
        return WFDUMMIESWORKFLOWSTATE;
    }

    public void setWFDUMMIESWORKFLOWSTATE(String WFDUMMIESWORKFLOWSTATE) {
        this.WFDUMMIESWORKFLOWSTATE = WFDUMMIESWORKFLOWSTATE;
    }

    public String getSUBMITTINGDATE() {
        return SUBMITTINGDATE;
    }

    public void setSUBMITTINGDATE(String SUBMITTINGDATE) {
        this.SUBMITTINGDATE = SUBMITTINGDATE;
    }

    public String getPERSONNELNUMBER() {
        return PERSONNELNUMBER;
    }

    public void setPERSONNELNUMBER(String PERSONNELNUMBER) {
        this.PERSONNELNUMBER = PERSONNELNUMBER;
    }

    public String getLASTWORKINGDATE() {
        return LASTWORKINGDATE;
    }

    public void setLASTWORKINGDATE(String LASTWORKINGDATE) {
        this.LASTWORKINGDATE = LASTWORKINGDATE;
    }

    public String getEOSTRANSTYPE() {
        return EOSTRANSTYPE;
    }

    public void setEOSTRANSTYPE(String EOSTRANSTYPE) {
        this.EOSTRANSTYPE = EOSTRANSTYPE;
    }

    public String getDAPEOSREQID() {
        return DAPEOSREQID;
    }

    public void setDAPEOSREQID(String DAPEOSREQID) {
        this.DAPEOSREQID = DAPEOSREQID;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }
}
