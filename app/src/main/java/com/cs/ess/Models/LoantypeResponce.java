package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class LoantypeResponce {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("GetLoanTypeDetails")
        private List<GetLoanTypeDetails> GetLoanTypeDetails;

        public List<GetLoanTypeDetails> getGetLoanTypeDetails() {
            return GetLoanTypeDetails;
        }

        public void setGetLoanTypeDetails(List<GetLoanTypeDetails> GetLoanTypeDetails) {
            this.GetLoanTypeDetails = GetLoanTypeDetails;
        }
    }

    public static class GetLoanTypeDetails {
        @Expose
        @SerializedName("LOANDESCRIPTION")
        private String LOANDESCRIPTION;
        @Expose
        @SerializedName("LOANTYPEID")
        private String LOANTYPEID;

        public String getLOANDESCRIPTION() {
            return LOANDESCRIPTION;
        }

        public void setLOANDESCRIPTION(String LOANDESCRIPTION) {
            this.LOANDESCRIPTION = LOANDESCRIPTION;
        }

        public String getLOANTYPEID() {
            return LOANTYPEID;
        }

        public void setLOANTYPEID(String LOANTYPEID) {
            this.LOANTYPEID = LOANTYPEID;
        }
    }
}
