package com.cs.ess.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BenefitContractResponse {

    String DESERVEDEVERY;

    int PERCENT;

    String CURRENCYCODE;

    double Amount;

    String Allowancededuction;

    public String getDESERVEDEVERY() {
        return DESERVEDEVERY;
    }

    public void setDESERVEDEVERY(String DESERVEDEVERY) {
        this.DESERVEDEVERY = DESERVEDEVERY;
    }

    public int getPERCENT() {
        return PERCENT;
    }

    public void setPERCENT(int PERCENT) {
        this.PERCENT = PERCENT;
    }

    public String getCURRENCYCODE() {
        return CURRENCYCODE;
    }

    public void setCURRENCYCODE(String CURRENCYCODE) {
        this.CURRENCYCODE = CURRENCYCODE;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }

    public String getAllowancededuction() {
        return Allowancededuction;
    }

    public void setAllowancededuction(String Allowancededuction) {
        this.Allowancededuction = Allowancededuction;
    }

}
