package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Adapter.LeaveAttachmentAdapter;
import com.cs.ess.Models.AttachmentJSONlist;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.content.Intent.EXTRA_MIME_TYPES;

public class LeaveAttachmentActivity extends AppCompatActivity {

    SharedPreferences userPrefs;
    String userid, imagesName = "";
    ImageView mback_btn;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    Bitmap thumbnail1 = null, fileExtention = null;
    RecyclerView attachment_list;
    ImageView img;
    String ret, uriString, finalString;
    public static final int RESULT_OK = -1;
    Uri uri;
    byte[] fileBytes;
    StringBuilder total = new StringBuilder();
    byte[] bytes;
    ByteArrayOutputStream output;
    String imagePath1;
    String image1Str = "", requestid, requesttype;
    ArrayList<AttachmentJSONlist> attachmentJSONlists = new ArrayList<>();
    public static String mstr_comment;
    ArrayList<Drawable> image = new ArrayList<>();
    ArrayList<String> file_name = new ArrayList<>();
    LeaveAttachmentAdapter adapter;
    Button submit_attachment;
    public static int image_size = 0;
    String type = "";
    ArrayList<String> filetype = new ArrayList<>();


    String[] permissions = new String[]{
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leave_attachments);

        image.clear();
        attachmentJSONlists.clear();
        file_name.clear();
        filetype.clear();

        image.add(getResources().getDrawable(R.drawable.attachment_add));
        file_name.add("");
        filetype.add("");

        requestid = getIntent().getStringExtra("requestid");
        requesttype = getIntent().getStringExtra("requesttype");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userid = userPrefs.getString("UserId", "");


        mback_btn = (ImageView) findViewById(R.id.back_btn);
        attachment_list = (RecyclerView) findViewById(R.id.attachemt_list);
        img = (ImageView) findViewById(R.id.img);
        submit_attachment = (Button) findViewById(R.id.create);

        checkPermissions();

        mback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        submit_attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject parent = new JSONObject();

                try {
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < attachmentJSONlists.size(); i++) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("getBaseSixFour", attachmentJSONlists.get(i).getGetByteArray());
                        jsonObject.put("getFileExt", attachmentJSONlists.get(i).getGetFileExt());
                        jsonObject.put("getFileName", attachmentJSONlists.get(i).getGetFileName());
                        jsonObject.put("getNotes", attachmentJSONlists.get(i).getGetNotes());
                        jsonObject.put("getRequestId", attachmentJSONlists.get(i).getGetRequestId());
                        jsonObject.put("getRequestType", attachmentJSONlists.get(i).getGetRequestType());
                        jsonArray.put(jsonObject);
                    }

                    parent.put("Attachments", jsonArray);

//                    int maxLogSize = 1000;
//                    for (int i = 0; i <= parent.toString().length() / maxLogSize; i++) {
//                        int start = i * maxLogSize;
//                        int end = (i + 1) * maxLogSize;
//                        end = end > parent.toString().length() ? parent.toString().length() : end;
//                        Log.v("TAG", parent.toString().substring(start, end));
//                    }

                    Log.i("TAG", parent.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                new InsertRegistration().execute(parent.toString());

            }
        });

        attachment_list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        adapter = new LeaveAttachmentAdapter(LeaveAttachmentActivity.this, attachmentJSONlists, image, file_name, filetype, LeaveAttachmentActivity.this, userid);
        attachment_list.setAdapter(adapter);

        final int THUMBSIZE = 128;


    }

    public void initView1() {

        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);

    }

    public void initView2() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            AttachmentJSONlist attachmentJSONlist = new AttachmentJSONlist();

            ContentResolver cR = getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getExtensionFromMimeType(cR.getType(uri));


            thumbnail1 = null;
            Log.i("TAG", "type: " + type);

            if (type != null) {
                if (type.equals("png") || type.equals("jpg")) {
                    try {
                        thumbnail1 = MediaStore.Images.Media.getBitmap(LeaveAttachmentActivity.this.getContentResolver(), uri);
                        Log.i("TAG", "onActivityResult: " + uri);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            }


            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
            String pictureName = "";
            if (requesttype.equals("LeaveRequest")) {

                pictureName = "LeaveRequest" + "_" + timeStamp;

            } else if (requesttype.equals("LoanRequest")) {

                pictureName = "LoanRequest" + "_" + timeStamp;

            } else if (requesttype.equals("OvertimeRequest")) {

                pictureName = "OvertimeRequest" + "_" + timeStamp;

            } else if (requesttype.equals("HourlyLeaveRequest")) {

                pictureName = "HourlyLeaveRequest" + "_" + timeStamp;

            } else if (requesttype.equals("HRGeneralRequest")) {

                pictureName = "HRGeneralRequest" + "_" + timeStamp;

            } else if (requesttype.equals("EOSRequest")) {

                pictureName = "EOSRequest" + "_" + timeStamp;

            }

//            if (requesttype.equals("LeaveRequest")) {
//
//                pictureName = "LeaveRequest" + "_" + timeStamp + "." + type;
//
//            } else if (requesttype.equals("LoanRequest")) {
//
//                pictureName = "LoanRequest" + "_" + timeStamp + "." + type;
//
//            } else if (requesttype.equals("OvertimeRequest")) {
//
//                pictureName = "OvertimeRequest" + "_" + timeStamp + "." + type;
//
//            } else if (requesttype.equals("HourlyLeaveRequest")) {
//
//                pictureName = "HourlyLeaveRequest" + "_" + timeStamp + "." + type;
//
//            } else if (requesttype.equals("HRGeneralRequest")) {
//
//                pictureName = "HRGeneralRequest" + "_" + timeStamp + "." + type;
//
//            } else if (requesttype.equals("EOSRequest")) {
//
//                pictureName = "EOSRequest" + "_" + timeStamp + "." + type;
//
//            }


            uriString = uri.toString();


            File sdImageMainDirectory = new File("/sdcard/" + pictureName);

            sdImageMainDirectory = Environment.getExternalStorageDirectory();
//            File sdcard = Environment.getExternalStorageDirectory();
            File from = new File(sdImageMainDirectory, uriString);
            File to = new File(sdImageMainDirectory, pictureName);
            from.renameTo(to);

            FileOutputStream fileOutputStream = null;

            String nameFile = null;

            try {

                BitmapFactory.Options options = new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
                fileOutputStream = new FileOutputStream(sdImageMainDirectory);

                BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

                bos.flush();

                bos.close();

            } catch (FileNotFoundException e) {

                // TODO Auto-generated catch block

                e.printStackTrace();

            } catch (IOException e) {

                // TODO Auto-generated catch block

                e.printStackTrace();
            }


            Log.i("TAG", "To: " + to.toString().replace("/storage/emulated/0/", ""));
            Log.i("TAG", "From: " + from.toString());

            Log.d("data", "onActivityResult: uri" + uriString);
//            //            myFile = new File(uriString);
//            //            ret = myFile.getAbsolutePath();
//            //Fpath.setText(ret);

//
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                String path = ret;
                InputStream in = getContentResolver().openInputStream(uri);

                bytes = getBytes(in);
                Log.d("data", "onActivityResult: bytes size=" + bytes.length);

                Log.d("data", "onActivityResult: Base64string=" + Base64.encodeToString(bytes, Base64.DEFAULT));

                Log.d("data", "note" + mstr_comment);

                image_size = image_size + bytes.length;

                Log.i("TAG", "image_size: " + image_size);

                if (image_size >= 4000000) {

                    image_size = image_size - bytes.length;
                    Constants.showOneButtonAlertDialog("Attachment size should be lesser than or equal to 4 MB", "ESS", "ok", LeaveAttachmentActivity.this);

                } else {

                    attachmentJSONlist.setGetByteArray(Base64.encodeToString(bytes, Base64.DEFAULT));
                    attachmentJSONlist.setGetFileExt(type);
                    attachmentJSONlist.setGetFileName(pictureName);
                    attachmentJSONlist.setGetNotes(mstr_comment);
                    attachmentJSONlist.setGetRequestId(requestid);

                    if (requesttype.equals("LeaveRequest")) {

                        attachmentJSONlist.setGetRequestType("LeaveRequest");

                    } else if (requesttype.equals("LoanRequest")) {

                        attachmentJSONlist.setGetRequestType("LoanRequest");

                    } else if (requesttype.equals("OvertimeRequest")) {

                        attachmentJSONlist.setGetRequestType("OvertimeRequest");

                    } else if (requesttype.equals("HourlyLeaveRequest")) {

                        attachmentJSONlist.setGetRequestType("HourlyLeaveRequest");

                    } else if (requesttype.equals("HRGeneralRequest")) {

                        attachmentJSONlist.setGetRequestType("HRGeneralRequest");

                    } else if (requesttype.equals("EOSRequest")) {

                        attachmentJSONlist.setGetRequestType("EOSRequest");

                    }
                    attachmentJSONlist.setImage_size(bytes.length);

                    attachmentJSONlists.add(attachmentJSONlist);

//                    if (type.equals("png") || type.equals("jpg")) {
//                        byte[] imageBytes = baos.toByteArray();
//
//                        imageBytes = Base64.decode(Base64.encodeToString(bytes, Base64.DEFAULT), Base64.DEFAULT);
//                        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
//                        img.setImageBitmap(decodedImage);
//
//                    } else {
//
//                        FileOutputStream fos = null;
//                        try {
//                            if (Base64.encodeToString(bytes, Base64.DEFAULT) != null) {
//                                fos = openFileOutput("myPdf.pdf", Context.MODE_PRIVATE);
//                                byte[] decodedString = android.util.Base64.decode(Base64.encodeToString(bytes, Base64.DEFAULT), android.util.Base64.DEFAULT);
//                                fos.write(decodedString);
//                                fos.flush();
//                                fos.close();
//                            }
//
//                        } catch (Exception e) {
//
//                        } finally {
//                            if (fos != null) {
//                                fos = null;
//                            }
//                        }
//
//                        Uri path1 = Uri.fromFile(new File("myPdf.pdf"));
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setDataAndType(path1, "application/pdf");
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//                        try {
//                            startActivity(intent);
//                        }
//                        catch (ActivityNotFoundException e) {
//                            Toast.makeText(LeaveAttachmentActivity.this,
//                                    "No Application Available to View PDF",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//
//
//                    }
                    file_name.add(pictureName);
                    filetype.add(type);

                    for (int i = 0; i < attachmentJSONlists.size(); i++) {

                        Log.i("TAG", "GetFilename: " + attachmentJSONlists.get(i).getGetByteArray());

                    }

                    convertPixel();

                }


//                Bitmap thumbImage = ThumbnailUtils.extractThumbnail(
//                        BitmapFactory.decodeFile(uriString),
//
//                        60,
//                        60);
//
//                img.setImageBitmap(thumbImage);


//                InputStreamReader inputStreamReader = new InputStreamReader(in);
//                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                String line;
//                while ((line = bufferedReader.readLine()) != null) {
//                    total.append(line);
//                }
//
//                byte[] buffer = new byte[8192];
//                int bytesRead;
//                output = new ByteArrayOutputStream();
//                int count=0;
//                try {
//                    while ((bytesRead = in.read(buffer)) != -1) {
//                        output.write(buffer, 0, bytesRead);
//                        count++;
//                    }
//                } catch (IOException e) {
//                    Log.d("error byte", "onActivityResult: " + e.toString());
//                    e.printStackTrace();
//                }
//                bytes = output.toByteArray();
//
//                Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();
                // FileInputStream fis = new FileInputStream(new File(uri.getPath()));

                //                byte[] buf = new byte[1024];
//                int n;
//                while (-1 != (n = in.read(buf))) {
//                    baos.write(buf, 0, n);
//                }
//                fileBytes = baos.toByteArray();
//                //Log.i("ByteArray" + path + ">><<" + ret, "----" + fileBytes.toString());
//                finalString = fileBytes.toString();


            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                Log.d("error", "onActivityResult: " + e.toString());
            }
        }
    }

    private void convertPixel() {

        Drawable drawable = null;

        if (thumbnail1 == null) {

            drawable = getResources().getDrawable(R.drawable.attachment_empty_img);

        } else {

            thumbnail1 = Bitmap.createScaledBitmap(thumbnail1, 700, 500,
                    false);
            thumbnail1 = codec(thumbnail1, Bitmap.CompressFormat.PNG, 100);

            drawable = new BitmapDrawable(getResources(), thumbnail1);

        }
        image.add(drawable);

        attachment_list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        adapter = new LeaveAttachmentAdapter(LeaveAttachmentActivity.this, attachmentJSONlists, image, file_name, filetype, LeaveAttachmentActivity.this, userid);
        attachment_list.setAdapter(adapter);

//        imagePath1 = StoreByteImage(thumbnail1, "image");
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);

    }

    public String StoreByteImage(Bitmap bitmap, String type) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        String pictureName = "";
        if (type.equals("image")) {
            pictureName = "IMG_" + userid + "_" + timeStamp + ".jpg";
            if (imagesName.length() == 0) {
                imagesName = pictureName;
            } else {
                imagesName = imagesName + "," + pictureName;
            }
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        bytes = stream.toByteArray();
        image1Str = Base64.encodeToString(bytes, Base64.DEFAULT);

        File sdImageMainDirectory = new File("/sdcard/" + pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options = new BitmapFactory.Options();

			/*Bitmap myImage = BitmapFactory.decodeByteArray(imageData, 0,

			imageData.length,options);*/
            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IOException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private boolean checkPermissions() {


        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {

            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;

        }
        return true;

    }

    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus, response;
        ACProgressFlower dialog = null;

        InputStream inputStream = null;
        StringBuilder sb;


        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LeaveAttachmentActivity.this);
            dialog = new ACProgressFlower.Builder(LeaveAttachmentActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();


        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        String username = "admin";
                        String password = "admin";

                        String unp = username + ":" + password;

                        Log.i("TAG", "doInBackground: " + unp);

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.getAttachment);

                        Log.i("TAG", "doInBackground: " + Constants.getAttachment);

                        String encoded_login = Base64.encodeToString(unp.getBytes(), Base64.NO_WRAP);
                        httpPost.setHeader(new BasicHeader("Authorization", "Basic " + encoded_login));

                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LeaveAttachmentActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LeaveAttachmentActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {


                        Log.d("TAG", "onPostExecute: else " + result);
                        try {
                            JSONObject jo = new JSONObject(result);
                            try {
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LeaveAttachmentActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LeaveAttachmentActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }


                            } catch (JSONException je) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LeaveAttachmentActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws
            IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-16LE"), 8);
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();

//        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-16LE"), 8);
//        sb = new StringBuilder();
//        sb.append(reader.readLine() + "\n");
//
//        String line = "0";
//        while ((line = reader.readLine()) != null) {
//            sb.append(line + "\n");
//        }
//        is.close();
//        json = sb.toString();

        return result;

    }


}
