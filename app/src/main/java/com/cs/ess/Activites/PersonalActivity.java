package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;
import com.cs.ess.Utils.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PersonalActivity extends AppCompatActivity {

    ImageView back_btn;
    RelativeLayout personadetalislayout,leavebalancelayout, pay_slip, personal_layout,mybenefitlayout;
//    RelativeLayout businesstriplayout;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personalinfo_activity);

        back_btn=(ImageView)findViewById(R.id.back_btn);
        personadetalislayout =(RelativeLayout) findViewById(R.id.emeployeinfolayout);
        leavebalancelayout =(RelativeLayout) findViewById(R.id.leavebalancelayout);

        personal_layout = (RelativeLayout) findViewById(R.id.emepolyinfo);
        pay_slip = (RelativeLayout) findViewById(R.id.pay_slip);
        mybenefitlayout = (RelativeLayout) findViewById(R.id.customers_layout);
//        businesstriplayout = (RelativeLayout) findViewById(R.id.businesstriplayout);



       back_btn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });

        personadetalislayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this, PersonalDetailsActivity.class);
                startActivity(intent);
            }
        });
        leavebalancelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this, LeaveBalanceReportActivity.class);
                startActivity(intent);
            }
        });

        personal_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this, EmployeePersonalInfo.class);
                startActivity(intent);
            }
        });
        pay_slip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this, PaySlipActivity.class);
                startActivity(intent);
            }
        });
//        businesstriplayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(PersonalActivity.this, BusniessActivity.class);
//                startActivity(intent);
//            }
//        });

        mybenefitlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this, BenefitContractActivity.class);
                startActivity(intent);
            }
        });


        setTypeface();


    }
    private void setTypeface(){
        ((TextView)findViewById(R.id.detailstext)).setTypeface(Constants.getarbooldTypeFace(PersonalActivity.this));
        ((TextView)findViewById(R.id.benefittext)).setTypeface(Constants.getarbooldTypeFace(PersonalActivity.this));
        ((TextView)findViewById(R.id.paysliptext)).setTypeface(Constants.getarbooldTypeFace(PersonalActivity.this));
        ((TextView)findViewById(R.id.balancetext)).setTypeface(Constants.getarbooldTypeFace(PersonalActivity.this));
//        ((TextView)findViewById(R.id.emepolytext)).setTypeface(Constants.getarbooldTypeFace(PersonalActivity.this));
        ((TextView)findViewById(R.id.personalinfotext)).setTypeface(Constants.getarbooldTypeFace(PersonalActivity.this));

    }

}
