package com.cs.ess.Activites;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BusniessActivity extends AppCompatActivity {

    ImageView back_btn,startcalander,endcalendra;
    EditText vactionet;
    private int mYear, mMonth, mDay;
    private int mendYear, mendMonth, mendDay;
    Boolean isToday = false;
    TextView startdate,startdayyear,enddate,enddayyear,noofdays;
    String today,month,year;
    Date checkInDate, checkoutDate;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.busniess_trip_activity);

        back_btn=(ImageView)findViewById(R.id.back_btn);
        vactionet=(EditText)findViewById(R.id.edit_annualvacation);
        startcalander=(ImageView)findViewById(R.id.starttime);
        endcalendra=(ImageView)findViewById(R.id.endtime);
        startdate=(TextView)findViewById(R.id.startdate);
        startdayyear=(TextView)findViewById(R.id.startdayyear);
        enddate=(TextView)findViewById(R.id.enddate);
        enddayyear=(TextView)findViewById(R.id.enddayyear);
//        noofdays=(TextView)findViewById(R.id.noofdays);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        startcalander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(BusniessActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth )
                            {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;

                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                Date date = null;

                                try {
                                    date = dateFormat.parse(mDay + "-" + (mMonth+1) + "-" + mYear);
                                    checkInDate = date;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String date1, date2;

                                date1 = postFormater.format(date);
                                date2 = postFormater1.format(date);
                                startdate.setText(date1);
                                startdayyear.setText(date2);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();
            }
        });

        endcalendra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",Locale.US);

                mendYear = mYear;
                mendMonth = mMonth;
                mendDay = mDay;

                DatePickerDialog datePickerDialog = new DatePickerDialog(BusniessActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth )
                            {
                                SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;

                                } else {
                                    isToday = false;
                                }
                                mendYear = year;
                                mendDay = dayOfMonth;
                                mendMonth = monthOfYear;

                                Date date = null;

                                try {
                                    date = dateFormat.parse(mendDay + "-" + (mendMonth+1) + "-" + mendYear);
                                    checkoutDate = date;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String date1, date2;

                                date1 = postFormater.format(date);
                                date2 = postFormater1.format(date);
                                enddate.setText(date1);
                                enddayyear .setText(date2);

//                                CalculateNumOfDays();

                            }
                        }, mendYear, mendMonth, mendDay);

                Date date = null;

                try {
                    date = dateFormat.parse(mDay + "-" + (mMonth+1) + "-" + mYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();

            }
        });
    }

//    public void CalculateNumOfDays() {
//        long diff =checkoutDate.getTime() - checkInDate.getTime();
//        long NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
//        noofdays.setText("" + NumofDaysStr);
//    }
    }


