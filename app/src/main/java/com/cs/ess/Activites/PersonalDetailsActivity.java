package com.cs.ess.Activites;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Helper;

import com.cs.ess.JSONParser;
import com.cs.ess.Models.PersonalDetailsResponse;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient;
import com.cs.ess.Utils.BasicAuthInterceptor;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.cs.ess.Utils.ServiceHelper;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PersonalDetailsActivity extends AppCompatActivity {

    ImageView back_btn;
    TextInputEditText personal_no, worker_name, worker_name_ar, job, desc, datetime;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    String processResponse;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personaldetails_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        personal_no = (TextInputEditText) findViewById(R.id.personal_no);
        worker_name = (TextInputEditText) findViewById(R.id.worker_name);
        worker_name_ar = (TextInputEditText) findViewById(R.id.worker_name_ar);
        job = (TextInputEditText) findViewById(R.id.job);
        desc = (TextInputEditText) findViewById(R.id.desc);
        datetime = (TextInputEditText) findViewById(R.id.date_time);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        String networkStatus1 = NetworkUtil.getConnectivityStatusString(PersonalDetailsActivity.this);
//        if (!networkStatus1.equalsIgnoreCase("Not connected to Internet")) {
//            AuthanticationApi();
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }

        // Retrofit
//        String networkStatus = NetworkUtil.getConnectivityStatusString(PersonalDetailsActivity.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            PersonalDetailsApi();
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }

        String networkStatus = NetworkUtil.getConnectivityStatusString(PersonalDetailsActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

            new PersonalDetailsApi().execute(Constants.getpersonDetails + "?UserId=" + userId);

        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    public static String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (Constants.API_USER_NAME + ":" + Constants.API_PASSWORD).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public class PersonalDetailsApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(PersonalDetailsActivity.this);
            dialog = new ACProgressFlower.Builder(PersonalDetailsActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(PersonalDetailsActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(PersonalDetailsActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetPersonalInfo");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    personalNumber = jo2.getString("PersonalNumber");
                                    workerName = jo2.getString("WorkerName");
                                    workerNameAR = jo2.getString("WorkerNameAR");
                                    mjob = jo2.getString("Job");
                                    description = jo2.getString("Description");
                                    empstartdate = jo2.getString("Empstartdate");
                                }

                                Log.i("TAG", "onPostExecute: " + workerName);

                                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
                                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

                                String date = empstartdate;

                                try {
                                    Date datetime = sdf.parse(date);
                                    date = sdf1.format(datetime);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }



                                personal_no.setText("" + personalNumber);
                                worker_name.setText("" + workerName);
                                worker_name_ar.setText("" + workerNameAR);
                                job.setText("" + mjob);
                                desc.setText("" + description);
                                datetime.setText("" + date);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(PersonalDetailsActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

//    private void PersonalDetailsApi() {
////        final ACProgressFlower dialog = new ACProgressFlower.Builder(PersonalDetailsActivity.this)
////                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
////                .themeColor(Color.WHITE)
////                .fadeColor(Color.DKGRAY).build();
////
////        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(PersonalDetailsActivity.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<PersonalDetailsResponse> call = apiService.getpersonDetails(userId);
//
//        call.enqueue(new Callback<PersonalDetailsResponse>() {
//
//            public void onResponse(Call<PersonalDetailsResponse> call, Response<PersonalDetailsResponse> response) {
//                if (response.isSuccessful()) {
//                    PersonalDetailsResponse registrationResponse = response.body();
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            personal_no.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getPersonalNumber());
//                            worker_name.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
//                            worker_name_ar.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerNameAR());
//                            job.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getJob());
//                            desc.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
//                            datetime.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getEmpstartdate());
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
////                    if (dialog != null)
////                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
////                    if (dialog != null)
////                        dialog.dismiss();
//                    Toast.makeText(PersonalDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            public void onFailure(Call<PersonalDetailsResponse> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(PersonalDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(PersonalDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
////                if (dialog != null)
////                    dialog.dismiss();
//            }
//        });
//    }
}
