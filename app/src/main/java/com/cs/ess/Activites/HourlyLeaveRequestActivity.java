package com.cs.ess.Activites;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.Models.HourlyLeaveRequestResponse;
import com.cs.ess.Models.PersonalDetailsResponse;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient;
import com.cs.ess.Utils.NetworkUtil;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HourlyLeaveRequestActivity extends AppCompatActivity {

    ImageView backbtn,starttime,endtime,datepiker;
    TextView starttimetext,endtimetext,hours_Balance,datetext;
    Date checkInDate, checkoutDate;
    private int mYear, mMonth, mDay;
    private int mendYear, mendMonth, mendDay;
    Boolean isToday = false;
    String userId = "ialsulta";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hourlyleave_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
//        userId = userPrefs.getString("UserId", "");

        backbtn=(ImageView)findViewById(R.id.back_btn);
        starttime=(ImageView)findViewById(R.id.starttime);
        endtime=(ImageView)findViewById(R.id.endtime);
        datepiker=(ImageView)findViewById(R.id.datepiker);
        starttimetext=(TextView)findViewById(R.id.starttimetex);
        endtimetext=(TextView)findViewById(R.id.endtimetext);
        hours_Balance=(TextView)findViewById(R.id.text_Balance);
        datetext=(TextView)findViewById(R.id.datetext);

        String networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            HourlyLeaveRequestApi();
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

        datepiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(HourlyLeaveRequestActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth )
                            {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;

                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                Date date = null;

                                try {
                                    date = dateFormat.parse(mDay + "-" + (mMonth+1) + "-" + mYear);
                                    checkInDate = date;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String date1, date2;

                                date1 = postFormater.format(date);
                                date2 = postFormater1.format(date);
                                datetext.setText(date1);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();

            }
        });

        starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(HourlyLeaveRequestActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        starttimetext.setText(selectedHour + ":" + selectedMinute);

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        endtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(HourlyLeaveRequestActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        endtimetext.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

//                CalculateNumOfDays();
            }
        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
//    public void CalculateNumOfDays() {
//        long diff =checkoutDate.getTime() - checkInDate.getTime();
//        long NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
//        hours_Balance.setText("" + NumofDaysStr);
//    }

    private void HourlyLeaveRequestApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(MainActivity.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<HourlyLeaveRequestResponse> call = apiService.gethourly(userId);
        call.enqueue(new Callback<HourlyLeaveRequestResponse>() {

            public void onResponse(Call<HourlyLeaveRequestResponse> call, Response<HourlyLeaveRequestResponse> response) {
                if (response.isSuccessful()) {
                    HourlyLeaveRequestResponse registrationResponse = response.body();
                    try {
                        //status true case
                        if (registrationResponse.getMessage().equals("Success")) {

//                            userPrefsEditor.putString("Empname", registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
//                            userPrefsEditor.putString("Emppos", registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
//                            userPrefsEditor.commit();
//
//                            userName.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
//                            pos.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());

                        } else {

                        }
                    } catch (Exception e) {

                    }

//                    if (dialog != null)
//                        dialog.dismiss();
                } else {
                    try {
                        Log.d("TAG", "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    if (dialog != null)
//                        dialog.dismiss();
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<HourlyLeaveRequestResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

//                if (dialog != null)
//                    dialog.dismiss();
            }
        });
    }

}
