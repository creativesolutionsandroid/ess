package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EmepolyReportsActivity extends AppCompatActivity {


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    ImageView backbtn;
    RelativeLayout subordinatlayout,leavecalender_layout,outstandlayout,leaverequest_report_layout,loan_reaquest_report_layout,btrlayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_activity);

        backbtn=(ImageView)findViewById(R.id.back_btn);
        subordinatlayout=(RelativeLayout)findViewById(R.id.subordinatlayout);
        leavecalender_layout=(RelativeLayout)findViewById(R.id.leavecalender_layout);
        outstandlayout=(RelativeLayout)findViewById(R.id.outstandlayout);
        leaverequest_report_layout=(RelativeLayout)findViewById(R.id.leaverequest_report_layout);
        loan_reaquest_report_layout=(RelativeLayout)findViewById(R.id.loan_reaquest_report_layout);
        btrlayout=(RelativeLayout)findViewById(R.id.btrlayout);

        subordinatlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(EmepolyReportsActivity.this,MSSSubordinateActivity.class);
                startActivity(intent);
            }
        });
        leavecalender_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(EmepolyReportsActivity.this,MSSLeaveCalanerActivity.class);
                startActivity(intent);
            }
        });
        outstandlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(EmepolyReportsActivity.this,MSSOutStandingLoanActivity.class);
                startActivity(intent);
            }
        });
        leaverequest_report_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(EmepolyReportsActivity.this, MSSLeaveRequestReportActivity.class);
                startActivity(intent);
            }
        });

        loan_reaquest_report_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(EmepolyReportsActivity.this,MSSLoanRequestActivity.class);
                startActivity(intent);
            }
        });

        btrlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(EmepolyReportsActivity.this,BTRAmendmentRequestActivity.class);
                startActivity(intent);
            }
        });



        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
