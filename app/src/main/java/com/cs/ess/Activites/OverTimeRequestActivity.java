package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Adapter.OvertimeTypeAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.LoanRequestResponce;
import com.cs.ess.Models.OverTimeViewResponse;
import com.cs.ess.Models.OverTypeDetailsResponse;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OverTimeRequestActivity extends AppCompatActivity {

    ImageView backbtn, datepiker, mattachment;
    RelativeLayout dropdown;
    TextView datetext, text_id, overtimetypetext;
    private int mYear, mMonth, mDay;
    private int mendYear, mendMonth, mendDay;
    Boolean isToday = false;
    Spinner spinner_overtimetype;
    public static RelativeLayout hidden_layout;
    public static int alternate_pos = -1;
    OvertimeTypeAdapter madapter;
    TextView edit_name;
    String userid;
    SharedPreferences userPrefs;
    Button create;
    String str_overtype, str_no_of_hours, str_desc, str_date, str_loan_date, str_deduction_date;
    EditText no_of_hours, desc;

    private ArrayList<OverTimeViewResponse> loandetailsarray = new ArrayList<>();

    private ArrayList<OverTypeDetailsResponse.GetLeaveIdDetails> overtime_type = new ArrayList<>();
    ArrayList<String> overtimetypearry = new ArrayList<>();
    boolean leave_request = false, medit = false;
    int pos;
    RelativeLayout request_id;
    EditText edit_requestid;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overtime_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userid = userPrefs.getString("UserId", "");

        backbtn = (ImageView) findViewById(R.id.back_btn);
        datetext = (TextView) findViewById(R.id.datetext);
        overtimetypetext = (TextView) findViewById(R.id.overtimetypetext);
        datepiker = (ImageView) findViewById(R.id.datepiker);
        dropdown = (RelativeLayout) findViewById(R.id.layout);
        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);
        edit_name = (TextView) findViewById(R.id.edit_id);
        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.submit);
        no_of_hours = (EditText) findViewById(R.id.no_of_hours);
        desc = (EditText) findViewById(R.id.desc);
        edit_requestid = (EditText) findViewById(R.id.edit_requestid);

        request_id = (RelativeLayout) findViewById(R.id.request_id);

        mattachment = (ImageView) findViewById(R.id.attactment);


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            loandetailsarray = (ArrayList<OverTimeViewResponse>) getIntent().getSerializableExtra("list");
            pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
            if (loandetailsarray.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }

        if (leave_request) {

            if (!medit) {

                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                datepiker.setEnabled(false);
                dropdown.setEnabled(false);
                no_of_hours.setEnabled(false);
//                edit_cyclepayament.setEnabled(false);
                desc.setEnabled(false);
                mattachment.setVisibility(View.GONE);


            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);
                mattachment.setVisibility(View.VISIBLE);


            }

            request_id.setVisibility(View.VISIBLE);
            edit_requestid.setText("" + loandetailsarray.get(pos).getOVERTIMETRANSID());
            desc.setText("" + loandetailsarray.get(pos).getTRASNACTIONTXT());
            create.setText("SAVE");

            String date = loandetailsarray.get(pos).getTRANSDATE();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            try {
                Date datetime = sdf.parse(date);
                date = sdf1.format(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            datetext.setText("" + date);

            overtimetypetext.setText("" + loandetailsarray.get(pos).getOVERTIMETYPE());

            double loanvalue = Double.parseDouble(loandetailsarray.get(pos).getNUMBEROFHOURS());

            no_of_hours.setText("" + Constants.priceFormat1.format(loanvalue));

        } else {


            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy", Locale.US);

            String date1;

            Calendar calendar = Calendar.getInstance();

            date1 = postFormater.format(calendar.getTime());

            datetext.setText(date1);

            mattachment.setVisibility(View.GONE);
            request_id.setVisibility(View.GONE);

        }

        edit_name.setText("" + userPrefs.getString("Empname", ""));

        mattachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(OverTimeRequestActivity.this, LeaveAttachmentActivity.class);
                a.putExtra("requestid", loandetailsarray.get(pos).getOVERTIMETRANSID());
                a.putExtra("requesttype", "OvertimeRequest");
                startActivity(a);

            }
        });


        datepiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(OverTimeRequestActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                SimpleDateFormat postFormater = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);
                                SimpleDateFormat postFormater2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;

                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                Date date = null;

                                try {
                                    date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String date1, date2;

                                date1 = postFormater2.format(date);
                                date2 = postFormater1.format(date);
                                datetext.setText(date1);


                            }
                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();

            }
        });

        new OverTimeTypeApi().execute(Constants.getovertimetpes);

        dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final BubbleLayout bubbleLayout;
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(OverTimeRequestActivity.this).inflate(R.layout.branch_list_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(OverTimeRequestActivity.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    Constants.time = true;
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    madapter = new OvertimeTypeAdapter(OverTimeRequestActivity.this, overtimetypearry, popupWindow);
                    mbubble_List.setAdapter(madapter);
                    int[] location = new int[2];
                    dropdown.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(dropdown, Gravity.NO_GRAVITY, location[0], dropdown.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String networkStatus = NetworkUtil.getConnectivityStatusString(OverTimeRequestActivity.this);
//                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (leave_request) {
//
//                            String service_url = Constants.getOverTimeSubmit + "?overTimeTransId=" + loandetailsarray.get(pos).getOVERTIMETRANSID() + "&UserId=" + userid + "&Note=";
//                            String main_url = service_url.replaceAll(" ", "%20");
//                            new SubmitRequestApi().execute(main_url);
//                            Log.i("TAG", "onClick: " + main_url);
//
//                        }
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//
//            }
//        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(OverTimeRequestActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {

                            String service_url = Constants.getOverTimeCreate + "?_numberOfHours=" + str_no_of_hours + "&_dapOvertimeType=" + str_overtype + "&_transDate=" + str_date + "&_notes=" + str_desc + "&_userid=" + userid + "&overtimeTransId=" + loandetailsarray.get(pos).getOVERTIMETRANSID();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            String service_url = Constants.getOverTimeCreate + "?_numberOfHours=" + str_no_of_hours + "&_dapOvertimeType=" + str_overtype + "&_transDate=" + str_date + "&_notes=" + str_desc + "&_userid=" + userid + "&overtimeTransId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

    }

    private boolean Validation() {

        str_overtype = overtimetypetext.getText().toString();
        str_no_of_hours = no_of_hours.getText().toString();
        str_desc = desc.getText().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy", Locale.US);

        Date date = null;

        if (datetext.getText().toString().length() != 0) {
            try {
                date = sdf2.parse(datetext.getText().toString());
                str_date = sdf1.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

//        str_cycle_pay = edit_cyclepayament.getText().toString();


        if (str_overtype.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select overtime type", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OverTimeRequestActivity.this);

            return false;
        } else if (str_no_of_hours.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter no. of hours", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OverTimeRequestActivity.this);
            no_of_hours.requestFocus();

            return false;
        } else if (str_desc.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter description", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OverTimeRequestActivity.this);
            desc.requestFocus();

            return false;
        }


        return true;
    }

    public class OverTimeTypeApi extends AsyncTask<String, Integer, String> {
        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            overtime_type.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OverTimeRequestActivity.this);
            dialog = new ACProgressFlower.Builder(OverTimeRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OverTimeRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equals("")) {
                        Toast.makeText(OverTimeRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);
                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetLeaveIdDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    OverTypeDetailsResponse.GetLeaveIdDetails alternateemp = new OverTypeDetailsResponse.GetLeaveIdDetails();
                                    alternateemp.setOVERTIMETYPE(jo2.getString("OVERTIMETYPE"));
                                    alternateemp.setDESCRIPTION(jo2.getString("DESCRIPTION"));
                                    overtime_type.add(alternateemp);
                                    overtimetypearry.add(overtime_type.get(i).getOVERTIMETYPE());

                                }
                                Log.d("TAG", "onPostExecute: " + overtimetypearry.get(0));
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OverTimeRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LeaveRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);
                            } catch (JSONException je) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(OverTimeRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public class CreatBalanceApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OverTimeRequestActivity.this);
            dialog = new ACProgressFlower.Builder(OverTimeRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OverTimeRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OverTimeRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OverTimeRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.mover_submit = -1;
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OverTimeRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }


                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OverTimeRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(OverTimeRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(OverTimeRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OverTimeRequestActivity.this);
            dialog = new ACProgressFlower.Builder(OverTimeRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OverTimeRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OverTimeRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OverTimeRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OverTimeRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(OverTimeRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public void initView1() {
        overtimetypetext.setText("" + overtimetypearry.get(alternate_pos));
    }

}
