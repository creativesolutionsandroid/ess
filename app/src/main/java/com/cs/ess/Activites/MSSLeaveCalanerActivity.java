package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MSSLeaveCalanerActivity extends AppCompatActivity {
    Button submit;
    ImageView backbtn;
    LinearLayout layout2,one;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mss_calender_activity);

        backbtn=(ImageView)findViewById(R.id.back_btn);
        layout2=(LinearLayout) findViewById(R.id.layout2);
        one=(LinearLayout) findViewById(R.id.one);
        submit=(Button)findViewById(R.id.submit);


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MSSLeaveCalanerActivity.this,MssLeaveCalender2Activity.class);
                startActivity(intent);
            }
        });
    }
}
