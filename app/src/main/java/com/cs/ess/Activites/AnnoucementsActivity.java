package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;
import com.cs.ess.Utils.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AnnoucementsActivity extends AppCompatActivity {

    ImageView back_btn;
    LinearLayout layout1,layout2,layout3;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.annoucements_acrivity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        back_btn=(ImageView)findViewById(R.id.back_btn);
        layout1=(LinearLayout)findViewById(R.id.layout1);
        layout2=(LinearLayout)findViewById(R.id.layout2);
        layout3=(LinearLayout)findViewById(R.id.layout3);

        layout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AnnoucementsActivity.this,AttachmentsAnnoucementActivity.class));

            }
        });
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AnnoucementsActivity.this,AttachmentsAnnoucementActivity.class));

            }
        });
        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AnnoucementsActivity.this,AttachmentsAnnoucementActivity.class));

            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setTypeface();
    }
    private void setTypeface(){
        ((TextView)findViewById(R.id.requesttext)).setTypeface(Constants.getarbooldTypeFace(AnnoucementsActivity.this));
        ((TextView)findViewById(R.id.hrgernaltext)).setTypeface(Constants.getarbooldTypeFace(AnnoucementsActivity.this));
        ((TextView)findViewById(R.id.leavetext)).setTypeface(Constants.getarbooldTypeFace(AnnoucementsActivity.this));

    }
}
