package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;
import com.cs.ess.Utils.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RequestsActivity extends AppCompatActivity {

    ImageView back_btn;
    RelativeLayout leaverequestlayout, loanrequestlayout, hrgenrallayout, overtimelayout, hourlylayout, businesstriplayout;
    RelativeLayout eoslayout, clerlayout;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requests_activity);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        leaverequestlayout = (RelativeLayout) findViewById(R.id.leaverequest_layout);
        loanrequestlayout = (RelativeLayout) findViewById(R.id.loan_layout);
        hrgenrallayout = (RelativeLayout) findViewById(R.id.hrlayout);
        overtimelayout = (RelativeLayout) findViewById(R.id.overtimelayout);
        hourlylayout = (RelativeLayout) findViewById(R.id.hourlylayout);
        businesstriplayout =(RelativeLayout) findViewById(R.id.business_trip_layout);
        eoslayout =(RelativeLayout) findViewById(R.id.eos_layout);
        clerlayout =(RelativeLayout) findViewById(R.id.clerlayout);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        leaverequestlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, LeaveRequestActivity1.class);
                startActivity(intent);
            }
        });
        loanrequestlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, LoanReaquest_1Activity.class);
                startActivity(intent);
            }
        });
        hrgenrallayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, HRGeneralRequestActivtity1.class);
                startActivity(intent);
            }
        });
        overtimelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, OverTimeRequestActivtity1.class);
                startActivity(intent);
            }
        });
        hourlylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, HourlyLeaveRequestActivity1.class);
                startActivity(intent);
            }
        });
        businesstriplayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestsActivity.this, BusinessRetunActivity1.class);
                startActivity(intent);
            }
        });

        eoslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RequestsActivity.this, EOSActivity1.class);
                startActivity(intent);
            }
        });

        clerlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RequestsActivity.this, ClearanceActivity1.class);
                startActivity(intent);
            }
        });


        setTypeface();

    }

    private void setTypeface() {
        ((TextView) findViewById(R.id.loantext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView) findViewById(R.id.leavetext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView) findViewById(R.id.overtimetext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView) findViewById(R.id.hrtext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView) findViewById(R.id.hourlytext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView) findViewById(R.id.businesstext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView)findViewById(R.id.eostext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));
        ((TextView)findViewById(R.id.clearancetext)).setTypeface(Constants.getarbooldTypeFace(RequestsActivity.this));


    }

}
