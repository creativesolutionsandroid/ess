package com.cs.ess.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ReportFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.ess.Fragment.DashBoardFragment;
import com.cs.ess.Adapter.SideMenuAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.LoginResponse;
import com.cs.ess.Models.PersonalDetailsResponse;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class MainActivity extends AppCompatActivity {

    public static DrawerLayout drawer;
    private DrawerLayout mDrawerLayout;
    SideMenuAdapter mSideMenuAdapter;
    LinearLayout mDrawerLinear;
    String[] sideMenuItems;
    Integer[] sideMenuImages;
    ListView sideMenuListView;
    int itemSelectedPostion = 0;
    String fistname;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    private static final String TAG = "TAG";
    Boolean settingfragment;
    String userId;
    TextView userName, pos;
    boolean doubleBackToExitPressedOnce = false;

    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer = findViewById(R.id.drawer_layout);
        userName = (TextView) findViewById(R.id.username);
        pos = (TextView) findViewById(R.id.pos);


        Fragment dashfragmet = new DashBoardFragment();
        fragmentManager.beginTransaction().replace(R.id.fragment_layout, dashfragmet).commit();


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");


//        userName.setText(userPrefs.getString("name" ,null));

        sideMenuItems = new String[]{
                getResources().getString(R.string.menu_home),
                getResources().getString(R.string.menu_request),
                getResources().getString(R.string.menu_approvals),
                getResources().getString(R.string.menu_expenes),
                getResources().getString(R.string.menu_announcement),
                getResources().getString(R.string.menu_report),
                getResources().getString(R.string.menu_logout)
        };

//        getResources().getString(R.string.menu_setting),
//        getResources().getString(R.string.menu_about),
//        R.drawable.announcement2x,
//        R.drawable.approval2x,
        sideMenuImages = new Integer[]{
                R.drawable.home2x,
                R.drawable.globe2x,
                R.drawable.message_icon,
                R.drawable.chart2x,
                R.drawable.group2x,
                R.drawable.settings2x,
                R.drawable.logout2x};

        sideMenuListView = (ListView) findViewById(R.id.side_menu_list_view);
        mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);

        sideMenuListView.setAdapter(mSideMenuAdapter);
        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());

        String networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

            new PersonalApi().execute(Constants.getpersonDetails + "?UserId=" + userId);

        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    public class PersonalApi extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus, response;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
//            dialog = ProgressDialog.show(MainActivity.this, "",
//                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(MainActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(MainActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i(TAG, "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "", personalNumber = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetPersonalInfo");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    personalNumber = jo2.getString("PersonalNumber");
                                    workerName = jo2.getString("WorkerName");
                                    String workerNameAR = jo2.getString("WorkerNameAR");
                                    String job = jo2.getString("Job");
                                    description = jo2.getString("Description");
                                    String empstartdate = jo2.getString("Empstartdate");
                                }

                                Log.i(TAG, "onPostExecute: " + workerName);

                                userPrefsEditor.putString("Empname", workerName);
                                userPrefsEditor.putString("Emppos", description);
                                userPrefsEditor.putString("personalNo", personalNumber);
                                userPrefsEditor.commit();

                                userName.setText("" + workerName);
                                pos.setText("" + description);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(MainActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            itemSelectedPostion = position;
//            if (position != 4 && position != 6 && position != 7 && position != 8) {
//                mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
            mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
//            }
            sideMenuListView.setAdapter(mSideMenuAdapter);
            mSideMenuAdapter.notifyDataSetChanged();
        }
    }

    public void selectItem(int position) {
        switch (position) {

            case 0:
                Fragment dashfragmet = new DashBoardFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, dashfragmet).commit();
                drawer.closeDrawer(GravityCompat.START);
                break;


            case 1:
                Intent intent2 = new Intent(MainActivity.this, RequestsActivity.class);
                startActivity(intent2);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case 2:
                Intent intent6 = new Intent(MainActivity.this, ApprovalActivity.class);
                startActivity(intent6);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case 3:
                Intent intent3 = new Intent(MainActivity.this, ExpensesActivity.class);
                startActivity(intent3);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 4:
                Intent intent7 = new Intent(MainActivity.this, AnnoucementsActivity.class);
                startActivity(intent7);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 5:
                Intent intent5 = new Intent(MainActivity.this, EmepolyReportsActivity.class);
                startActivity(intent5);
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 6:
                Intent intent8 = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(intent8);
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public void menuClick() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }

    }

    public static String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (Constants.API_USER_NAME + ":" + Constants.API_PASSWORD).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



//    private void PersonalDetailsApi() {
////        final ACProgressFlower dialog = new ACProgressFlower.Builder(MainActivity.this)
////                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
////                .themeColor(Color.WHITE)
////                .fadeColor(Color.DKGRAY).build();
////
////        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(MainActivity.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<PersonalDetailsResponse> call = apiService.getpersonDetails(userId);
//        Log.i(TAG, "PersonalDetailsApi: " + apiService.getpersonDetails(userId));
//        call.enqueue(new Callback<PersonalDetailsResponse>() {
//
//            public void onResponse(Call<PersonalDetailsResponse> call, Response<PersonalDetailsResponse> response) {
//                Log.i(TAG, "onResponse1: " + response);
//                if (response.isSuccessful()) {
//                    PersonalDetailsResponse registrationResponse = response.body();
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
////                            userPrefsEditor.putString("Empname", registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
////                            userPrefsEditor.putString("Emppos", registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
////                            userPrefsEditor.commit();
////
////                            userName.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
////                            pos.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
////                    if (dialog != null)
////                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
////                    if (dialog != null)
////                        dialog.dismiss();
////                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            public void onFailure(Call<PersonalDetailsResponse> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
////                    Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
////                    Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
////                if (dialog != null)
////                    dialog.dismiss();
//            }
//        });
//    }

}
