package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.Models.LoginResponse;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient1;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignInActivity extends AppCompatActivity {

    EditText inputMobile, inputPassword;
    String strName, strPassword, strdevicedatetime, strDeviceVersion, strAppVersion;
    private Button continueBtn;
    private TextView forgotpassword;
    SharedPreferences userPrefs;
    ImageView showPassword;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputPassword = (EditText) findViewById(R.id.password);
        inputMobile = (EditText) findViewById(R.id.email);
        continueBtn = (Button) findViewById(R.id.getstart);
        showPassword = (ImageView) findViewById(R.id.eyeicon);

        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inputPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
                    inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                else{
                    inputPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                inputPassword.setSelection(inputPassword.length());
            }
        });


        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        signInApi();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        Calendar c = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);

        strdevicedatetime = dateFormat.format(c.getTime());
        strAppVersion = Constants.getDeviceType(SignInActivity.this);
        strDeviceVersion = "Android";

        strdevicedatetime = "2019/11/10";
        strDeviceVersion = "aa";
        strAppVersion = "12";

    }

    private boolean validations() {
        strName = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();

        if (strName.length() == 0) {
            inputMobile.setError(getResources().getString(R.string.signup_msg_enter_name));

            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        } else if (strPassword.length() == 0) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));

            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));

            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        return true;
    }

    private void signInApi() {
        final ACProgressFlower dialog = new ACProgressFlower.Builder(SignInActivity.this)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();

        dialog.show();
        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient1.getClient().create(APIInterface.class);
        Call<LoginResponse> call = apiService.getLogin(strName, strPassword, strdevicedatetime, strDeviceVersion, strAppVersion.replace(" ", ""));
        Log.i("TAG", "signInApi: " + strName+ " " + strPassword + " " +  strdevicedatetime + " " + strDeviceVersion + " " + strAppVersion.replace(" ", ""));
        call.enqueue(new Callback<LoginResponse>() {

            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    LoginResponse registrationResponse = response.body();
                    try {
                        //status true case
                        Log.i("TAG", "onResponse: " + registrationResponse.getStatus());
                        if (registrationResponse.getStatus()) {
                            String userId = strName;
                            userPrefsEditor.putString("UserId", userId);
                            userPrefsEditor.commit();
                            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Constants.showOneButtonAlertDialog(registrationResponse.getMessage(),
                                    getResources().getString(R.string.app_name), "ok", SignInActivity.this);
                        }
                    } catch (Exception e) {
                        Constants.showOneButtonAlertDialog(registrationResponse.getMessage(),
                                getResources().getString(R.string.app_name), "ok", SignInActivity.this);
                    }

                    if (dialog != null)
                        dialog.dismiss();
                } else {
                    try {
                        Log.d("TAG", "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (dialog != null)
                        dialog.dismiss();
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
            }

            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

                if (dialog != null)
                    dialog.dismiss();
            }
        });
    }

}
