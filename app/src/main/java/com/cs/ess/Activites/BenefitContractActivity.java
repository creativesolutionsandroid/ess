package com.cs.ess.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Adapter.BenefitContractAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.BenefitContractResponse;
import com.cs.ess.Models.GetBenefitContractDetailsResponse;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BenefitContractActivity extends AppCompatActivity {

    ImageView back_btn;
    String userId;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    TextView personal_no;
    TextView name, contract_no, contract_period, basic_salary, employee_grade, valid_from, valid_to, contract_status;
    RecyclerView list_items;
//    TextView pay_group;
    BenefitContractAdapter mAdapter;
    ArrayList<BenefitContractResponse> benefitContractsDetailsLines = new ArrayList<>();
    ArrayList<GetBenefitContractDetailsResponse> benefitContractsDetails = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.benefit_contract);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");

        personal_no = (TextView) findViewById(R.id.personnel_no);
        name = (TextView) findViewById(R.id.name);
        contract_no = (TextView) findViewById(R.id.contract_no);
//        contract_period = (TextView) findViewById(R.id.contract_period);
        basic_salary = (TextView) findViewById(R.id.basic_salary);
        employee_grade = (TextView) findViewById(R.id.employee_grade);
//        pay_group = (TextView) findViewById(R.id.pay_group);
        valid_from = (TextView) findViewById(R.id.valid_from);
        valid_to = (TextView) findViewById(R.id.valid_to);
        contract_status = (TextView) findViewById(R.id.contract_status);

        list_items = (RecyclerView) findViewById(R.id.list_item);

        back_btn = (ImageView) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });


        String networkStatus = NetworkUtil.getConnectivityStatusString(BenefitContractActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new BenefitContractApi().execute(Constants.getbenefitcontract + "?UserId=" + userId);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    public class BenefitContractApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            benefitContractsDetailsLines.clear();
            benefitContractsDetails.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(BenefitContractActivity.this);
            dialog = new ACProgressFlower.Builder(BenefitContractActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(BenefitContractActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(BenefitContractActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");


                                JSONArray ja = jo1.getJSONArray("GetBenefitContractsDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);

                                    GetBenefitContractDetailsResponse leaveRequestGetResponse = new GetBenefitContractDetailsResponse();
                                    leaveRequestGetResponse.setCONTRACTNUM(jo2.getString("CONTRACTNUM"));
                                    leaveRequestGetResponse.setCONTRACTPERIODS(jo2.getString("CONTRACTPERIODS"));
                                    leaveRequestGetResponse.setBASICSALARY(jo2.getInt("BASICSALARY"));
                                    leaveRequestGetResponse.setDATAAREAID(jo2.getString("DATAAREAID"));
                                    leaveRequestGetResponse.setPERSONNELNUMBER(jo2.getString("PERSONNELNUMBER"));
                                    leaveRequestGetResponse.setNAME(jo2.getString("NAME"));
                                    leaveRequestGetResponse.setValidFrom(jo2.getString("ValidFrom"));
                                    leaveRequestGetResponse.setValidTo(jo2.getString("ValidTo"));
                                    leaveRequestGetResponse.setARABICNAME(jo2.getString("ARABICNAME"));

                                    benefitContractsDetails.add(leaveRequestGetResponse);
                                    Log.i("TAG", "leaverequest: " + benefitContractsDetails.get(i).getValidTo());
                                }

                                JSONArray ja1 = jo1.getJSONArray("GetBenefitContractsDetailsLines");
                                for (int i = 0; i < ja1.length(); i++) {
                                    JSONObject jo2 = ja1.getJSONObject(i);

                                    BenefitContractResponse leaveRequestGetResponse = new BenefitContractResponse();
                                    leaveRequestGetResponse.setAllowancededuction(jo2.getString("Allowancededuction"));
                                    leaveRequestGetResponse.setAmount(jo2.getDouble("Amount"));
                                    leaveRequestGetResponse.setCURRENCYCODE(jo2.getString("CURRENCYCODE"));
                                    leaveRequestGetResponse.setPERCENT(jo2.getInt("PERCENT"));
                                    leaveRequestGetResponse.setDESERVEDEVERY(jo2.getString("DESERVEDEVERY"));

                                    benefitContractsDetailsLines.add(leaveRequestGetResponse);
                                    Log.i("TAG", "leaverequest: " + benefitContractsDetailsLines.get(i).getAmount());
                                }

                                personal_no.setText("" + benefitContractsDetails.get(0).getPERSONNELNUMBER());
                                Log.i("TAG", "onPostExecute: " + benefitContractsDetails.get(0).getPERSONNELNUMBER());
                                name.setText("" + benefitContractsDetails.get(0).getNAME());
                                contract_no.setText("" + benefitContractsDetails.get(0).getCONTRACTNUM());
//                                contract_period.setText("" + benefitContractsDetails.get(0).getCONTRACTPERIODS());

                                double basicsalary = benefitContractsDetails.get(0).getBASICSALARY();

                                basic_salary.setText("" + Constants.priceFormat1.format(basicsalary));
//                            employee_grade.setText("" + benefitContractsDetails.get(0).get());
//                            pay_group.setText("" + benefitContractsDetails.get(0).getPERSONNELNUMBER());
                                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
                                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

                                String date = benefitContractsDetails.get(0).getValidFrom();
                                String date1 = benefitContractsDetails.get(0).getValidTo();

                                try {
                                    Date datetime = sdf.parse(date);
                                    Date datetime1 = sdf.parse(date1);
                                    date = sdf1.format(datetime);
                                    date1 = sdf1.format(datetime1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                valid_from.setText("" + date);
                                valid_to.setText("" + date1);
//                            contract_status.setText("" + benefitContractsDetails.get(0).getPERSONNELNUMBER());

                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(BenefitContractActivity.this);
                                list_items.setLayoutManager(mLayoutManager);
                                list_items.setLayoutManager(new GridLayoutManager(BenefitContractActivity.this, 1));
                                mAdapter = new BenefitContractAdapter(BenefitContractActivity.this, benefitContractsDetailsLines);
                                list_items.setAdapter(mAdapter);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(BenefitContractActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

//    private void BenefitContractApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(BenefitContractActivity.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(BenefitContractActivity.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<BenefitContractResponse> call = apiService.getbenefit(userId);
//
//        call.enqueue(new Callback<BenefitContractResponse>() {
//
//            public void onResponse(Call<BenefitContractResponse> call, Response<BenefitContractResponse> response) {
//                if (response.isSuccessful()) {
//                    BenefitContractResponse registrationResponse = response.body();
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            personal_no.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getPERSONNELNUMBER());
//                            name.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getNAME());
//                            contract_no.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getCONTRACTNUM());
//                            contract_period.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getCONTRACTPERIODS());
//                            basic_salary.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getBASICSALARY());
////                            employee_grade.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).get());
////                            pay_group.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getPERSONNELNUMBER());
//                            valid_from.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getValidFrom());
//                            valid_to.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getValidTo());
////                            contract_status.setText("" + registrationResponse.getData().getGetBenefitContractsDetails().get(0).getPERSONNELNUMBER());
//
//                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(BenefitContractActivity.this);
//                            list_items.setLayoutManager(new GridLayoutManager(BenefitContractActivity.this,1 ));
//                            mAdapter = new BenefitContractAdapter(BenefitContractActivity.this, registrationResponse.getData().getGetBenefitContractsDetailsLines());
//                            list_items.setAdapter(mAdapter);
//
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    if (dialog != null)
//                        dialog.dismiss();
//                    Toast.makeText(BenefitContractActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            public void onFailure(Call<BenefitContractResponse> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(BenefitContractActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(BenefitContractActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }
}
