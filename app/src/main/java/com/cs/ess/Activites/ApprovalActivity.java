package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;
import com.cs.ess.Utils.Constants;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ApprovalActivity extends AppCompatActivity {

    ImageView back_btn;
    LinearLayout hourly_layout,business_layout,overtime_layout,hr_layout,leave_laout,loan_layout;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approval_activity);
        back_btn=(ImageView)findViewById(R.id.back_btn);
        hourly_layout=(LinearLayout)findViewById(R.id.hourly_layout);
        business_layout=(LinearLayout)findViewById(R.id.business_layout);
        overtime_layout=(LinearLayout)findViewById(R.id.overtime_layout);
        hr_layout=(LinearLayout)findViewById(R.id.hr_layout);
        leave_laout=(LinearLayout)findViewById(R.id.leave_laout);
        loan_layout=(LinearLayout)findViewById(R.id.loan_layout);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        loan_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(ApprovalActivity.this, LoanReaquest_1Activity.class);
                a.putExtra("approval",true);
                startActivity(a);
            }
        });
        leave_laout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(ApprovalActivity.this, LeaveRequestActivity1.class);
                a.putExtra("approval",true);
                startActivity(a);

            }
        });
        hr_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(ApprovalActivity.this, HRGeneralRequestActivtity1.class);
                a.putExtra("approval",true);
                startActivity(a);

            }
        });
        overtime_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(ApprovalActivity.this, OverTimeRequestActivtity1.class);
                a.putExtra("approval",true);
                startActivity(a);

            }
        });
        business_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(ApprovalActivity.this, BusinessRetunActivity1.class);
                a.putExtra("approval",true);
                startActivity(a);

            }
        });
        hourly_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(ApprovalActivity.this, HourlyLeaveRequestActivity1.class);
                a.putExtra("approval",true);
                startActivity(a);

            }
        });

        setTypeface();
    }

    private void setTypeface(){
        ((TextView)findViewById(R.id.requesttext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.hrgernaltext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.leavetext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.loanrequesttext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.eostext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.hourlytext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.return_text)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));
        ((TextView)findViewById(R.id.clearancetext)).setTypeface(Constants.getarbooldTypeFace(ApprovalActivity.this));

    }

}
