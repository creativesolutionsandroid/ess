package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.Adapter.EOSRequestTypeAdapter;
import com.cs.ess.Adapter.HourlyPermissonTypeAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.EOSDetailsResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EOSRequestActivity2 extends AppCompatActivity {

    String userId;
    ImageView back_btn, datepiker, mattachment;
    SharedPreferences userPrefs;
    TextView submit_date, text_pid, last_working_date;
    EditText notes_input, edit_id;
    Button create;
    ArrayList<EOSDetailsResponce> eosDetailsResponces = new ArrayList<>();
    int pos;
    boolean medit = false, leave_request = false;
    RelativeLayout request_id_layout;
    String str_submit_date, str_last_working_day, str_trans_type, str_note;
    private int mYear, mMonth, mDay;
    boolean isToday = false;
    public static RelativeLayout hidden_layout;
    public static int alternate_pos = -1;
    ArrayList<String> eos_type = new ArrayList<>();
    EOSRequestTypeAdapter madapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eos_screen2);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("UserId", "");


        back_btn = (ImageView) findViewById(R.id.back_btn);
        datepiker = (ImageView) findViewById(R.id.datepiker);
        mattachment = (ImageView) findViewById(R.id.attactment);

        submit_date = (TextView) findViewById(R.id.submit_date);
        text_pid = (TextView) findViewById(R.id.text_pid);
        last_working_date = (TextView) findViewById(R.id.datetext);

        notes_input = (EditText) findViewById(R.id.notes_input);
        edit_id = (EditText) findViewById(R.id.edit_id);

        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.sumbit);

        request_id_layout = (RelativeLayout) findViewById(R.id.request_id_layout);
        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);

        try {
            eosDetailsResponces = (ArrayList<EOSDetailsResponce>) getIntent().getSerializableExtra("stores");
            pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
            if (eosDetailsResponces.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }

        if (leave_request) {

            request_id_layout.setVisibility(View.VISIBLE);

            if (!medit) {

                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                notes_input.setEnabled(false);
                text_pid.setEnabled(false);
                datepiker.setEnabled(false);
//                edit_personalnumber.setEnabled(false);
                edit_id.setEnabled(false);
                mattachment.setVisibility(View.GONE);


            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);
                mattachment.setVisibility(View.VISIBLE);

            }
            create.setText("SAVE");


            edit_id.setText("" + eosDetailsResponces.get(pos).getDAPEOSREQID());
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

            Date date = null, date1 = null;

            try {
                date = dateFormat.parse(eosDetailsResponces.get(pos).getLASTWORKINGDATE());
                date1 = dateFormat.parse(eosDetailsResponces.get(pos).getSUBMITTINGDATE());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            last_working_date.setText("" + dateFormat1.format(date));
            submit_date.setText("" + dateFormat1.format(date1));

            notes_input.setText("" + eosDetailsResponces.get(pos).getNOTES());
            text_pid.setText("" + eosDetailsResponces.get(pos).getEOSTRANSTYPE());


        } else {

            request_id_layout.setVisibility(View.GONE);

            create.setText("CREATE");
            create.setVisibility(View.VISIBLE);
//            submit.setVisibility(View.GONE);
            mattachment.setVisibility(View.GONE);

        }


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();

        submit_date.setText("" + dateFormat.format(calendar.getTime()));

        last_working_date.setText("" + dateFormat.format(calendar.getTime()));

        datepiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(EOSRequestActivity2.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;

                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                Date date = null;

                                try {
                                    date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String date1, date2;

                                date1 = postFormater.format(date);
                                date2 = postFormater1.format(date);
                                last_working_date.setText(date1);


                            }
                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();

            }
        });

        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidden_layout.setVisibility(View.GONE);

            }
        });

        eos_type.add("End of Contract");
        eos_type.add("Resignation");

        text_pid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final BubbleLayout bubbleLayout;
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(EOSRequestActivity2.this).inflate(R.layout.branch_list_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(EOSRequestActivity2.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    madapter = new EOSRequestTypeAdapter(EOSRequestActivity2.this, eos_type, popupWindow);
                    mbubble_List.setAdapter(madapter);
                    Log.i("TAG", "onClick: " + eos_type.get(0));
                    int[] location = new int[2];
                    text_pid.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(text_pid, Gravity.NO_GRAVITY, location[0], text_pid.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        mattachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(EOSRequestActivity2.this, LeaveAttachmentActivity.class);
                a.putExtra("requestid", eosDetailsResponces.get(pos).getDAPEOSREQID());
                a.putExtra("requesttype", "EOSRequest");
                startActivity(a);

            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String service_url = Constants.geteosSubmit + "?dapEOSReqId=" + eosDetailsResponces.get(pos).getDAPEOSREQID() + "&UserId=" + userId + "&Note=";
//                    String main_url = service_url.replaceAll(" ", "%20");
//                    new SubmitRequestApi().execute(main_url);
//                    Log.i("TAG", "onClick: " + main_url);
//                }
//
//            }
//        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(EOSRequestActivity2.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {



                            String service_url = Constants.geteoscreate + "?_dapEosTransType=" + str_trans_type + "&_lastWorkingDate=" + str_last_working_day + "&_notes=" + str_note + "&_UserId=" + userId + "&dapEOSReqId=" + eosDetailsResponces.get(pos).getDAPEOSREQID();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            String service_url = Constants.geteoscreate + "?_dapEosTransType=" + str_trans_type + "&_lastWorkingDate=" + str_last_working_day + "&_notes=" + str_note + "&_UserId=" + userId + "&dapEOSReqId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });


    }

    private boolean Validation() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy",Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy",Locale.US);

        Date date = null, date1 = null;

        try {
            date = dateFormat.parse(submit_date.getText().toString());
            date1 = dateFormat.parse(last_working_date.getText().toString());
            str_submit_date = dateFormat1.format(date);
            str_last_working_day = dateFormat1.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        str_trans_type = text_pid.getText().toString();
        str_note = notes_input.getText().toString();

        if (str_last_working_day.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select last working day", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EOSRequestActivity2.this);

            return false;
        } else if (str_trans_type.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select transation type", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EOSRequestActivity2.this);

            return false;
        } else if (str_note.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter notes", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EOSRequestActivity2.this);

            return false;
        }

        return true;
    }

    public class CreatBalanceApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EOSRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(EOSRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(EOSRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(EOSRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EOSRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.meos_submit = -1;
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EOSRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(EOSRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EOSRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(EOSRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(EOSRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(EOSRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EOSRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EOSRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(EOSRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public void initView() {
        text_pid.setText("" + eos_type.get(alternate_pos));
    }

}
