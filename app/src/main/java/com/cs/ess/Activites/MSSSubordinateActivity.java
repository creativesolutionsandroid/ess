package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MSSSubordinateActivity extends AppCompatActivity {

    ImageView backbtn;
    LinearLayout layout1,layout2, one;
    Button submit;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.management_subordinate_report);
        backbtn=(ImageView)findViewById(R.id.back_btn);
        submit=(Button)findViewById(R.id.submit);
        layout1=(LinearLayout)findViewById(R.id.layout1);
        layout2=(LinearLayout) findViewById(R.id.layout2);
        one=(LinearLayout) findViewById(R.id.one);

        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.GONE);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MSSSubordinateActivity.this, MSSSubordinateServiceActivity.class));
            }
        });


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
