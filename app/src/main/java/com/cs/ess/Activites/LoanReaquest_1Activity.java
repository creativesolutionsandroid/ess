package com.cs.ess.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cs.ess.Adapter.BenefitContractAdapter;
import com.cs.ess.Adapter.LeaveRequestAdapter;
import com.cs.ess.Adapter.LoanDetailsAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.LeaveRequestGetResponse;
import com.cs.ess.Models.LoanRequestResponce;
import com.cs.ess.Models.LoantypeResponce;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class LoanReaquest_1Activity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ImageView backbtn, pluse;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    RecyclerView listView;
    LoanDetailsAdapter mAdpter;
    TextView intent;
    SwipeRefreshLayout swipe_refresh;
    LoanDetailsAdapter loanDetailsAdapter;
    boolean approvel = false;

    private ArrayList<LoanRequestResponce> loandetailsarray = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.loanrequest_activity1);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");

        try {
            approvel = getIntent().getBooleanExtra("approval",false);
        } catch (Exception e) {
            e.printStackTrace();
            approvel = false;
        }

        backbtn = (ImageView) findViewById(R.id.back_btn);
        pluse = (ImageView) findViewById(R.id.pluse);
        listView = (RecyclerView) findViewById(R.id.listview);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
//        refresh = (ImageView) findViewById(R.id.refresh);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        swipe_refresh.setOnRefreshListener(LoanReaquest_1Activity.this);
        swipe_refresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

        pluse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoanReaquest_1Activity.this, LoanReaquestActivity2.class));
            }
        });

//        String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquest_1Activity.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new LoanRequestApi().execute(Constants.getLoanview + "?UserId=" + userId);
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }

//        refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquest_1Activity.this);
//                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    new LoanRequestApi().execute(Constants.getLoanview + "?UserId=" + userId);
//                } else {
//                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });

    }

    @Override
    public void onRefresh() {

        loadRecyclerViewData();

    }

    private void loadRecyclerViewData() {

        String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquest_1Activity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new LoanRequestApi().execute(Constants.getLoanview + "?UserId=" + userId);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    public class LoanRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
//        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            loandetailsarray.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquest_1Activity.this);
//            dialog = new ACProgressFlower.Builder(LoanReaquest_1Activity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoanReaquest_1Activity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LoanReaquest_1Activity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                loandetailsarray.clear();
                                JSONArray ja = jo1.getJSONArray("GetLoanRequestDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    LoanRequestResponce leaveRequestGetResponse = new LoanRequestResponce();
                                    leaveRequestGetResponse.setLOANREQUESTID(jo2.getString("LOANREQUESTID"));
                                    leaveRequestGetResponse.setLOANTYPE(jo2.getString("LOANTYPE"));
                                    leaveRequestGetResponse.setLOANDATE(jo2.getString("LOANDATE"));
                                    leaveRequestGetResponse.setDEDUCTIONSTARTDATE(jo2.getString("DEDUCTIONSTARTDATE"));
                                    leaveRequestGetResponse.setLOANVALUE(jo2.getString("LOANVALUE"));
                                    leaveRequestGetResponse.setLOANMONTHLYPAY(jo2.getString("LOANMONTHLYPAY"));
                                    leaveRequestGetResponse.setWORKFLOWSTATE(jo2.getString("WORKFLOWSTATE"));
                                    leaveRequestGetResponse.setWORKFLOWSTATENUM(jo2.getString("WORKFLOWSTATENUM"));
                                    leaveRequestGetResponse.setNAME(jo2.getString("NAME"));
                                    leaveRequestGetResponse.setPRPOSEOFLOAN(jo2.getString("PRPOSEOFLOAN"));
                                    leaveRequestGetResponse.setDATAAREAID(jo2.getString("DATAAREAID"));
                                    leaveRequestGetResponse.setmSubmit(true);

                                    loandetailsarray.add(leaveRequestGetResponse);
                                    Log.i("TAG", "leaverequest: " + loandetailsarray.get(i).getLOANREQUESTID());
                                }


                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LoanReaquest_1Activity.this);
                                listView.setLayoutManager(linearLayoutManager);
                                loanDetailsAdapter = new LoanDetailsAdapter(LoanReaquest_1Activity.this, loandetailsarray, LoanReaquest_1Activity.this, userId);
                                listView.setAdapter(loanDetailsAdapter);

                                swipe_refresh.setRefreshing(false);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LoanReaquest_1Activity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

//    private void PersonalDetailsApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(LoanReaquest_1Activity.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquest_1Activity.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<LoanRequestResponce> call = apiService.getloan(userId);
//
//        call.enqueue(new Callback<LoanRequestResponce>() {
//
//            public void onResponse(Call<LoanRequestResponce> call,Response<LoanRequestResponce>response) {
//                if (response.isSuccessful()) {
//                    LoanRequestResponce registrationResponse = response.body();
//                    Log.d(TAG, "LoanResponse: "+response.message());
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            loandetailsarray.get(0).getGetLoanRequestDetails();
//                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LoanReaquest_1Activity.this);
//                            listView.setLayoutManager(new GridLayoutManager(LoanReaquest_1Activity.this,1 ));
//                            mAdpter = new LoanDetailsAdapter(LoanReaquest_1Activity.this, loandetailsarray.get(0).getGetLoanRequestDetails());
//                            listView.setAdapter(mAdpter);
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    if (dialog != null)
//                        dialog.dismiss();
//                    Toast.makeText(LoanReaquest_1Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//            public void onFailure(Call<LoanRequestResponce> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(LoanReaquest_1Activity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(LoanReaquest_1Activity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }

    public void initView() {

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

        @Override
    protected void onResume() {
        super.onResume();

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
        Log.i("TAG", "onResume: " + userId);
    }

}
