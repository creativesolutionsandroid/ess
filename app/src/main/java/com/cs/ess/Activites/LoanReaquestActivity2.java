package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Adapter.LoanTypeApdater;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.LoanBalanceDetailsResponse;
import com.cs.ess.Models.LoanRequestResponce;
import com.cs.ess.Models.LoanTypeDetailsResponse;
import com.cs.ess.Models.LoantypeResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class LoanReaquestActivity2 extends AppCompatActivity {

    ImageView backbtn, mattachment;
    TextView date_ddeductiondate, date_loandate;
    //    EditText edit_cyclepayament;
    EditText edit_loanvalu;
    EditText edit_loanid, edit_pesronalnumber, edit_name, date_loanperpose, spintext;
    String dapLoanTypeId, dapLoanValue, dapLoanDate, deductionStartDate, purposeOfLoan, userid, loanRequestId;
//    Button submit;
    //    Spinner spinner_loantype;
    RelativeLayout loanid, personal_no_layout;
    public static RelativeLayout hidden_layout;
    int pos;
    private int LoantypeSelected = -1;
    public static int alternate_pos;

    private ArrayList<LoanRequestResponce> loandetailsarray = new ArrayList<>();
    ArrayList<LoanTypeDetailsResponse> loanTypeDetailsResponses = new ArrayList<>();
    ArrayList<LoanBalanceDetailsResponse> loanBalanceDetailsResponses = new ArrayList<>();
    ArrayList<String> loantype = new ArrayList<>();
    LoanTypeApdater branchadapter;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String str_loantype, str_purpose_of_loan, str_loan_value, str_cycle_pay, str_loan_date, str_deduction_date;
    Button create;
    boolean leave_request = false, medit = false;
    RelativeLayout loanid_layout;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_activity);

        try {
            loandetailsarray = (ArrayList<LoanRequestResponce>) getIntent().getSerializableExtra("stores");
            pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
            if (loandetailsarray.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }

        backbtn = (ImageView) findViewById(R.id.back_btn);
        edit_loanid = (EditText) findViewById(R.id.edit_loanid);
        edit_pesronalnumber = (EditText) findViewById(R.id.edit_pesronalnumber);
        edit_name = (EditText) findViewById(R.id.edit_name);
        date_loanperpose = (EditText) findViewById(R.id.date_loanperpose);
        date_loandate = (TextView) findViewById(R.id.date_loandate);
//        edit_cyclepayament = (EditText) findViewById(R.id.edit_cyclepayament);
        edit_loanvalu = (EditText) findViewById(R.id.edit_loanvalu);
        date_ddeductiondate = (TextView) findViewById(R.id.date_ddeductiondate);
//        submit = (Button) findViewById(R.id.submit);
        spintext = (EditText) findViewById(R.id.text1_loantype);
        personal_no_layout = (RelativeLayout) findViewById(R.id.personal_no_layout);
        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);
        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.sumbit);

        loanid_layout = (RelativeLayout) findViewById(R.id.loanid);
        mattachment = (ImageView) findViewById(R.id.attactment);


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userid = userPrefs.getString("UserId", "");

        hidden_layout.setVisibility(View.GONE);

        personal_no_layout.setVisibility(View.GONE);

        if (leave_request) {

            if (!medit) {

                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                spintext.setEnabled(false);
                date_loanperpose.setEnabled(false);
                edit_loanvalu.setEnabled(false);
//                edit_cyclepayament.setEnabled(false);
                personal_no_layout.setEnabled(false);
                mattachment.setVisibility(View.GONE);


            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);
                mattachment.setVisibility(View.VISIBLE);


            }

            create.setText("SAVE");

            edit_loanid.setText("" + loandetailsarray.get(pos).getLOANREQUESTID());

            personal_no_layout.setVisibility(View.VISIBLE);
            loanid_layout.setVisibility(View.VISIBLE);
            spintext.setText("" + loandetailsarray.get(pos).getLOANTYPE());
            date_loanperpose.setText("" + loandetailsarray.get(pos).getPRPOSEOFLOAN());
            Log.i("TAG", "loantype: " + loandetailsarray.get(pos).getLOANTYPE());

            Log.i("TAG", "loanbalance: " + loanBalanceDetailsResponses.size());


            if (loandetailsarray.get(pos).getLOANTYPE().equals("Emergency")) {

                edit_loanvalu.setEnabled(true);
                double loanvalue = Double.parseDouble(loandetailsarray.get(pos).getLOANVALUE());

                edit_loanvalu.setText("" + Constants.priceFormat1.format(loanvalue));

            } else {

                edit_loanvalu.setEnabled(false);
                double loanvalue = Double.parseDouble(loandetailsarray.get(pos).getLOANVALUE());

                edit_loanvalu.setText("" + Constants.priceFormat1.format(loanvalue));

            }


            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy", Locale.US);


            String date = loandetailsarray.get(pos).getLOANDATE();
            String date1 = loandetailsarray.get(pos).getDEDUCTIONSTARTDATE();
            String date2 = null, date3 = null;

            try {
                Date datetime = sdf.parse(date);
                Date datetime1 = sdf.parse(date1);
                date = sdf1.format(datetime);
                date1 = sdf1.format(datetime1);
                date2 = dateFormat1.format(datetime);
                date3 = dateFormat1.format(datetime1);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            date_loandate.setText("" + date);
            date_ddeductiondate.setText("" + date1);

            str_deduction_date = date3;
            str_loan_date = date2;


        } else {

            mattachment.setVisibility(View.GONE);

            personal_no_layout.setVisibility(View.GONE);
            loanid_layout.setVisibility(View.GONE);

            Calendar calendar = Calendar.getInstance();
            String date;
            Date date1 = null, date2 = null, date3 = null;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

            date = dateFormat.format(calendar.getTime());

            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            try {
                date1 = dateFormat.parse(date);
                date2 = dateFormat.parse(15 + "-" + (month + 1) + "-" + year);
                date3 = dateFormat.parse(16 + "-" + (month + 1) + "-" + year);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.i("TAG", "onCreate: " + dateFormat.format(date2));

            date_loandate.setText("" + dateFormat2.format(date1));
            str_loan_date = dateFormat1.format(date1);


            if (date1.after(date2)) {

                Calendar calendar1 = Calendar.getInstance();
                calendar1.add(Calendar.MONTH, 2);

                int mouth1 = calendar1.get(Calendar.MONTH);
                int year1 = calendar1.get(Calendar.YEAR);

                Date date4 = null;
                String date5;

                try {
                    date4 = dateFormat.parse(1 + "-" + mouth1 + "-" + year1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.i("TAG", "onCreate: " + dateFormat.format(date4));
                date5 = dateFormat2.format(date4);

                date_ddeductiondate.setText("" + date5);
                str_deduction_date = dateFormat1.format(date4);

            } else if (date1.before(date2) || date1.equals(date2)) {

                Calendar calendar1 = Calendar.getInstance();


                int mouth1 = calendar1.get(Calendar.MONTH);
                int year1 = calendar1.get(Calendar.YEAR);

                Date date4 = null;
                String date5;

                try {
                    date4 = dateFormat.parse(1 + "-" + (mouth1 + 1) + "-" + year1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                date5 = dateFormat2.format(date4);

                date_ddeductiondate.setText("" + date5);
                str_deduction_date = dateFormat1.format(date4);

            }
        }


        edit_name.setText("" + userPrefs.getString("Empname", ""));

//        edit_loanid.setText(loandetailsarray.get(pos).getLOANREQUESTID());



        mattachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(LoanReaquestActivity2.this, LeaveAttachmentActivity.class);
                a.putExtra("requestid", loandetailsarray.get(pos).getLOANREQUESTID());
                a.putExtra("requesttype", "LoanRequest");
                startActivity(a);

            }
        });

        dapLoanDate = date_loandate.getText().toString();

        dapLoanValue = edit_loanvalu.getText().toString();
        loanRequestId = edit_loanid.getText().toString();
        purposeOfLoan = date_loanperpose.getText().toString();
        deductionStartDate = date_ddeductiondate.getText().toString();
        dapLoanTypeId = spintext.getText().toString();

//        if (getIntent().getExtras().equals("stores")){
//
//            loanid.setVisibility(View.VISIBLE);
//        }
//        else {
//            loanid.setVisibility(View.GONE);
//        }


        spintext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Log.i(TAG, "onClick: " + loantype.size());
                    final BubbleLayout bubbleLayout;
//            if (language.equalsIgnoreCase("En")) {
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(LoanReaquestActivity2.this).inflate(R.layout.branch_list_bubble, null);
//            } else {
//                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup_arabic, null);
//            }
                    final PopupWindow popupWindow = BubblePopupHelper.create(LoanReaquestActivity2.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    branchadapter = new LoanTypeApdater(LoanReaquestActivity2.this, loantype, popupWindow);
                    mbubble_List.setAdapter(branchadapter);
                    int[] location = new int[2];
                    spintext.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(spintext, Gravity.NO_GRAVITY, location[0], spintext.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new LoanTypeDetailsApi().execute(Constants.getLonetypedetails);
            new LoanBalanceDetailsApi().execute(Constants.getLoanBalancedetails + "?_userid=" + userid);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }


//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                loanrequestApi();
//            }
//        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidden_layout.setVisibility(View.GONE);

            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
//                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (leave_request) {
//
//                            String service_url = Constants.getLoanSubmit + "?loanRequestId=" + loandetailsarray.get(pos).getLOANREQUESTID() + "&UserId=" + userid + "&Note=";
//                            String main_url = service_url.replaceAll(" ", "%20");
//                            new SubmitRequestApi().execute(main_url);
//                            Log.i("TAG", "onClick: " + main_url);
//
//                        }
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//
//            }
//        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {

                            str_loan_value = str_loan_value.replace(",","");
                            String service_url = Constants.getLoanCreate + "?dapLoanTypeId=" + str_loantype + "&dapLoanValue=" + str_loan_value + "&dapLoanDate=" + str_loan_date + "&deductionStartDate=" + str_deduction_date + "&purposeOfLoan=" + str_purpose_of_loan + "&userid=" + userid + "&loanRequestId=" + loandetailsarray.get(pos).getLOANREQUESTID();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            str_loan_value = str_loan_value.replace(",","");
                            String service_url = Constants.getLoanCreate + "?dapLoanTypeId=" + str_loantype + "&dapLoanValue=" + str_loan_value + "&dapLoanDate=" + str_loan_date + "&deductionStartDate=" + str_deduction_date + "&purposeOfLoan=" + str_purpose_of_loan + "&userid=" + userid + "&loanRequestId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

    }

    public void initView1() {

        spintext.setText("" + loantype.get(alternate_pos));

        if (loantype.get(alternate_pos).equals("Emergency")) {

            edit_loanvalu.setEnabled(true);
            edit_loanvalu.setHint("0.00");
            edit_loanvalu.setText("");

        } else {

            edit_loanvalu.setEnabled(false);
            for (int i = 0; i < loanBalanceDetailsResponses.size(); i++) {
                if (!loanBalanceDetailsResponses.get(i).getLoanTypeId().equals("Emergency")) {
                    if (loanBalanceDetailsResponses.get(i).getLoanTypeId().equals(loantype.get(alternate_pos))) {
                        edit_loanvalu.setText("" + loanBalanceDetailsResponses.get(i).getTotalLoanBalance());
                    }
                }

            }

        }

    }

    private boolean Validation() {

        str_loantype = spintext.getText().toString();
        str_purpose_of_loan = date_loanperpose.getText().toString();
        str_loan_value = edit_loanvalu.getText().toString();
//        str_cycle_pay = edit_cyclepayament.getText().toString();


        if (str_loantype.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select LoanType", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LoanReaquestActivity2.this);

            return false;
        } else if (str_purpose_of_loan.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter purpose of loan", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LoanReaquestActivity2.this);
            date_loanperpose.requestFocus();

            return false;
        } else if (str_loan_value.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter loan value", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LoanReaquestActivity2.this);

            return false;
        }


        return true;
    }

    public class LoanTypeDetailsApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            loanTypeDetailsResponses.clear();
//            loantype.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
            dialog = new ACProgressFlower.Builder(LoanReaquestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoanReaquestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetLoanTypeDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    LoanTypeDetailsResponse leaveRequestGetResponse = new LoanTypeDetailsResponse();
                                    leaveRequestGetResponse.setLOANTYPEID(jo2.getString("LOANTYPEID"));
                                    leaveRequestGetResponse.setLOANDESCRIPTION(jo2.getString("LOANDESCRIPTION"));

                                    loanTypeDetailsResponses.add(leaveRequestGetResponse);


                                    Log.d("TAG", "onPostExecute: " + loanTypeDetailsResponses.get(i).getLOANTYPEID());

                                }

                                Log.d("TAG", "loansize: " + loantype.size());

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LoanReaquestActivity2.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LoanReaquestActivity2.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class LoanBalanceDetailsApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            loanBalanceDetailsResponses.clear();
            loantype.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
            dialog = new ACProgressFlower.Builder(LoanReaquestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoanReaquestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");

                                JSONArray ja = jo1.getJSONArray("GetEmployeeLoanBalanceDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    LoanBalanceDetailsResponse leaveRequestGetResponse = new LoanBalanceDetailsResponse();
                                    leaveRequestGetResponse.setLoanTypeId(jo2.getString("LoanTypeId"));
                                    leaveRequestGetResponse.setPersonnalNumber(jo2.getString("PersonnalNumber"));
                                    leaveRequestGetResponse.setTotalLoanBalance(jo2.getString("TotalLoanBalance"));

                                    loanBalanceDetailsResponses.add(leaveRequestGetResponse);
                                    loantype.add(loanBalanceDetailsResponses.get(i).getLoanTypeId());

                                    Log.d("TAG", "onPostExecute: " + loanBalanceDetailsResponses.get(i).getLoanTypeId());

                                }

                                if (leave_request) {
                                    for (int i = 0; i < loanBalanceDetailsResponses.size(); i++) {
                                        if (loandetailsarray.get(pos).getLOANTYPE().equals(loanBalanceDetailsResponses.get(i).getLoanTypeId())) {
                                            edit_pesronalnumber.setText("" + loanBalanceDetailsResponses.get(i).getPersonnalNumber());
                                            Log.i("TAG", "loantype: " + loanBalanceDetailsResponses.get(i).getPersonnalNumber());
                                        }
                                    }
                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LoanReaquestActivity2.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LoanReaquestActivity2.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class CreatBalanceApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
            dialog = new ACProgressFlower.Builder(LoanReaquestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoanReaquestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoanReaquestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            Constants.msubmit = -1;
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoanReaquestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                                Log.d("TAG", "loansize: " + loantype.size());

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LoanReaquestActivity2.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LoanReaquestActivity2.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
            dialog = new ACProgressFlower.Builder(LoanReaquestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LoanReaquestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoanReaquestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoanReaquestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LoanReaquestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    //    private void loanrequestApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(LoanReaquestActivity2.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<LoanaReaquestResponce2> call = apiService.getloan2(userid,dapLoanDate,deductionStartDate,purposeOfLoan,loanRequestId,dapLoanValue,dapLoanTypeId);
//
//        call.enqueue(new Callback<LoanaReaquestResponce2>() {
//
//            public void onResponse(Call<LoanaReaquestResponce2> call, Response<LoanaReaquestResponce2> response) {
//                if (response.isSuccessful()) {
//                    LoanaReaquestResponce2 registrationResponse = response.body();
//                    Log.d(TAG, "LoanResponse: "+response.message());
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                }
//            }
//
//            public void onFailure(Call<LoanaReaquestResponce2> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(LoanReaquestActivity2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(LoanReaquestActivity2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }
//    private void loantypedrop() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(LoanReaquestActivity2.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(LoanReaquestActivity2.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<LoantypeResponce> call = apiService.gteloantype();
//
//        call.enqueue(new Callback<LoantypeResponce>() {
//
//            public void onResponse(Call<LoantypeResponce> call, Response<LoantypeResponce> response) {
//                if (response.isSuccessful()) {
//                    LoantypeResponce registrationResponse = response.body();
//                    Log.d(TAG, "LoanResponse: "+response.message());
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            if (response.isSuccessful()) {
//                                LoantypeSelected  = -1;
//                                filterCities(response.body().getData());
//                            }
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                }
//            }
//
//            public void onFailure(Call<LoantypeResponce> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(LoanReaquestActivity2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(LoanReaquestActivity2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }
    private void filterCities(LoantypeResponce.Data data) {
        ArrayList<String> cityArray = new ArrayList<>();


        cityArray.add("-- Select LoanType --");
        for (int i = 0; i < data.getGetLoanTypeDetails().size(); i++) {
            cityArray.add(data.getGetLoanTypeDetails().get(i).getLOANTYPEID());
        }

    }
}
