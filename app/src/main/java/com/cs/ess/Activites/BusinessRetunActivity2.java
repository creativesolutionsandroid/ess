package com.cs.ess.Activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Adapter.RetunTypeAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.RetunTripDetailsResponce;
import com.cs.ess.Models.RetunTypeResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class BusinessRetunActivity2 extends Activity {

    ImageView backbtn, pluse, datepiker;
    RecyclerView listview;
    public static RelativeLayout iddrowdron, hidden_layout;
    TextView fromdatetext, todatetext, leavetype, datetext;
    EditText text_pid;
    RetunTypeAdapter madapter;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    String userId = "ialsulta";
    public static int alternate_pos = -1, leave_pos = -1;
    boolean leave_request = false, medit = false;
    int pos;
    private int mYear, mMonth, mDay;
    private int mendYear, mendMonth, mendDay;
    Boolean isToday = false;
    Date checkInDate, checkoutDate;
    String strreferid, strfromdate, strtransdate, strretundate, requestid;
    Button create;


    ArrayList<String> retuntypearry = new ArrayList<>();
    ArrayList<RetunTypeResponce> getretuntyresponcearray = new ArrayList<>();
    private ArrayList<RetunTripDetailsResponce> retundetailsarray = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_retun_activity);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");
        retuntypearry.clear();

        try {
            retundetailsarray = (ArrayList<RetunTripDetailsResponce>) getIntent().getSerializableExtra("stores");
            pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
            if (retundetailsarray.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }
        String service_url = Constants.getretuntype + "?UserId=" + userId;
        String main_url = service_url.replaceAll(" ", "%20");
        new permissionidApi().execute(main_url);

        backbtn = (ImageView) findViewById(R.id.back_btn);
        datepiker = (ImageView) findViewById(R.id.datepiker);
        pluse = (ImageView) findViewById(R.id.pluse);
        listview = (RecyclerView) findViewById(R.id.listview);
        fromdatetext = (TextView) findViewById(R.id.fromdatetext);
        todatetext = (TextView) findViewById(R.id.todatetext);
        text_pid = (EditText) findViewById(R.id.text_pid);
        leavetype = (TextView) findViewById(R.id.leavetype);
        datetext = (TextView) findViewById(R.id.datetext);
        iddrowdron = (RelativeLayout) findViewById(R.id.iddrowdron);
        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);

        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.sumbit);


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (leave_request) {

            if (!medit) {

                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                text_pid.setEnabled(false);
                datepiker.setEnabled(false);

            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);

            }
//            loanid_layout.setVisibility(View.VISIBLE);
            create.setText("SAVE");

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            try {
//                Date datetime = sdf.parse(retundetailsarray.get(pos).getTRANSDATE());
                Date datetime1 = sdf.parse(retundetailsarray.get(pos).getRETURNDATE());

                datetext.setText(sdf1.format(datetime1));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            text_pid.setText("" + retundetailsarray.get(pos).getREFTRANSID());
            leavetype.setText("" + retundetailsarray.get(pos).getLeave_Type());


        } else {

            create.setText("CREATE");

        }

        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidden_layout.setVisibility(View.GONE);

            }
        });

        text_pid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final BubbleLayout bubbleLayout;
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(BusinessRetunActivity2.this).inflate(R.layout.branch_list_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(BusinessRetunActivity2.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    madapter = new RetunTypeAdapter(BusinessRetunActivity2.this, getretuntyresponcearray, popupWindow);
                    mbubble_List.setAdapter(madapter);
                    int[] location = new int[2];
                    iddrowdron.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(iddrowdron, Gravity.NO_GRAVITY, location[0], iddrowdron.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        datepiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (text_pid.getText().toString().length() == 0) {

                    Constants.showOneButtonAlertDialog("Please select Reference Id", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), BusinessRetunActivity2.this);

                } else {

                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(BusinessRetunActivity2.this,
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                    SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                    SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

                                    if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                        isToday = true;

                                    } else {
                                        isToday = false;
                                    }
                                    mYear = year;
                                    mDay = dayOfMonth;
                                    mMonth = monthOfYear;

                                    Date date = null;

                                    try {
                                        date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
                                        checkInDate = date;
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    String date1, date2;

                                    date1 = postFormater.format(date);
                                    date2 = postFormater1.format(date);
                                    datetext.setText(date1);


                                }
                            }, mYear, mMonth, mDay);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                    Date date = null;

                    try {
                        date = dateFormat.parse(fromdatetext.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);

                    datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                    datePickerDialog.setTitle("Select Date");
                    datePickerDialog.show();

                }

            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity2.this);
//                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (leave_request) {
//
//                            String service_url = Constants.getreturnSubmit + "?RequestId=" + retundetailsarray.get(pos).getREQUESTID() + "&UserId=" + userId + "&Note=";
//                            String main_url = service_url.replaceAll(" ", "%20");
//                            new SubmitRequestApi().execute(main_url);
//                            Log.i("TAG", "onClick: " + main_url);
//
//                        }
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//
//            }
//        });


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity2.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {

                            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                            Date date = null, date1 = null;

                            try {
                                date = dateFormat1.parse(strretundate);
                                date1 = dateFormat1.parse(strtransdate);
                                strretundate = dateFormat.format(date);
                                strtransdate = dateFormat.format(date1);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            String service_url = Constants.getreturnCreate + "?returnDate=" + strretundate + "&transDate=" + strtransdate + "&refTransID=" + strreferid + "&userId=" + userId + "&requestId=" + retundetailsarray.get(pos).getREQUESTID();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreateRequestApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                            Date date = null, date1 = null;

                            try {
                                date = dateFormat1.parse(strretundate);
                                date1 = dateFormat1.parse(strtransdate);
                                strretundate = dateFormat.format(date);
                                strtransdate = dateFormat.format(date1);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            String service_url = Constants.getreturnCreate + "?returnDate=" + strretundate + "&transDate=" + strtransdate + "&refTransID=" + strreferid + "&userId=" + userId + "&requestId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreateRequestApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

    }

    private boolean Validation() {
        strreferid = text_pid.getText().toString();
        strtransdate = fromdatetext.getText().toString();
        strretundate = datetext.getText().toString();

        if (strreferid.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select Reference Id", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), BusinessRetunActivity2.this);

            return false;
        } else if (strtransdate.length() == 0) {

            Constants.showOneButtonAlertDialog("Please Select Date ", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), BusinessRetunActivity2.this);


            return false;
        } else if (strretundate.length() == 0) {

            Constants.showOneButtonAlertDialog("Please Select RetunDate ", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), BusinessRetunActivity2.this);

            return false;
        }


        return true;
    }


    public class permissionidApi extends AsyncTask<String, Integer, String> {
        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            retuntypearry.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity2.this);
            dialog = new ACProgressFlower.Builder(BusinessRetunActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(BusinessRetunActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equals("")) {
                        Toast.makeText(BusinessRetunActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);
                            try {
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");

                                JSONArray ja = jo1.getJSONArray("GetEmplReturnDateTransaction");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    RetunTypeResponce RetunTypeResponce = new RetunTypeResponce();
                                    RetunTypeResponce.setLEAVETRANSID(jo2.getString("LEAVETRANSID"));
                                    RetunTypeResponce.setFROMDATE(jo2.getString("FROMDATE"));
                                    RetunTypeResponce.setTODATE(jo2.getString("TODATE"));
                                    RetunTypeResponce.setLeaveType(jo2.getString("LeaveType"));
                                    getretuntyresponcearray.add(RetunTypeResponce);
                                    retuntypearry.add(getretuntyresponcearray.get(i).getLEAVETRANSID());

                                }
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BusinessRetunActivity2.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

                                if (leave_request) {

                                    for (int i = 0; i < getretuntyresponcearray.size(); i++) {

                                        if (retundetailsarray.get(pos).getREFTRANSID().equals(getretuntyresponcearray.get(i).getLEAVETRANSID())) {

                                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
                                            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                            try {
                                                Date datetime = sdf.parse(getretuntyresponcearray.get(i).getFROMDATE());
                                                Date datetime1 = sdf.parse(getretuntyresponcearray.get(i).getTODATE());
                                                fromdatetext.setText(sdf1.format(datetime));
                                                todatetext.setText(sdf1.format(datetime1));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    }
                                }

                            } catch (JSONException je) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(BusinessRetunActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public void initView() {

        text_pid.setText("" + getretuntyresponcearray.get(alternate_pos).getLEAVETRANSID());
//                text_pid.setText("" + getretuntyresponcearray.get(alternate_pos).getLEAVETRANSID() + " " + sdf1.format(datetime) + " " + sdf1.format(datetime1));

        leavetype.setText("" + getretuntyresponcearray.get(alternate_pos).getLeaveType());

        for (int i = 0; i < getretuntyresponcearray.size(); i++) {

            if (retuntypearry.get(alternate_pos).equals(getretuntyresponcearray.get(i).getLEAVETRANSID())) {

                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                Date datetime = null, datetime1 = null;
                try {
                    datetime = sdf.parse(getretuntyresponcearray.get(i).getFROMDATE());
                    datetime1 = sdf.parse(getretuntyresponcearray.get(i).getTODATE());
                    fromdatetext.setText("" + sdf1.format(datetime));
                    todatetext.setText("" + sdf1.format(datetime1));
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    public class CreateRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response1;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity2.this);
            dialog = new ACProgressFlower.Builder(BusinessRetunActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response1 = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response1:" + response1);
                return response1;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(BusinessRetunActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(BusinessRetunActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String Message = "", MessageAr = "";
                                boolean status = false;
                                status = jo.getBoolean("Status");
                                Message = jo.getString("Message");
                                MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BusinessRetunActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.mreturn_submit = -1;
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BusinessRetunActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(BusinessRetunActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity2.this);
            dialog = new ACProgressFlower.Builder(BusinessRetunActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(BusinessRetunActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(BusinessRetunActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BusinessRetunActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BusinessRetunActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(BusinessRetunActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
