package com.cs.ess.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cs.ess.Adapter.RetunDetailsAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.RetunTripDetailsResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BusinessRetunActivity1 extends Activity implements SwipeRefreshLayout.OnRefreshListener {

    String userId;
    ImageView back_btn, plus;
    RecyclerView retun_list;

    ArrayList<RetunTripDetailsResponce> retunDetailsarray = new ArrayList<>();
    SharedPreferences userPrefs;
    public RetunDetailsAdapter madapter;
    SwipeRefreshLayout swipe_refresh;
    boolean approvel = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_retun_request1);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("UserId", "");

        try {
            approvel = getIntent().getBooleanExtra("approval",false);
        } catch (Exception e) {
            e.printStackTrace();
            approvel = false;
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
        plus = (ImageView) findViewById(R.id.pluse);
        retun_list = (RecyclerView) findViewById(R.id.leave_list);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BusinessRetunActivity1.this, BusinessRetunActivity2.class);
                startActivity(intent);
            }
        });

        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipe_refresh.setOnRefreshListener(BusinessRetunActivity1.this);
        swipe_refresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

//        String networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity1.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new RetunRequestApi().execute(Constants.getretundetails + "?UserId=" + userId);
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }

    }

    private void loadRecyclerViewData() {

        String networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity1.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new RetunRequestApi().execute(Constants.getretundetails + "?UserId=" + userId);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRefresh() {

        loadRecyclerViewData();

    }


    public class RetunRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
//        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            retunDetailsarray.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity1.this);
//            dialog = new ACProgressFlower.Builder(BusinessRetunActivity1.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(BusinessRetunActivity1.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(BusinessRetunActivity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                retunDetailsarray.clear();
                                JSONArray ja = jo1.getJSONArray("GetReturnDateRequestDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    RetunTripDetailsResponce RetunTripDetailsResponce = new RetunTripDetailsResponce();
                                    RetunTripDetailsResponce.setREQUESTID(jo2.getString("REQUESTID"));
                                    RetunTripDetailsResponce.setTRANSDATE(jo2.getString("TRANSDATE"));
                                    RetunTripDetailsResponce.setRETURNDATE(jo2.getString("RETURNDATE"));
                                    RetunTripDetailsResponce.setREFTRANSID(jo2.getString("REFTRANSID"));
                                    RetunTripDetailsResponce.setWORKFLOWSTATE(jo2.getString("WORKFLOWSTATE"));
                                    RetunTripDetailsResponce.setNAME(jo2.getString("NAME"));
                                    RetunTripDetailsResponce.setLeave_Type(jo2.getString("Leave Type"));
                                    RetunTripDetailsResponce.setDATAAREAID(jo2.getString("DATAAREAID"));
                                    RetunTripDetailsResponce.setWORKFLOWSTATENUM(jo2.getString("WORKFLOWSTATENUM"));

                                    retunDetailsarray.add(RetunTripDetailsResponce);
                                }


                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BusinessRetunActivity1.this);
                                retun_list.setLayoutManager(linearLayoutManager);
                                madapter = new RetunDetailsAdapter(BusinessRetunActivity1.this, retunDetailsarray, BusinessRetunActivity1.this, userId, approvel);
                                retun_list.setAdapter(madapter);

                                swipe_refresh.setRefreshing(false);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(BusinessRetunActivity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

    public void initView() {

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
//        String networkStatus = NetworkUtil.getConnectivityStatusString(BusinessRetunActivity1.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new RetunRequestApi().execute(Constants.getretundetails + "?UserId=" + userId);
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }
//        Log.i("TAG", "onResume: " + userId);

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

}



