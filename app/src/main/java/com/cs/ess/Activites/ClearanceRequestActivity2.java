package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.Adapter.EOSRequestTypeAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.ClearanceDetailsResponce;
import com.cs.ess.Models.EOSDetailsResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ClearanceRequestActivity2 extends AppCompatActivity {

    String userId;
    ImageView back_btn;
    SharedPreferences userPrefs;
    TextView last_working_date, trans_type;
    EditText notes_input, edit_id;
    Button submit;
    ArrayList<ClearanceDetailsResponce> clearanceDetailsResponces = new ArrayList<>();
    int pos;
    boolean medit = false, leave_request = false;
//    RelativeLayout request_id_layout;
    String str_submit_date, str_last_working_day, str_trans_type, str_note;
//    private int mYear, mMonth, mDay;
//    boolean isToday = false;
//    public static RelativeLayout hidden_layout;
//    public static int alternate_pos = -1;
//    ArrayList<String> eos_type = new ArrayList<>();
//    EOSRequestTypeAdapter madapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clearance_screen2);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("UserId", "");


        back_btn = (ImageView) findViewById(R.id.back_btn);

        last_working_date = (TextView) findViewById(R.id.last_working_date);
        trans_type = (TextView) findViewById(R.id.trans_type);

        notes_input = (EditText) findViewById(R.id.notes_input);
        edit_id = (EditText) findViewById(R.id.edit_id);

        submit = (Button) findViewById(R.id.sumbit);

        try {
            clearanceDetailsResponces = (ArrayList<ClearanceDetailsResponce>) getIntent().getSerializableExtra("stores");
            pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }


            edit_id.setText("" + clearanceDetailsResponces.get(pos).getEOSTRANSID());
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

            Date date = null, date1 = null;

            try {
                date = dateFormat.parse(clearanceDetailsResponces.get(pos).getTRANSDATE());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            last_working_date.setText("" + dateFormat1.format(date));

//            notes_input.setText("" + eosDetailsResponces.get(pos).get);
            trans_type.setText("" + clearanceDetailsResponces.get(pos).getEOSTRANSTYPE());



        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {

//                    String service_url = Constants.geteosSubmit + "?dapEOSReqId=" + eosDetailsResponces.get(pos).getDAPEOSREQID() + "&UserId=" + userId + "&Note=";
//                    String main_url = service_url.replaceAll(" ", "%20");
//                    new SubmitRequestApi().execute(main_url);
//                    Log.i("TAG", "onClick: " + main_url);
                }

            }
        });



    }

    private boolean Validation() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy",Locale.US);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy",Locale.US);

        Date date = null, date1 = null;

        try {
//            date = dateFormat.parse(submit_date.getText().toString());
            date1 = dateFormat.parse(last_working_date.getText().toString());
            str_submit_date = dateFormat1.format(date);
            str_last_working_day = dateFormat1.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        str_trans_type = text_pid.getText().toString();
        str_note = notes_input.getText().toString();

        if (str_last_working_day.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select last working day", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), ClearanceRequestActivity2.this);

            return false;
        } else if (str_trans_type.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select transation type", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), ClearanceRequestActivity2.this);

            return false;
        } else if (str_note.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter notes", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), ClearanceRequestActivity2.this);

            return false;
        }

        return true;
    }

    public class CreatBalanceApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ClearanceRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(ClearanceRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ClearanceRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(ClearanceRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ClearanceRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ClearanceRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(ClearanceRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ClearanceRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(ClearanceRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ClearanceRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(ClearanceRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ClearanceRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ClearanceRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(ClearanceRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

//    public void initView() {
//        text_pid.setText("" + eos_type.get(alternate_pos));
//    }

}
