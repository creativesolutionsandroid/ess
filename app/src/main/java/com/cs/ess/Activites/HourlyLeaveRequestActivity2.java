package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.cs.ess.Adapter.HourlyPermissonTypeAdapter;
import com.cs.ess.Adapter.OvertimeTypeAdapter;
import com.cs.ess.Adapter.OvertimeTypeAdapter2;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.HourlyDetailsResponce;
import com.cs.ess.Models.HourlyLeaveRequestResponse;
import com.cs.ess.Models.HourlyPermissionResponce;
import com.cs.ess.R;
import com.cs.ess.Rest.APIInterface;
import com.cs.ess.Rest.ApiClient;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HourlyLeaveRequestActivity2 extends AppCompatActivity {

    ImageView backbtn, starttime, endtime, datepiker, mattachment;
    TextView starttimetext, endtimetext, hours_Balance, datetext, hourly_leaveid, name, status, permissionid, transdate, leavebalancetext;
    Date checkInDate, checkoutDate;
    EditText dsc;
    private int mYear, mMonth, mDay;
    private int mendYear, mendMonth, mendDay;
    Boolean isToday = false;
    String userId;
    SharedPreferences userPrefs;
    public static RelativeLayout hidden_layout, dropdown, dropdown1, iddropdown;
    SharedPreferences.Editor userPrefsEditor;
    boolean screen = false;
    ArrayList<HourlyDetailsResponce> hourlyDetailsArrayList = new ArrayList<>();
    int pos;
    ArrayList<String> timepickarray = new ArrayList<>();
    ArrayList<String> permissiontypearry = new ArrayList<>();
    ArrayList<HourlyPermissionResponce> getPermissiontyresponce = new ArrayList<>();
    OvertimeTypeAdapter madapter;
    OvertimeTypeAdapter2 madapter2;
    HourlyPermissonTypeAdapter madapter3;
    public static int start_time = -1, end_time = -1;
    Button create;
    boolean leave_request = false, medit = false;
    String strpermissionid, strfromtime, strtotime, strdate, strdescription, strdapbsenceTransId;
    RelativeLayout hourly_request_layout, status_layout;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hourlyleave_activity2);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("UserId", "");
        timepickarray.clear();
        new permissionidApi().execute(Constants.gethourlypermissionid + "?_userid=" + userId);


        try {
            hourlyDetailsArrayList = (ArrayList<HourlyDetailsResponce>) getIntent().getSerializableExtra("stores");
            pos = getIntent().getIntExtra("pos", 0);
            screen = getIntent().getBooleanExtra("screen", false);

            if (hourlyDetailsArrayList.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }


        timepickarray.add("8:00 am");
        timepickarray.add("8:30 am");
        timepickarray.add("9:00 am");
        timepickarray.add("9:30 am");
        timepickarray.add("10:00 am");
        timepickarray.add("10:30 am");
        timepickarray.add("11:00 am");
        timepickarray.add("11:30 am");
        timepickarray.add("12:00 pm");
        timepickarray.add("12:30 am");
        timepickarray.add("1:00 pm");
        timepickarray.add("1:30 am");
        timepickarray.add("2:00 pm");
        timepickarray.add("2:30 am");
        timepickarray.add("3:00 pm");
        timepickarray.add("3:30 am");
        timepickarray.add("4:00 pm");
        timepickarray.add("4:30 am");
        timepickarray.add("5:00 am");

        backbtn = (ImageView) findViewById(R.id.back_btn);
        starttime = (ImageView) findViewById(R.id.starttime);
        endtime = (ImageView) findViewById(R.id.endtime);
        datepiker = (ImageView) findViewById(R.id.datepiker);
        starttimetext = (TextView) findViewById(R.id.starttimetex);
        endtimetext = (TextView) findViewById(R.id.endtimetext);
        datetext = (TextView) findViewById(R.id.datetext);
        hourly_leaveid = (TextView) findViewById(R.id.hourly_leaveid);
        name = (TextView) findViewById(R.id.text_empolyname);
        status = (TextView) findViewById(R.id.text_status);
        permissionid = (TextView) findViewById(R.id.text_pid);
        leavebalancetext = (TextView) findViewById(R.id.text_Balance);
        dsc = (EditText) findViewById(R.id.employee_desc);

        mattachment = (ImageView) findViewById(R.id.attactment);

        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);
        dropdown = (RelativeLayout) findViewById(R.id.dropdown);
        dropdown1 = (RelativeLayout) findViewById(R.id.dropdown1);
        iddropdown = (RelativeLayout) findViewById(R.id.iddrowdron);
        status_layout = (RelativeLayout) findViewById(R.id.status_layout);

        hourly_request_layout = (RelativeLayout) findViewById(R.id.hourly_request_layout);

        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.sumbit);


        if (leave_request) {

            if (!screen) {
                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                mattachment.setVisibility(View.GONE);
                dsc.setEnabled(false);
                iddropdown.setEnabled(false);
                datepiker.setEnabled(false);
                dropdown.setEnabled(false);
                dropdown1.setEnabled(false);

//                spintext.setEnabled(false);
//                date_loanperpose.setEnabled(false);
//                edit_loanvalu.setEnabled(false);
//                edit_cyclepayament.setEnabled(false);
//                personal_no_layout.setEnabled(false);
            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);
                mattachment.setVisibility(View.VISIBLE);

            }

            create.setText("SAVE");

            status_layout.setVisibility(View.VISIBLE);
            hourly_request_layout.setVisibility(View.VISIBLE);


            hourly_leaveid.setText("" + hourlyDetailsArrayList.get(pos).getDAPABSENCETRANS());
            name.setText("" + hourlyDetailsArrayList.get(pos).getNAME());
            permissionid.setText("" + hourlyDetailsArrayList.get(pos).getPERMISSIONLEAVETYPE());
            dsc.setText("" + hourlyDetailsArrayList.get(pos).getDESCRIPTION());
            starttimetext.setText(hourlyDetailsArrayList.get(pos).getFROMTIME());
            endtimetext.setText(hourlyDetailsArrayList.get(pos).getTOTIME());
//            leavebalancetext.setText(hourlyDetailsArrayList.get(pos).getTOTIME());

            double leavebalance = Double.parseDouble(hourlyDetailsArrayList.get(pos).getTXLLEAVEBALANCEREAL());
            leavebalancetext.setText("" + Constants.priceFormat1.format(leavebalance));

            String date = hourlyDetailsArrayList.get(pos).getTRANSDATE();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            try {
                Date datetime = sdf.parse(date);
                date = sdf1.format(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            datetext.setText("" + date);

            if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("0")) {
                status.setText("Not submitted");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("1")) {
                status.setText("Submitted");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("2")) {
                status.setText("Pending approval");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("3")) {
                status.setText("Pending complete");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("4")) {
                status.setText("Approved");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("5")) {
                status.setText("Rejected");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("6")) {
                status.setText("Change requested");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("7")) {
                status.setText("Completed");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("8")) {
                status.setText("Completed");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("9")) {
                status.setText("Pending cancellation");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("10")) {
                status.setText("Canceled");
            } else if (hourlyDetailsArrayList.get(pos).getWORKFLOWSTATENUM().equals("11")) {
                status.setText("Rejected");
            }

        } else {

            create.setText("CREATE");

            mattachment.setVisibility(View.GONE);

            hourly_request_layout.setVisibility(View.GONE);
            status_layout.setVisibility(View.GONE);

            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

            Calendar calendar = Calendar.getInstance();

            datetext.setText("" + sdf1.format(calendar.getTime()));

        }

        name.setText("" + userPrefs.getString("Empname", ""));


        mattachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(HourlyLeaveRequestActivity2.this, LeaveAttachmentActivity.class);
                a.putExtra("requestid", hourlyDetailsArrayList.get(pos).getDAPABSENCETRANS());
                a.putExtra("requesttype", "HourlyLeaveRequest");
                startActivity(a);

            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String service_url = Constants.gethourlysubmit + "?dapAbsenceTransId=" + hourlyDetailsArrayList.get(pos).getDAPABSENCETRANS() + "&UserId=" + userId + "&Note=";
//                    String main_url = service_url.replaceAll(" ", "%20");
//                    new SubmitRequestApi().execute(main_url);
//                    Log.i("TAG", "onClick: " + main_url);
//                }
//
//            }
//        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity2.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {

                            String service_url = Constants.gethourlycreate + "?_dapPermissionLeaveType=" + strpermissionid + "&_fromTime=" + strfromtime + "&_toTime=" + strtotime + "&_transDate=" + strdate + "&_description=" + strdescription + "&_userid=" + userId + "&_dapbsenceTransId=" + hourlyDetailsArrayList.get(pos).getDAPABSENCETRANS();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            String service_url = Constants.gethourlycreate + "?_dapPermissionLeaveType=" + strpermissionid + "&_fromTime=" + strfromtime + "&_toTime=" + strtotime + "&_transDate=" + strdate + "&_description=" + strdescription + "&_userid=" + userId + "&_dapbsenceTransId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });


        iddropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final BubbleLayout bubbleLayout;
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(HourlyLeaveRequestActivity2.this).inflate(R.layout.branch_list_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(HourlyLeaveRequestActivity2.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    madapter3 = new HourlyPermissonTypeAdapter(HourlyLeaveRequestActivity2.this, permissiontypearry, popupWindow);
                    mbubble_List.setAdapter(madapter3);
                    Log.i("TAG", "onClick: " + permissiontypearry.get(0));
                    int[] location = new int[2];
                    iddropdown.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(iddropdown, Gravity.NO_GRAVITY, location[0], iddropdown.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidden_layout.setVisibility(View.GONE);

            }
        });



        datepiker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(HourlyLeaveRequestActivity2.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                                    isToday = true;

                                } else {
                                    isToday = false;
                                }
                                mYear = year;
                                mDay = dayOfMonth;
                                mMonth = monthOfYear;

                                Date date = null;

                                try {
                                    date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
                                    checkInDate = date;
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String date1, date2;

                                date1 = postFormater.format(date);
                                date2 = postFormater1.format(date);
                                datetext.setText(date1);


                            }
                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.setTitle("Select Date");
                datePickerDialog.show();

            }
        });

        dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final BubbleLayout bubbleLayout;
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(HourlyLeaveRequestActivity2.this).inflate(R.layout.branch_list_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(HourlyLeaveRequestActivity2.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    madapter = new OvertimeTypeAdapter(HourlyLeaveRequestActivity2.this, timepickarray, popupWindow);
                    mbubble_List.setAdapter(madapter);
                    int[] location = new int[2];
                    dropdown.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(dropdown, Gravity.NO_GRAVITY, location[0], dropdown.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        dropdown1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final BubbleLayout bubbleLayout;
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(HourlyLeaveRequestActivity2.this).inflate(R.layout.branch_list_bubble, null);
                    final PopupWindow popupWindow = BubblePopupHelper.create(HourlyLeaveRequestActivity2.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    madapter2 = new OvertimeTypeAdapter2(HourlyLeaveRequestActivity2.this, timepickarray, popupWindow);
                    mbubble_List.setAdapter(madapter2);
                    int[] location = new int[2];
                    dropdown1.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(dropdown1, Gravity.NO_GRAVITY, location[0], dropdown1.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

//        endtime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(HourlyLeaveRequestActivity2.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        endtimetext.setText(selectedHour + ":" + selectedMinute);
//                    }
//                }, hour, minute, true);//Yes 24 hour time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//
////                CalculateNumOfDays();
//            }
//        });
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private boolean Validation() {

        strpermissionid = permissionid.getText().toString();
        strdate = datetext.getText().toString();
        strtotime = endtimetext.getText().toString();
        strfromtime = starttimetext.getText().toString();
        strdescription = dsc.getText().toString();


        if (strpermissionid.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select PermissionType", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HourlyLeaveRequestActivity2.this);

            return false;
        } else if (strdate.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select From Time", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HourlyLeaveRequestActivity2.this);

            return false;
        } else if (strtotime.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select To Time", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HourlyLeaveRequestActivity2.this);

            return false;
        } else if (strdate.length() == 0) {

            Constants.showOneButtonAlertDialog("Please Enter Date", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HourlyLeaveRequestActivity2.this);

            return false;
        } else if (strdescription.length() == 0) {

            Constants.showOneButtonAlertDialog("Please Enter Description", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HourlyLeaveRequestActivity2.this);

            return false;
        }


        return true;
    }


    private void HourlyLeaveRequestApi() {

        final String networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity2.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<HourlyLeaveRequestResponse> call = apiService.gethourly(userId);
        call.enqueue(new Callback<HourlyLeaveRequestResponse>() {

            public void onResponse(Call<HourlyLeaveRequestResponse> call, Response<HourlyLeaveRequestResponse> response) {
                if (response.isSuccessful()) {
                    HourlyLeaveRequestResponse registrationResponse = response.body();
                    try {
                        if (registrationResponse.getMessage().equals("Success")) {


                        } else {

                        }
                    } catch (Exception e) {

                    }
                } else {
                    try {
                        Log.d("TAG", "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            public void onFailure(Call<HourlyLeaveRequestResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                } else {
                }
            }
        });
    }

    public class permissionidApi extends AsyncTask<String, Integer, String> {
        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            permissiontypearry.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(HourlyLeaveRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HourlyLeaveRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equals("")) {
                        Toast.makeText(HourlyLeaveRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);
                            try {
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("getHourlyLeaveBalanceDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    HourlyPermissionResponce permissiontype = new HourlyPermissionResponce();
                                    permissiontype.setPermissionTypeId(jo2.getString("PermissionTypeId"));
                                    permissiontype.setPersonnalNumber(jo2.getString("PersonnalNumber"));
                                    permissiontype.setTotalleaveBalance(jo2.getString("TotalleaveBalance"));
                                    getPermissiontyresponce.add(permissiontype);
                                    permissiontypearry.add(getPermissiontyresponce.get(i).getPermissionTypeId());
                                    Log.d("TAG", "onPostExecute: " + permissiontypearry.get(i));
                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HourlyLeaveRequestActivity2.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

                            } catch (JSONException je) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(HourlyLeaveRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    public class CreatBalanceApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(HourlyLeaveRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HourlyLeaveRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HourlyLeaveRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HourlyLeaveRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.mhour_submit = -1;
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HourlyLeaveRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HourlyLeaveRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity2.this);
            dialog = new ACProgressFlower.Builder(HourlyLeaveRequestActivity2.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HourlyLeaveRequestActivity2.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HourlyLeaveRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HourlyLeaveRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HourlyLeaveRequestActivity2.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HourlyLeaveRequestActivity2.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public void initView1() {
        starttimetext.setText("" + timepickarray.get(start_time));
    }

    public void initView2() {
        endtimetext.setText("" + timepickarray.get(end_time));
    }

    public void initView3() {

        for (int i = 0; i < getPermissiontyresponce.size(); i++){

            if (permissiontypearry.get(start_time).equals(getPermissiontyresponce.get(i).getPermissionTypeId())) {

                double leavebalance = Double.parseDouble(getPermissiontyresponce.get(i).getTotalleaveBalance());
                leavebalancetext.setText("" + Constants.priceFormat1.format(leavebalance));

            }

        }
        permissionid.setText("" + permissiontypearry.get(start_time));
    }


}
