package com.cs.ess.Activites;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.ess.Adapter.AlternateEmpAdapter;
import com.cs.ess.Adapter.BranchListAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.AlternateEmpResponse;
import com.cs.ess.Models.LeaveBalanceResponse;
import com.cs.ess.Models.LeaveRequestGetResponse;
import com.cs.ess.Models.LeaveRequestResponse;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LeaveRequestActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    ImageView back_btn;
    RelativeLayout startcalander, endcalendra;
    EditText vactionet;
    private int mYear, mMonth, mDay;
    private int mendYear, mendMonth, mendDay;
    Boolean isToday = false;
    TextView startdate, startdayyear, enddate, enddayyear, noofdays, leaves_balance;
    int day, month, year;
    Date checkInDate = null, checkoutDate = null, starting_date = null;
    DatePickerDialog datePickerDialog, datePickerDialog1;
    boolean from = false, to = false;
    int pos, endpos, num_holidays = 0;

//    EditText comments;
//    String str_comment;
    EditText contact_input, ticket_input, destination_input, notes_input, alternate_employee_name;
    String str_leave_name, str_contact, str_note, str_startdate, str_enddate, str_alternate, str_alternate_id;
    boolean strprovide_visa = false;
    RadioButton re_entry;
    Button create;
    public static RelativeLayout hidden_layout;
    public static int leave_pos = -1, alternate_pos = -1;
    RelativeLayout leave_layout, alternate_layout;
    ImageView down_arrow, mattachment;
    BranchListAdapter branchadapter;
    AlternateEmpAdapter madapter;
    ArrayList<LeaveRequestResponse.GetLeaveIdDetails> leaveIdDetails = new ArrayList<>();
    ArrayList<AlternateEmpResponse.EmployeeDetails> employeeDetails = new ArrayList<>();
    ArrayList<LeaveBalanceResponse> getEmployeeBalanceDetails = new ArrayList<>();
    ArrayList<LeaveRequestGetResponse> leaveRequestDetails = new ArrayList<>();
    int leave_get_pos;
    ArrayList<String> leaveid = new ArrayList<>();
    ArrayList<String> alternate = new ArrayList<>();
    boolean leave_request = false, medit = false;
    public static boolean attachment = false;
    String userid;
    SharedPreferences userPrefs;
    RelativeLayout loanid_layout;
    EditText leave_request_id;



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_request);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userid = userPrefs.getString("UserId", "");

        back_btn = (ImageView) findViewById(R.id.back_btn);
        vactionet = (EditText) findViewById(R.id.edit_annualvacation);
        startcalander = (RelativeLayout) findViewById(R.id.form_layout);
        endcalendra = (RelativeLayout) findViewById(R.id.to_layout);
        startdate = (TextView) findViewById(R.id.startdate);
        startdayyear = (TextView) findViewById(R.id.startdayyear);
        enddate = (TextView) findViewById(R.id.enddate);
        enddayyear = (TextView) findViewById(R.id.enddayyear);
        noofdays = (TextView) findViewById(R.id.noofdays);
        leaves_balance = (TextView) findViewById(R.id.leaves_balance);

        leave_layout = (RelativeLayout) findViewById(R.id.leave_list);
        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);
        alternate_layout = (RelativeLayout) findViewById(R.id.alternate_layout);

        down_arrow = (ImageView) findViewById(R.id.down_arrow);
        mattachment = (ImageView) findViewById(R.id.attactment);

//        comments = (EditText) findViewById(R.id.comments);
        contact_input = (EditText) findViewById(R.id.contact_input);
//        ticket_input = (EditText) findViewById(R.id.ticket_input);
//        destination_input = (EditText) findViewById(R.id.destination_input);
        notes_input = (EditText) findViewById(R.id.notes_input);
        alternate_employee_name = (EditText) findViewById(R.id.alternate_employee_name);

        re_entry = (RadioButton) findViewById(R.id.re_entry);

        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.sumbit);

        leave_request_id = (EditText) findViewById(R.id.edit_loanid);
        loanid_layout = (RelativeLayout) findViewById(R.id.loanid);

        hidden_layout.setVisibility(View.GONE);

        try {
            leaveRequestDetails = (ArrayList<LeaveRequestGetResponse>) getIntent().getSerializableExtra("list");
            leave_get_pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
            if (leaveRequestDetails.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }


        if (leave_request) {

            if (!medit) {

                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                leave_layout.setEnabled(false);
                startcalander.setEnabled(false);
                endcalendra.setEnabled(false);
//                comments.setFocusable(false);
                alternate_layout.setEnabled(false);
                contact_input.setFocusable(false);
                re_entry.setEnabled(false);
                notes_input.setFocusable(false);
                mattachment.setVisibility(View.GONE);

            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);
                mattachment.setVisibility(View.VISIBLE);

            }
            loanid_layout.setVisibility(View.VISIBLE);
            create.setText("SAVE");

            SimpleDateFormat editdateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
            SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

            Calendar calendar = Calendar.getInstance();

            Date date1 = null;

            try {
                date1 = editdateFormat.parse(leaveRequestDetails.get(leave_get_pos).getFROMDATE());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            calendar.setTime(date1);

            mDay = calendar.get(Calendar.DAY_OF_MONTH);
            mMonth = calendar.get(Calendar.MONTH);
            mYear = calendar.get(Calendar.YEAR);

            Date date = null;

            try {
                date = dateFormat1.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
                checkInDate = date;
                starting_date = date;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String date2, date3;

            date2 = postFormater.format(date);
            date3 = postFormater1.format(date);
            startdate.setText(date2);
            startdayyear.setText(date3);

            str_startdate = dateFormat.format(calendar.getTime());

            Calendar calendar1 = Calendar.getInstance();

            Date date6 = null;

            try {
                date6 = editdateFormat.parse(leaveRequestDetails.get(leave_get_pos).getTODATE());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            calendar1.setTime(date6);

            mendDay = calendar1.get(Calendar.DAY_OF_MONTH);
            mendMonth = calendar1.get(Calendar.MONTH);
            mendYear = calendar1.get(Calendar.YEAR);

            Date date7 = null;

            try {
                date7 = dateFormat1.parse(mendDay + "-" + (mendMonth + 1) + "-" + mendYear);
                checkoutDate = date7;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            str_enddate = dateFormat.format(date7);

            String date4, date5;

            date4 = postFormater.format(date7);
            date5 = postFormater1.format(date7);
            enddate.setText(date4);
            enddayyear.setText(date5);

            noofdays.setText("01");
            CalculateNumOfDays();


            // Setting Min Date to today date


            if (leaveRequestDetails.get(leave_get_pos).getPROVIDEVISA().equals("1")) {

                strprovide_visa = true;
                re_entry.setChecked(true);

            } else {

                strprovide_visa = false;
                re_entry.setChecked(false);

            }

            leaves_balance.setText("" + leaveRequestDetails.get(leave_get_pos).getLEAVEBALANCE());

            vactionet.setText("" + leaveRequestDetails.get(leave_get_pos).getLEAVEID());

            alternate_employee_name.setText("" + leaveRequestDetails.get(leave_get_pos).getALTERNATIVEEMPLOYEENAME());

            notes_input.setText("" + leaveRequestDetails.get(leave_get_pos).getNOTES());
            contact_input.setText("" + leaveRequestDetails.get(leave_get_pos).getCONTACTINFODURINGLEAVE());

            leave_request_id.setText("" + leaveRequestDetails.get(leave_get_pos).getLEAVEREQUESTID());


        } else {

            loanid_layout.setVisibility(View.GONE);

            create.setText("CREATE");
            create.setVisibility(View.VISIBLE);
//            submit.setVisibility(View.GONE);
            mattachment.setVisibility(View.GONE);

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
            SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

            Calendar calendar = Calendar.getInstance();

            mDay = calendar.get(Calendar.DAY_OF_MONTH);
            mMonth = calendar.get(Calendar.MONTH);
            mYear = calendar.get(Calendar.YEAR);

            Date date = null;

            try {
                date = dateFormat1.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
                checkInDate = date;
                checkoutDate = date;
                starting_date = date;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String date1, date2;

            date1 = postFormater.format(calendar.getTime());
            date2 = postFormater1.format(calendar.getTime());
            startdate.setText(date1);
            startdayyear.setText(date2);

            str_startdate = dateFormat.format(calendar.getTime());
            str_enddate = dateFormat.format(calendar.getTime());

            String date3, date4;

            date3 = postFormater.format(calendar.getTime());
            date4 = postFormater1.format(calendar.getTime());
            enddate.setText(date3);
            enddayyear.setText(date4);

            noofdays.setText("01");


            // Setting Min Date to today date
//            Calendar min_date_c = Calendar.getInstance();
//            min_date_c.setTime(starting_date);
//            // Setting Max Date to next 2 years
//            Calendar max_date_c = Calendar.getInstance();
//            max_date_c.setTime(starting_date);
//            max_date_c.add(Calendar.DAY_OF_MONTH, 15);
//
//            for (Calendar loopdate = min_date_c; min_date_c.before(max_date_c); min_date_c.add(Calendar.DAY_OF_MONTH, 1), loopdate = min_date_c) {
//                int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
//                if (dayOfWeek == Calendar.FRIDAY) {
//                    Calendar[] disabledDays = new Calendar[1];
//                    disabledDays[0] = loopdate;
//                    if (disabledDays != null) {
//
//                        pos = pos + 1;
//
//
//                    }
//                }
//                if (dayOfWeek == Calendar.SATURDAY) {
//                    Calendar[] disabledDays = new Calendar[1];
//                    disabledDays[0] = loopdate;
//                    if (disabledDays != null) {
//
//                        pos = pos + 1;
//
//
//                    }
//                }
//            }

        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidden_layout.setVisibility(View.GONE);

            }
        });

        mattachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(LeaveRequestActivity.this, LeaveAttachmentActivity.class);
                a.putExtra("requestid", leaveRequestDetails.get(leave_get_pos).getLEAVEREQUESTID());
                a.putExtra("requesttype", "LeaveRequest");
                startActivity(a);

            }
        });

        Log.i("TAG", "pos: " + pos);

//                Calendar max_date_c1 = Calendar.getInstance();
//                int dayOfWeek1 = max_date_c1.get(Calendar.DAY_OF_WEEK);
//                if (dayOfWeek1 != Calendar.FRIDAY || dayOfWeek1 != Calendar.SATURDAY) {
//                    // Setting Max Date to next 2 years
//                    max_date_c1.add(Calendar.DAY_OF_MONTH, 5);
//                    datePickerDialog.setMaxDate(max_date_c1);
//                }


        startcalander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                final Calendar c = Calendar.getInstance();
//                mYear = c.get(Calendar.YEAR);
//                mMonth = c.get(Calendar.MONTH);
//                mDay = c.get(Calendar.DAY_OF_MONTH);
                String str_vactionet = vactionet.getText().toString();
                if (str_vactionet.length() == 0) {

                    Constants.showOneButtonAlertDialog("Please select leave type", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);

                } else {

                    DecimalFormat decimalFormat = new DecimalFormat("0");
                    double leavebalance = Double.parseDouble(leaves_balance.getText().toString());
                    String str_vactionet1 = decimalFormat.format(leavebalance);

                    Calendar min_date_c1 = Calendar.getInstance();
                    min_date_c1.setTime(starting_date);
                    // Setting Max Date to next 2 years
                    Calendar max_date_c1 = Calendar.getInstance();
                    max_date_c1.setTime(starting_date);
                    max_date_c1.add(Calendar.DAY_OF_MONTH, Integer.parseInt(str_vactionet1) + 1);

                    for (Calendar loopdate = min_date_c1; min_date_c1.before(max_date_c1); min_date_c1.add(Calendar.DAY_OF_MONTH, 1), loopdate = min_date_c1) {
                        int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek == Calendar.FRIDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
                            if (disabledDays != null) {

                                pos = pos + 1;


                            }
                        }
                        if (dayOfWeek == Calendar.SATURDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
                            if (disabledDays != null) {

                                pos = pos + 1;


                            }
                        }
                    }


                    datePickerDialog = DatePickerDialog.newInstance(LeaveRequestActivity.this, mYear, mMonth, mDay);
                    datePickerDialog.setThemeDark(false);
                    datePickerDialog.showYearPickerFirst(false);
                    datePickerDialog.setTitle("Select Date");

                    // Setting Min Date to today date
                    Calendar min_date_c = Calendar.getInstance();
                    min_date_c.setTime(starting_date);
//                    datePickerDialog.setMinDate(min_date_c);

                    // Setting Max Date to next 2 years
                    Calendar max_date_c = Calendar.getInstance();
                    max_date_c.setTime(starting_date);
                    max_date_c.add(Calendar.DAY_OF_MONTH, (Integer.parseInt(str_vactionet1) + 1 + pos));
                    datePickerDialog.setMaxDate(max_date_c);

                    for (Calendar loopdate = min_date_c; min_date_c.before(max_date_c); min_date_c.add(Calendar.DAY_OF_MONTH, 1), loopdate = min_date_c) {
                        int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek == Calendar.FRIDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
//                        datePickerDialog.setDisabledDays(disabledDays);
                        }
                        if (dayOfWeek == Calendar.SATURDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
//                        datePickerDialog.setDisabledDays(disabledDays);
                        }
                    }
//                Calendar max_date_c1 = Calendar.getInstance();
//                int dayOfWeek1 = max_date_c1.get(Calendar.DAY_OF_WEEK);
//                if (dayOfWeek1 != Calendar.FRIDAY || dayOfWeek1 != Calendar.SATURDAY) {
//                    // Setting Max Date to next 2 years
//                    max_date_c1.add(Calendar.DAY_OF_MONTH, 5);
//                    datePickerDialog.setMaxDate(max_date_c1);
//                }

                    int pos1 = 0;
                    if (datePickerDialog.getDisabledDays() != null) {
                        for (int i = 0; i < datePickerDialog.getDisabledDays().length; i++) {


                            pos1 = pos1 + 1;

                        }

                        Log.i("TAG", "pos1: " + pos1);

                    }
//                Calendar max_date_c1 = Calendar.getInstance();
//                max_date_c1.add(Calendar.DAY_OF_MONTH, 5 + pos);
//                datePickerDialog.setMaxDate(max_date_c1);

//                for (int i = 0; i < disableDaysList.size(); i++) {
                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();
//                    max.add(Calendar.MONTH, 1);
//                    max.add(Calendar.DATE, 1);
                    Calendar loopdate = Calendar.getInstance();

//                    for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
//                int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
//                        if (dayOfWeek == disableDaysList.get(i).getWeekdayId()) {
//                Calendar[] disabledDays = new Calendar[1];
                    int saturday = loopdate.get(Calendar.SATURDAY);
                    int sunday = loopdate.get(Calendar.SUNDAY);


//                        }
//                    }
//                }

                    datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialogInterface) {

                        }
                    });

                    datePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
                    from = true;
                    to = false;

//                DatePickerDialog datePickerDialog = new DatePickerDialog(LeaveRequestActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//
//
//                            @Override
//                            public void onDateSet(DatePicker view, int year,
//                                                  int monthOfYear, int dayOfMonth) {
//                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
//                                SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
//                                SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);
//
//                                if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
//                                    isToday = true;
//
//                                } else {
//                                    isToday = false;
//                                }
//                                mYear = year;
//                                mDay = dayOfMonth;
//                                mMonth = monthOfYear;
//
//                                Date date = null;
//
//                                try {
//                                    date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
//                                    checkInDate = date;
//                                } catch (ParseException e) {
//                                    e.printStackTrace();
//                                }
//
//                                String date1, date2;
//
//                                date1 = postFormater.format(date);
//                                date2 = postFormater1.format(date);
//                                startdate.setText(date1);
//                                startdayyear.setText(date2);
//
//                            }
//                        }, mYear, mMonth, mDay);
//                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//                datePickerDialog.setTitle("Select Date");
//                datePickerDialog.show();

                }
            }
        });

        endcalendra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String str_vactionet = vactionet.getText().toString();
                if (str_vactionet.length() == 0) {

                    Constants.showOneButtonAlertDialog("Please select leave type", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);

                } else {
//                if (checkInDate == null) {
//
//                    Constants.showOneButtonAlertDialog("Please select From date", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);
//
//                } else {
                    DecimalFormat decimalFormat = new DecimalFormat("0");
                    double leavebalance = Double.parseDouble(leaves_balance.getText().toString());
                    String str_vactionet1 = decimalFormat.format(leavebalance);

                    Calendar min_date_c1 = Calendar.getInstance();
                    min_date_c1.setTime(checkInDate);
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    // Setting Max Date to next 2 years
                    Calendar max_date_c1 = Calendar.getInstance();
                    max_date_c1.setTime(starting_date);
                    max_date_c1.add(Calendar.DAY_OF_MONTH, Integer.parseInt(str_vactionet1) + 1);

                    for (Calendar loopdate = min_date_c1; min_date_c1.before(max_date_c1); min_date_c1.add(Calendar.DAY_OF_MONTH, 1), loopdate = min_date_c1) {
                        int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek == Calendar.FRIDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
                            if (disabledDays != null) {

                                endpos = endpos + 1;


                            }
                        }
                        if (dayOfWeek == Calendar.SATURDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
                            if (disabledDays != null) {

                                endpos = endpos + 1;


                            }
                        }
                    }
                    Log.i("TAG", "pos: " + endpos);

                    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

                    mendYear = mYear;
                    mendMonth = mMonth;
                    mendDay = mDay;

                    datePickerDialog = DatePickerDialog.newInstance(LeaveRequestActivity.this, mYear, mMonth, mDay);
                    datePickerDialog.setThemeDark(false);
                    datePickerDialog.showYearPickerFirst(false);
                    datePickerDialog.setTitle("Select Date");

                    // Setting Min Date to today date
                    Calendar min_date_c = Calendar.getInstance();
                    min_date_c.setTime(checkInDate);
                    datePickerDialog.setMinDate(min_date_c);

                    // Setting Max Date to next 2 years
                    Calendar max_date_c = Calendar.getInstance();
                    max_date_c.setTime(starting_date);
                    max_date_c.add(Calendar.DAY_OF_MONTH, (Integer.parseInt(str_vactionet1) + pos + 1));
                    datePickerDialog.setMaxDate(max_date_c);

                    for (Calendar loopdate = min_date_c; min_date_c.before(max_date_c); min_date_c.add(Calendar.DAY_OF_MONTH, 1), loopdate = min_date_c) {
                        int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek == Calendar.FRIDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
//                        datePickerDialog.setDisabledDays(disabledDays);
                        }
                        if (dayOfWeek == Calendar.SATURDAY) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
//                        datePickerDialog.setDisabledDays(disabledDays);
                        }
                    }
//                Calendar max_date_c1 = Calendar.getInstance();
//                int dayOfWeek1 = max_date_c1.get(Calendar.DAY_OF_WEEK);
//                if (dayOfWeek1 != Calendar.FRIDAY || dayOfWeek1 != Calendar.SATURDAY) {
//                    // Setting Max Date to next 2 years
//                    max_date_c1.add(Calendar.DAY_OF_MONTH, 5);
//                    datePickerDialog.setMaxDate(max_date_c1);
//                }

                    int pos1 = 0;
                    if (datePickerDialog.getDisabledDays() != null) {
                        for (int i = 0; i < datePickerDialog.getDisabledDays().length; i++) {


                            pos1 = pos1 + 1;

                        }

                        Log.i("TAG", "pos1: " + pos1);

                    }


//                for (int i = 0; i < disableDaysList.size(); i++) {
                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();
//                    max.add(Calendar.MONTH, 1);
//                    max.add(Calendar.DATE, 1);
                    Calendar loopdate = Calendar.getInstance();

//                    for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
//                int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
//                        if (dayOfWeek == disableDaysList.get(i).getWeekdayId()) {
//                Calendar[] disabledDays = new Calendar[1];
                    int saturday = loopdate.get(Calendar.SATURDAY);
                    int sunday = loopdate.get(Calendar.SUNDAY);


//                        }
//                    }
//                }

                    datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                        @Override
                        public void onCancel(DialogInterface dialogInterface) {

                        }
                    });

                    datePickerDialog.show(getSupportFragmentManager(), "DatePickerDialog");
                    from = false;
                    to = true;

//                    DatePickerDialog datePickerDialog = new DatePickerDialog(LeaveRequestActivity.this,
//                            new DatePickerDialog.OnDateSetListener() {
//
//
//                                @Override
//                                public void onDateSet(DatePicker view, int year,
//                                                      int monthOfYear, int dayOfMonth) {
//                                    SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
//                                    SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);
//
//                                    if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
//                                        isToday = true;
//
//                                    } else {
//                                        isToday = false;
//                                    }
//                                    mendYear = year;
//                                    mendDay = dayOfMonth;
//                                    mendMonth = monthOfYear;
//
//                                    Date date = null;
//
//                                    try {
//                                        date = dateFormat.parse(mendDay + "-" + (mendMonth + 1) + "-" + mendYear);
//                                        checkoutDate = date;
//                                    } catch (ParseException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    String date1, date2;
//
//                                    date1 = postFormater.format(date);
//                                    date2 = postFormater1.format(date);
//                                    enddate.setText(date1);
//                                    enddayyear.setText(date2);
//
//                                    CalculateNumOfDays();
//
//                                }
//                            }, mendYear, mendMonth, mendDay);
//
//                    Date date = null;
//
//                    try {
//                        date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                    Calendar calendar = Calendar.getInstance();
//                    calendar.setTime(checkInDate);
//                    datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
//                    datePickerDialog.setTitle("Select Date");
//                    datePickerDialog.show();

                }

            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new LeaveRequestApi().execute(Constants.getleave);
            new AlternateEmpApi().execute(Constants.getAlterateget);
            new EmpBalanceDetailsApi().execute(Constants.getLeaveBalance + "?_userid=" + userid);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

        alternate_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final BubbleLayout bubbleLayout;
//            if (language.equalsIgnoreCase("En")) {
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(LeaveRequestActivity.this).inflate(R.layout.branch_list_bubble, null);
//            } else {
//                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup_arabic, null);
//            }
                    final PopupWindow popupWindow = BubblePopupHelper.create(LeaveRequestActivity.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LeaveRequestActivity.this);
                    madapter = new AlternateEmpAdapter(LeaveRequestActivity.this, alternate, popupWindow);
                    mbubble_List.setAdapter(madapter);
                    int[] location = new int[2];
                    down_arrow.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(down_arrow, Gravity.NO_GRAVITY, down_arrow.getWidth() - 50, down_arrow.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        leave_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final BubbleLayout bubbleLayout;
//            if (language.equalsIgnoreCase("En")) {
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(LeaveRequestActivity.this).inflate(R.layout.branch_list_bubble, null);
//            } else {
//                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup_arabic, null);
//            }
                    final PopupWindow popupWindow = BubblePopupHelper.create(LeaveRequestActivity.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LeaveRequestActivity.this);
                    branchadapter = new BranchListAdapter(LeaveRequestActivity.this, leaveid, popupWindow);
                    mbubble_List.setAdapter(branchadapter);
                    int[] location = new int[2];
                    down_arrow.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(down_arrow, Gravity.NO_GRAVITY, down_arrow.getWidth() - 50, down_arrow.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        re_entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!strprovide_visa) {
                    re_entry.setChecked(true);
                    strprovide_visa = true;
                } else {
                    re_entry.setChecked(false);
                    strprovide_visa = false;
                }

            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
//                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (leave_request) {
//
//                            String service_url = Constants.getSubmit + "?TransactionID=" + leaveRequestDetails.get(leave_get_pos).getLEAVEREQUESTID() + "&UserId=" + userid + "&Note=";
//                            String main_url = service_url.replaceAll(" ", "%20");
//                            new SubmitRequestApi().execute(main_url);
//                            Log.i("TAG", "onClick: " + main_url);
//
//                        }
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//
//            }
//        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {

                            String service_url = Constants.getleaveupate + "?_dapLeaveId=" + str_leave_name + "&_fromDate=" + str_startdate + "&_toDate=" + str_enddate + "&_alternateEmpID=" + str_alternate_id + "&_contactInfo=" + str_contact + "&_Providevisa=" + strprovide_visa + "&_note=" + str_note + "&_comment=&_userid=" + userid + "&_dapLeaveRequestId=" + leaveRequestDetails.get(leave_get_pos).getLEAVEREQUESTID();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new LeaveRequestUpdateApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            String service_url = Constants.getleaveupate + "?_dapLeaveId=" + str_leave_name + "&_fromDate=" + str_startdate + "&_toDate=" + str_enddate + "&_alternateEmpID=" + str_alternate_id + "&_contactInfo=" + str_contact + "&_Providevisa=" + strprovide_visa + "&_note=" + str_note + "&_comment=&_userid=" + userid + "&_dapLeaveRequestId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new LeaveRequestUpdateApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

    }

    private boolean Validation() {

        str_leave_name = vactionet.getText().toString();
//        str_comment = comments.getText().toString();
        str_note = notes_input.getText().toString();
        str_contact = contact_input.getText().toString();
        str_alternate = alternate_employee_name.getText().toString();

        if (str_alternate.length() != 0) {
            for (int i = 0; i < employeeDetails.size(); i++) {
                if (str_alternate.equals(employeeDetails.get(i).getNAME())) {

                    str_alternate_id = employeeDetails.get(i).getPERSONNELNUMBER();

                }
            }
        }


        if (str_leave_name.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select Leave", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);

            return false;
        }
//        else if (str_comment.length() == 0) {
//
//            Constants.showOneButtonAlertDialog("Please enter comment", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);
//            comments.requestFocus();
//
//            return false;
//        }
        else if (str_alternate.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select overtimetypearry employee", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);

            return false;
        } else if (str_contact.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter contact During Leave", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);
            contact_input.requestFocus();

            return false;
        }
//        else if (str_contact.length() < 9) {
//
//            Constants.showOneButtonAlertDialog("Contact number invalid", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);
//            contact_input.requestFocus();
//
//            return false;
//        }
        else if (str_note.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter note", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);
            notes_input.requestFocus();

            return false;
        }
//        else if (leave_request) {
//
//            if (!attachment) {
//
//                Constants.showOneButtonAlertDialog("Please add attachment file", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), LeaveRequestActivity.this);
//
//                return false;
//            }
//
//        }


        return true;
    }

    public void initView() {

        for (int i = 0; i < getEmployeeBalanceDetails.size(); i++) {

            if (leaveid.get(leave_pos).equals(getEmployeeBalanceDetails.get(i).getLeaveTypeId())) {

                leaves_balance.setText("" + getEmployeeBalanceDetails.get(i).getTotalleaveBalance());
                Log.i("TAG", "initView1: " + getEmployeeBalanceDetails.get(i).getTotalleaveBalance());
            }
            Log.i("TAG", "initView2: " + getEmployeeBalanceDetails.get(i).getTotalleaveBalance());
        }
        Log.i("TAG", "initView3: " + getEmployeeBalanceDetails.get(0).getTotalleaveBalance());

        vactionet.setText("" + leaveid.get(leave_pos));

    }

    public void initView1() {

        alternate_employee_name.setText("" + alternate.get(alternate_pos));

    }

    public void CalculateNumOfDays() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(checkInDate);

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(checkoutDate);

        num_holidays = 0;
        for (Calendar loopdate = calendar; calendar.before(calendar1); calendar.add(Calendar.DATE, 1), loopdate = calendar) {
            int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == Calendar.FRIDAY) {
                Calendar[] disabledDays = new Calendar[1];
                disabledDays[0] = loopdate;
                num_holidays = num_holidays + 1;
            }
            if (dayOfWeek == Calendar.SATURDAY) {
                Calendar[] disabledDays = new Calendar[1];
                disabledDays[0] = loopdate;
                num_holidays = num_holidays + 1;
            }
        }
        Log.i("TAG", "CalculateNumOfDays1: " + num_holidays);
        long diff = checkoutDate.getTime() - checkInDate.getTime();
        long NumofDaysStr = (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        Log.i("TAG", "CalculateNumOfDays: " + NumofDaysStr);
        long final_num_of_days = (NumofDaysStr - num_holidays) + 1;
        Log.i("TAG", "final_num_of_days: " + final_num_of_days);
        if (final_num_of_days == 0) {
            noofdays.setText("01");
        } else {
            noofdays.setText("" + final_num_of_days);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM", Locale.US);
        SimpleDateFormat postFormater1 = new SimpleDateFormat("EEEE yyyy", Locale.US);

        if (from) {
            if (year == mYear && monthOfYear == mMonth && dayOfMonth == mDay) {
                isToday = true;

            } else {
                isToday = false;
            }
            mYear = year;
            mDay = dayOfMonth;
            mMonth = monthOfYear;

            Date date = null;

            try {
                date = dateFormat.parse(mDay + "-" + (mMonth + 1) + "-" + mYear);
                checkInDate = date;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            CalculateNumOfDays();

            if (checkoutDate != null) {

                Log.i("TAG", "date: " + dateFormat.format(date));
                Log.i("TAG", "checkout: " + dateFormat.format(checkoutDate));
                if (date.after(checkoutDate)) {

                    Log.i("TAG", "date1: " + dateFormat.format(date));
                    Log.i("TAG", "checkout1: " + dateFormat.format(checkoutDate));

                    String date1, date2;

                    date1 = postFormater.format(date);
                    date2 = postFormater1.format(date);
                    startdate.setText(date1);
                    startdayyear.setText(date2);
                    checkInDate = date;


                    String date3, date4;

                    date3 = postFormater.format(date);
                    date4 = postFormater1.format(date);
                    enddate.setText(date3);
                    enddayyear.setText(date4);
                    str_startdate = dateFormat1.format(date);
                    str_enddate = dateFormat1.format(date);
                    checkoutDate = date;


//                } else if (date.after(checkoutDate) || date.equals(checkoutDate)) {
                } else {

                    String date1, date2;

                    date1 = postFormater.format(date);
                    date2 = postFormater1.format(date);
                    startdate.setText(date1);
                    startdayyear.setText(date2);
                    str_startdate = dateFormat1.format(date);

                }

            } else {

                String date1, date2;

                date1 = postFormater.format(date);
                date2 = postFormater1.format(date);
                startdate.setText(date1);
                startdayyear.setText(date2);
                str_startdate = dateFormat1.format(date);

            }
        } else {


            if (year == mendYear && monthOfYear == mendMonth && dayOfMonth == mendDay) {
                isToday = true;

            } else {
                isToday = false;
            }
            mendYear = year;
            mendDay = dayOfMonth;
            mendMonth = monthOfYear;

            Date enddate1 = null;

            try {
                enddate1 = dateFormat.parse(mendDay + "-" + (mendMonth + 1) + "-" + mendYear);
                checkoutDate = enddate1;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String date3, date4;

            date3 = postFormater.format(enddate1);
            date4 = postFormater1.format(enddate1);
            enddate.setText(date3);
            enddayyear.setText(date4);
            str_enddate = dateFormat1.format(enddate1);

            CalculateNumOfDays();
        }

    }

    public class LeaveRequestUpdateApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response1;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
            dialog = new ACProgressFlower.Builder(LeaveRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response1 = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response1:" + response1);
                return response1;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LeaveRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String Message = "", MessageAr = "";
                                boolean status = false;
                                status = jo.getBoolean("Status");
                                Message = jo.getString("Message");
                                MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LeaveRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.mleave_submit = -1;
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LeaveRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class LeaveRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            leaveIdDetails.clear();
            leaveid.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
            dialog = new ACProgressFlower.Builder(LeaveRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LeaveRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetLeaveIdDetails");
                                LeaveRequestResponse.GetLeaveIdDetails leaveRequestGetResponse = new LeaveRequestResponse.GetLeaveIdDetails();
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    leaveRequestGetResponse.setLEAVEID(jo2.getString("LEAVEID"));
                                    leaveRequestGetResponse.setDESCRIPTION(jo2.getString("DESCRIPTION"));

                                    leaveIdDetails.add(leaveRequestGetResponse);
                                    leaveid.add(leaveIdDetails.get(i).getLEAVEID());

                                    Log.d("TAG", "onPostExecute: " + leaveIdDetails.get(i).getDESCRIPTION());

                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LeaveRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LeaveRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class AlternateEmpApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            employeeDetails.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
            dialog = new ACProgressFlower.Builder(LeaveRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LeaveRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("EmployeeDetails");

                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    AlternateEmpResponse.EmployeeDetails alternateemp = new AlternateEmpResponse.EmployeeDetails();
                                    alternateemp.setPERSONNELNUMBER(jo2.getString("PERSONNELNUMBER"));
                                    alternateemp.setNAME(jo2.getString("NAME"));

                                    employeeDetails.add(alternateemp);
                                    alternate.add(employeeDetails.get(i).getNAME());

                                    Log.d("TAG", "onPostExecute: " + employeeDetails.get(i).getNAME());

                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LeaveRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LeaveRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class EmpBalanceDetailsApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            getEmployeeBalanceDetails.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
            dialog = new ACProgressFlower.Builder(LeaveRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LeaveRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("GetEmployeeBalanceDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    LeaveBalanceResponse alternateemp = new LeaveBalanceResponse();
                                    alternateemp.setLeaveTypeId(jo2.getString("LeaveTypeId"));
                                    alternateemp.setPersonnalNumber(jo2.getString("PersonnalNumber"));
                                    alternateemp.setTotalleaveBalance(jo2.getString("TotalleaveBalance"));

                                    getEmployeeBalanceDetails.add(alternateemp);

                                    Log.d("TAG", "totalleavebalance: " + getEmployeeBalanceDetails.get(i).getTotalleaveBalance());

                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LeaveRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(LeaveRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
            dialog = new ACProgressFlower.Builder(LeaveRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(LeaveRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LeaveRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LeaveRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(LeaveRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


//    private void HRGeneralRequestApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(LeaveRequestActivity.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<LeaveRequestResponse> call = apiService.getleave();
//
//        call.enqueue(new Callback<LeaveRequestResponse>() {
//
//            public void onResponse(Call<LeaveRequestResponse> call, Response<LeaveRequestResponse> response) {
//                if (response.isSuccessful()) {
//                    LeaveRequestResponse registrationResponse = response.body();
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            Log.i("TAG", "onResponse: " + registrationResponse.getData().getGetLeaveIdDetails().get(0).getDESCRIPTION());
//
////                            personal_no.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getPersonalNumber());
////                            worker_name.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
////                            worker_name_ar.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerNameAR());
////                            job.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getJob());
////                            desc.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
////                            datetime.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getEmpstartdate());
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    if (dialog != null)
//                        dialog.dismiss();
//                    Toast.makeText(LeaveRequestActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            public void onFailure(Call<LeaveRequestResponse> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(LeaveRequestActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(LeaveRequestActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }
}
