package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cs.ess.Adapter.HRGeneralViewAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.HRGeneralDetailsResponse;
import com.cs.ess.Models.HRGeneralDocuResponse;
import com.cs.ess.Models.OverTimeViewResponse;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HRGeneralRequestActivtity1 extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    String userId;
    ImageView back_btn, plus;
    RecyclerView leave_list;
    HRGeneralViewAdapter leaveRequestAdapter;
    ArrayList<HRGeneralDetailsResponse> leaveRequestGetResponses = new ArrayList<>();
    ArrayList<HRGeneralDocuResponse> hrGeneralDocuResponses = new ArrayList<>();
    SharedPreferences userPrefs;
    SwipeRefreshLayout swipe_refresh;
    boolean approvel = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hr_general_request_activity1);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("UserId", "");

        try {
            approvel = getIntent().getBooleanExtra("approval",false);
        } catch (Exception e) {
            e.printStackTrace();
            approvel = false;
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
        plus = (ImageView) findViewById(R.id.pluse);
        leave_list = (RecyclerView) findViewById(R.id.leave_list);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HRGeneralRequestActivtity1.this, HRGeneralRequestActivity.class);
                startActivity(intent);
            }
        });

        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipe_refresh.setOnRefreshListener(HRGeneralRequestActivtity1.this);
        swipe_refresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

    private void loadRecyclerViewData() {

        String networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivtity1.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new HRGeneralDetailsApi().execute(Constants.getHRDetails);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRefresh() {

        loadRecyclerViewData();

    }

    public class HRGeneralRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
//        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            leaveRequestGetResponses.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivtity1.this);
//            dialog = new ACProgressFlower.Builder(HRGeneralRequestActivtity1.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HRGeneralRequestActivtity1.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HRGeneralRequestActivtity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                leaveRequestGetResponses.clear();
                                JSONArray ja = jo1.getJSONArray("GetHRGeneralRequestDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    HRGeneralDetailsResponse leaveRequestGetResponse = new HRGeneralDetailsResponse();
                                    leaveRequestGetResponse.setDAPDOCREQID(jo2.getString("DAPDOCREQID"));
                                    leaveRequestGetResponse.setDOCID(jo2.getString("DOCID"));
                                    leaveRequestGetResponse.setTRANSDATE(jo2.getString("TRANSDATE"));
                                    leaveRequestGetResponse.setCOMMENTS(jo2.getString("COMMENTS"));
                                    leaveRequestGetResponse.setDESCRIPTION(jo2.getString("DESCRIPTION"));
                                    leaveRequestGetResponse.setPersonnalNumber(jo2.getString("PersonnalNumber"));
                                    leaveRequestGetResponse.setWORKFLOWSTATE(jo2.getString("WORKFLOWSTATE"));
                                    leaveRequestGetResponse.setWORKFLOWSTATENUM(jo2.getString("WORKFLOWSTATENUM"));

                                    leaveRequestGetResponses.add(leaveRequestGetResponse);
                                    Log.i("TAG", "leaverequest: " + leaveRequestGetResponses.get(i).getDOCID());
                                }

                                for (int i = 0; i < leaveRequestGetResponses.size(); i++) {
                                    Log.i("TAG", "leaverequest: " + leaveRequestGetResponses.get(i).getDOCID());
                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HRGeneralRequestActivtity1.this);
                                leave_list.setLayoutManager(linearLayoutManager);
                                leaveRequestAdapter = new HRGeneralViewAdapter(HRGeneralRequestActivtity1.this, leaveRequestGetResponses, hrGeneralDocuResponses, userId, HRGeneralRequestActivtity1.this);
                                leave_list.setAdapter(leaveRequestAdapter);

                                swipe_refresh.setRefreshing(false);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HRGeneralRequestActivtity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

    public class HRGeneralDetailsApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
//        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {

            hrGeneralDocuResponses.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivtity1.this);
//            dialog = new ACProgressFlower.Builder(HRGeneralRequestActivtity1.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HRGeneralRequestActivtity1.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HRGeneralRequestActivtity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");

                                hrGeneralDocuResponses.clear();

                                JSONArray ja = jo1.getJSONArray("HRGeneralDocType");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    HRGeneralDocuResponse leaveRequestGetResponse = new HRGeneralDocuResponse();
                                    leaveRequestGetResponse.setDOCID(jo2.getString("DOCID"));
                                    leaveRequestGetResponse.setDOCTYPEID(jo2.getString("DOCTYPEID"));
                                    leaveRequestGetResponse.setDESCRIPTION(jo2.getString("DESCRIPTION"));

                                    hrGeneralDocuResponses.add(leaveRequestGetResponse);

                                }

                                new HRGeneralRequestApi().execute(Constants.getHRView + "?_userId=" + userId);

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HRGeneralRequestActivtity1.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(HRGeneralRequestActivtity1.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HRGeneralRequestActivtity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

//    private void HRGeneralRequestApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(OverTimeRequestActivtity1.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(OverTimeRequestActivtity1.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<LeaveRequestGetResponse> call = apiService.getleaveget(userId);
//
//        call.enqueue(new Callback<LeaveRequestGetResponse>() {
//
//            public void onResponse(Call<LeaveRequestGetResponse> call, Response<LeaveRequestGetResponse> response) {
//                if (response.isSuccessful()) {
//                    LeaveRequestGetResponse registrationResponse = response.body();
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OverTimeRequestActivtity1.this);
//                            linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                            leaveRequestAdapter = new LeaveRequestAdapter(OverTimeRequestActivtity1.this);
////                            leaveRequestAdapter = new LeaveRequestAdapter(OverTimeRequestActivtity1.this, registrationResponse.getData().getGETLeaveRequestDetails());
//                            leave_list.setAdapter(leaveRequestAdapter);
//
////                            personal_no.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getPersonalNumber());
////                            worker_name.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
////                            worker_name_ar.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerNameAR());
////                            job.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getJob());
////                            desc.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
////                            datetime.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getEmpstartdate());
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    if (dialog != null)
//                        dialog.dismiss();
//                    Toast.makeText(OverTimeRequestActivtity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            public void onFailure(Call<LeaveRequestGetResponse> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(OverTimeRequestActivtity1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(OverTimeRequestActivtity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }

    public void initView() {

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
//        String networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivtity1.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new HRGeneralRequestApi().execute(Constants.getHRView + "?_userId=" + userId);
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
        Log.i("TAG", "onResume: " + userId);
    }

}
