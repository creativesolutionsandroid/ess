package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.ess.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MSSOutStandingLoanActivity extends AppCompatActivity {

    ImageView backbtn;
    LinearLayout layout1,layout2, one;
    Button submit;
    TextView title;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mss_outstandingloan_activity);

        submit=(Button)findViewById(R.id.submit);
        layout1=(LinearLayout)findViewById(R.id.layout1);
        layout2=(LinearLayout) findViewById(R.id.layout2);

        title=(TextView) findViewById(R.id.title);
        title.setText("MSS Outstanding Loan Request");

        backbtn=(ImageView)findViewById(R.id.back_btn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MSSOutStandingLoanActivity.this,MSSOutStandingLoanReportActivity.class);
                startActivity(intent);
            }
        });
    }
}
