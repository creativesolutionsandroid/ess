package com.cs.ess.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.ess.Adapter.HRRequestTypeApdater;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.HRGeneralDetailsResponse;
import com.cs.ess.Models.HRGeneralDocuResponse;
import com.cs.ess.Models.LeaveRequestGetResponse;
import com.cs.ess.Models.OverTimeViewResponse;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HRGeneralRequestActivity extends AppCompatActivity {
    ImageView backbtn;
    EditText edit_id, edit_personalnumber, edit_workername, comment;
    TextView spinner_requesttype;
    Button create;
    SharedPreferences userPrefs;
    String userid;
    public static RelativeLayout hidden_layout;
    RelativeLayout request_id_layout;
    boolean leave_request = false, medit = false;
    ArrayList<String> requestTypelist = new ArrayList<>();
    public static int alternate_pos = -1;
    HRRequestTypeApdater branchadapter;
    String str_personal_no, str_requesttype, str_requesttype1, str_comment, str_date;

    ArrayList<HRGeneralDetailsResponse> leaveRequestDetails = new ArrayList<>();
    ArrayList<HRGeneralDocuResponse> hrGeneralDocuResponses = new ArrayList<>();
    int leave_get_pos;
    ImageView mattachment;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hr_genral_reaquest_activity);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userid = userPrefs.getString("UserId", "");

        backbtn = (ImageView) findViewById(R.id.back_btn);
        edit_id = (EditText) findViewById(R.id.edit_id);
        edit_personalnumber = (EditText) findViewById(R.id.edit_personalnumber);
        edit_workername = (EditText) findViewById(R.id.edit_workername);
        comment = (EditText) findViewById(R.id.comment_edit);

        spinner_requesttype = (TextView) findViewById(R.id.spinner_requesttype);

        create = (Button) findViewById(R.id.create);
//        submit = (Button) findViewById(R.id.submit);

        mattachment = (ImageView) findViewById(R.id.attactment);

        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);
        request_id_layout = (RelativeLayout) findViewById(R.id.request_id_layout);

        hidden_layout.setVisibility(View.GONE);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            leaveRequestDetails = (ArrayList<HRGeneralDetailsResponse>) getIntent().getSerializableExtra("list");
            leave_get_pos = getIntent().getIntExtra("pos", 0);
            medit = getIntent().getBooleanExtra("medit", false);
            if (leaveRequestDetails.size() == 0) {
                leave_request = false;
            } else {
                leave_request = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            leave_request = false;
        }


        if (leave_request) {

            request_id_layout.setVisibility(View.VISIBLE);

            if (!medit) {

                create.setVisibility(View.GONE);
//                submit.setVisibility(View.GONE);
                comment.setEnabled(false);
                spinner_requesttype.setEnabled(false);
//                edit_personalnumber.setEnabled(false);
                edit_id.setEnabled(false);
                mattachment.setVisibility(View.GONE);


            } else {

                create.setVisibility(View.VISIBLE);
//                submit.setVisibility(View.VISIBLE);
                mattachment.setVisibility(View.VISIBLE);

            }
            create.setText("SAVE");


            edit_id.setText("" + leaveRequestDetails.get(leave_get_pos).getDAPDOCREQID());
            edit_personalnumber.setText("" + leaveRequestDetails.get(leave_get_pos).getPersonnalNumber());
            comment.setText("" + leaveRequestDetails.get(leave_get_pos).getDESCRIPTION());


        } else {

            request_id_layout.setVisibility(View.GONE);

            create.setText("CREATE");
            create.setVisibility(View.VISIBLE);
//            submit.setVisibility(View.GONE);
            mattachment.setVisibility(View.GONE);

        }

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                comment.setFocusable(true);

            }
        });


        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hidden_layout.setVisibility(View.GONE);

            }
        });

        edit_workername.setText("" + userPrefs.getString("Empname", ""));
        edit_personalnumber.setText("" + userPrefs.getString("personalNo", ""));

        mattachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(HRGeneralRequestActivity.this, LeaveAttachmentActivity.class);
                a.putExtra("requestid", leaveRequestDetails.get(leave_get_pos).getDAPDOCREQID());
                a.putExtra("requesttype", "HRGeneralRequest");
                startActivity(a);

            }
        });

        String networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

            new HRGeneralDetailsApi().execute(Constants.getHRDetails);

        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

        spinner_requesttype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    final BubbleLayout bubbleLayout;
//            if (language.equalsIgnoreCase("En")) {
                    bubbleLayout = (BubbleLayout) LayoutInflater.from(HRGeneralRequestActivity.this).inflate(R.layout.branch_list_bubble, null);
//            } else {
//                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup_arabic, null);
//            }
                    final PopupWindow popupWindow = BubblePopupHelper.create(HRGeneralRequestActivity.this, bubbleLayout);
                    hidden_layout.setVisibility(View.VISIBLE);
                    ListView mbubble_List = (ListView) bubbleLayout.findViewById(R.id.branch_list);
                    branchadapter = new HRRequestTypeApdater(HRGeneralRequestActivity.this, requestTypelist, popupWindow);
                    mbubble_List.setAdapter(branchadapter);
                    int[] location = new int[2];
                    spinner_requesttype.getLocationOnScreen(location);
                    bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                    popupWindow.showAtLocation(spinner_requesttype, Gravity.NO_GRAVITY, location[0], spinner_requesttype.getHeight() + location[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Validation()) {
//
//                    String networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivity.this);
//                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (leave_request) {
//
//                            String service_url = Constants.getHRSubmit + "?RequestId=" + leaveRequestDetails.get(leave_get_pos).getDAPDOCREQID() + "&UserId=" + userid + "&Note=";
//                            String main_url = service_url.replaceAll(" ", "%20");
//                            new SubmitRequestApi().execute(main_url);
//                            Log.i("TAG", "onClick: " + main_url);
//
//                        }
//
//                    } else {
//                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//
//            }
//        });


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validation()) {

                    String networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (leave_request) {

                            String service_url = Constants.getHRCreate + "?_personnalNum=" + str_personal_no + "&_docId=" + str_requesttype + "&_description=" + str_comment + "&_transdate=" + str_date + "&_userid=" + userid + "&_dapDocReqId=" + leaveRequestDetails.get(leave_get_pos).getDAPDOCREQID();
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        } else {

                            String service_url = Constants.getHRCreate + "?_personnalNum=" + str_personal_no + "&_docId=" + str_requesttype + "&_description=" + str_comment + "&_transdate=" + str_date + "&_userid=" + userid + "&_dapDocReqId=";
                            String main_url = service_url.replaceAll("\\s+", "%20");
                            new CreatBalanceApi().execute(main_url);
                            Log.i("TAG", "onClick: " + main_url);

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

    }

    private boolean Validation() {

        String requestype = "";
        str_personal_no = edit_personalnumber.getText().toString();
        requestype = spinner_requesttype.getText().toString();
        str_comment = comment.getText().toString();
        str_requesttype1 = spinner_requesttype.getText().toString();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();

        str_date = dateFormat.format(calendar.getTime());

        Log.i("TAG", "Validation: " + requestype);
        Log.i("TAG", "Validation1: " + hrGeneralDocuResponses.size());
        if (requestype.length() > 0) {

            for (int i = 0; i < hrGeneralDocuResponses.size(); i++) {
                Log.i("TAG", "req: " + hrGeneralDocuResponses.get(i).getDESCRIPTION());

                if (requestype.equalsIgnoreCase(hrGeneralDocuResponses.get(i).getDESCRIPTION())) {

                    str_requesttype = hrGeneralDocuResponses.get(i).getDOCID();
                    Log.i("TAG", "Validation: " + str_requesttype);

                }
            }

        }

//        str_cycle_pay = edit_cyclepayament.getText().toString();


//        if (str_personal_no.length() == 0) {
//
//            Constants.showOneButtonAlertDialog("Please enter personalno", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HRGeneralRequestActivity.this);
//
//            return false;
//        } else 
        if (str_requesttype1.length() == 0) {

            Constants.showOneButtonAlertDialog("Please select requestType", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HRGeneralRequestActivity.this);


            return false;
        } else if (str_comment.length() == 0) {

            Constants.showOneButtonAlertDialog("Please enter comment", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), HRGeneralRequestActivity.this);

            comment.requestFocus();
            return false;
        }


        return true;
    }

    public void initView1() {

        spinner_requesttype.setText("" + requestTypelist.get(alternate_pos));

    }

    public class CreatBalanceApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivity.this);
            dialog = new ACProgressFlower.Builder(HRGeneralRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HRGeneralRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HRGeneralRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HRGeneralRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Constants.mhr_submit = -1;
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HRGeneralRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

//                                Log.d("TAG", "loansize: " + loantype.size());

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HRGeneralRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(HRGeneralRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HRGeneralRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class SubmitRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivity.this);
            dialog = new ACProgressFlower.Builder(HRGeneralRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HRGeneralRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HRGeneralRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {

                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");

                                if (status) {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HRGeneralRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();
                                            finish();
                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                } else {

                                    AlertDialog customDialog = null;
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(HRGeneralRequestActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);


                                    no.setVisibility(View.GONE);

                                    title.setText(getResources().getString(R.string.app_name));
                                    yes.setText(getResources().getString(R.string.ok));
                                    desc.setText(Message);

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();

                                    final AlertDialog finalCustomDialog = customDialog;
                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finalCustomDialog.dismiss();

                                        }
                                    });

                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);

                                }

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HRGeneralRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class HRGeneralDetailsApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
        ACProgressFlower dialog = null;

        @Override
        protected void onPreExecute() {
            requestTypelist.clear();
            hrGeneralDocuResponses.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(HRGeneralRequestActivity.this);
            dialog = new ACProgressFlower.Builder(HRGeneralRequestActivity.this)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .fadeColor(Color.DKGRAY).build();

            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HRGeneralRequestActivity.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HRGeneralRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String workerName = "", description = "", personalNumber = "", workerNameAR = "", mjob = "", empstartdate = "";
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                JSONArray ja = jo1.getJSONArray("HRGeneralDocType");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    HRGeneralDocuResponse leaveRequestGetResponse = new HRGeneralDocuResponse();
                                    leaveRequestGetResponse.setDOCID(jo2.getString("DOCID"));
                                    leaveRequestGetResponse.setDOCTYPEID(jo2.getString("DOCTYPEID"));
                                    leaveRequestGetResponse.setDESCRIPTION(jo2.getString("DESCRIPTION"));

                                    hrGeneralDocuResponses.add(leaveRequestGetResponse);
                                    requestTypelist.add(hrGeneralDocuResponses.get(i).getDESCRIPTION());

                                    Log.d("TAG", "onPostExecute: " + hrGeneralDocuResponses.get(i).getDESCRIPTION());

                                }

                                if (leave_request) {

                                    for (int i = 0; i < hrGeneralDocuResponses.size(); i++) {

                                        if (leaveRequestDetails.get(leave_get_pos).getDOCID().equals(hrGeneralDocuResponses.get(i).getDOCID())) {

                                            spinner_requesttype.setText("" + hrGeneralDocuResponses.get(i).getDESCRIPTION());

                                        }

                                    }

                                }
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HRGeneralRequestActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                                leave_list.setLayoutManager(linearLayoutManager);
//                                leaveRequestAdapter = new LeaveRequestAdapter(HRGeneralRequestActivity.this, leaveRequestGetResponses);
//                                leave_list.setAdapter(leaveRequestAdapter);

                            } catch (JSONException je) {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HRGeneralRequestActivity.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}


