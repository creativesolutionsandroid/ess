package com.cs.ess.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

//import com.cs.ess.Adapter.ClearanceDetailsAdapter;
import com.cs.ess.Adapter.ClearanceDetailsAdapter;
import com.cs.ess.JSONParser;
//import com.cs.ess.Models.ClearanceDetailsResponce;
import com.cs.ess.Models.ClearanceDetailsResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ClearanceActivity1 extends Activity implements SwipeRefreshLayout.OnRefreshListener{

    String userId;
    ImageView back_btn;
    RecyclerView retun_list;

    ArrayList<ClearanceDetailsResponce> cleranceDetailsarray = new ArrayList<>();
    SharedPreferences userPrefs;
    public ClearanceDetailsAdapter madapter;
    SwipeRefreshLayout swipe_refresh;
    boolean approvel = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clearaance_screen1);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("UserId", "");

        try {
            approvel = getIntent().getBooleanExtra("approval",false);
        } catch (Exception e) {
            e.printStackTrace();
            approvel = false;
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
//        plus = (ImageView) findViewById(R.id.pluse);
        retun_list = (RecyclerView) findViewById(R.id.leave_list);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        plus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(ClearanceActivity1.this, BusinessRetunActivity2.class);
//                startActivity(intent);
//            }
//        });

        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipe_refresh.setOnRefreshListener(ClearanceActivity1.this);
        swipe_refresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

    private void loadRecyclerViewData() {

        String networkStatus = NetworkUtil.getConnectivityStatusString(ClearanceActivity1.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new RetunRequestApi().execute(Constants.getclearancedetails + "?UserId=" + userId);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRefresh() {

        loadRecyclerViewData();

    }


    public class RetunRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
//        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            cleranceDetailsarray.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(ClearanceActivity1.this);
//            dialog = new ACProgressFlower.Builder(ClearanceActivity1.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ClearanceActivity1.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(ClearanceActivity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");

                                cleranceDetailsarray.clear();

                                JSONArray ja = jo1.getJSONArray("GetClearanceRequestDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    ClearanceDetailsResponce ClearanceDetailsResponce = new ClearanceDetailsResponce();
                                    ClearanceDetailsResponce.setEOSTRANSID(jo2.getString("EOSTRANSID"));
                                    ClearanceDetailsResponce.setTRANSDATE(jo2.getString("TRANSDATE"));
                                    ClearanceDetailsResponce.setWORKFLOWSTATE(jo2.getString("WORKFLOWSTATE"));
                                    ClearanceDetailsResponce.setEOSTRANSTYPE(jo2.getString("EOSTRANSTYPE"));
                                    ClearanceDetailsResponce.setPOSTED(jo2.getString("POSTED"));
                                    ClearanceDetailsResponce.setPAID(jo2.getString("PAID"));
                                    ClearanceDetailsResponce.setEOSCOMPENSATION(jo2.getString("EOSCOMPENSATION"));
                                    ClearanceDetailsResponce.setTICKETPAYMENTMETHOD(jo2.getString("TICKETPAYMENTMETHOD"));
                                    ClearanceDetailsResponce.setTXLADVANCEHOUSING(jo2.getString("TXLADVANCEHOUSING"));
                                    ClearanceDetailsResponce.setTXLEXPENSES(jo2.getString("TXLEXPENSES"));
                                    ClearanceDetailsResponce.setTXLGRATUITY(jo2.getString("TXLGRATUITY"));
                                    ClearanceDetailsResponce.setUNPAIDLASTWORKINGDAYS(jo2.getString("UNPAIDLASTWORKINGDAYS"));
                                    ClearanceDetailsResponce.setUNPAIDLEAVENUMBEROFDAYS(jo2.getString("UNPAIDLEAVENUMBEROFDAYS"));
                                    ClearanceDetailsResponce.setUNPAIDLOANS(jo2.getString("UNPAIDLOANS"));
                                    ClearanceDetailsResponce.setUNPAIDSALARIES(jo2.getString("UNPAIDSALARIES"));
                                    ClearanceDetailsResponce.setUNPAIDNOTICEPERIOD(jo2.getString("UNPAIDNOTICEPERIOD"));
                                    ClearanceDetailsResponce.setUNPAIDTICKETS(jo2.getString("UNPAIDTICKETS"));
                                    ClearanceDetailsResponce.setUNPAIDTICKETSAMOUNT(jo2.getString("UNPAIDTICKETSAMOUNT"));
                                    ClearanceDetailsResponce.setUNPAIDVACATIONS(jo2.getString("UNPAIDVACATIONS"));
                                    ClearanceDetailsResponce.setWORKFLOWSTATENUM(jo2.getString("WORKFLOWSTATENUM"));
                                    ClearanceDetailsResponce.setDATAAREAID(jo2.getString("DATAAREAID"));
                                    ClearanceDetailsResponce.setPERSONNELNUMBER(jo2.getString("PERSONNELNUMBER"));
                                    ClearanceDetailsResponce.setNAME(jo2.getString("NAME"));
                                    ClearanceDetailsResponce.setARABICNAME(jo2.getString("ARABICNAME"));
                                    ClearanceDetailsResponce.setNOTES(jo2.getString("NOTES"));
//
                                    cleranceDetailsarray.add(ClearanceDetailsResponce);
                                }
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ClearanceActivity1.this);
                                retun_list.setLayoutManager(linearLayoutManager);
                                madapter = new ClearanceDetailsAdapter(ClearanceActivity1.this, cleranceDetailsarray, ClearanceActivity1.this, userId);
                                retun_list.setAdapter(madapter);

                                swipe_refresh.setRefreshing(false);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(ClearanceActivity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }
    @Override
    protected void onResume() {
        super.onResume();
//        String networkStatus = NetworkUtil.getConnectivityStatusString(ClearanceActivity1.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new RetunRequestApi().execute(Constants.getclearancedetails + "?UserId=" + userId);
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }
        Log.i("TAG", "onResume: " + userId);

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
    }
}



