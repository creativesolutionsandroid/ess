package com.cs.ess.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cs.ess.Adapter.HourlyDetailsAdapter;
import com.cs.ess.Adapter.LeaveRequestAdapter;
import com.cs.ess.JSONParser;
import com.cs.ess.Models.HourlyDetailsResponce;
import com.cs.ess.Models.LeaveRequestGetResponse;
import com.cs.ess.Models.LoanRequestResponce;
import com.cs.ess.R;
import com.cs.ess.Utils.Constants;
import com.cs.ess.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HourlyLeaveRequestActivity1 extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    String userId;
    ImageView back_btn, plus;
    RecyclerView leave_list;
    HourlyDetailsAdapter hourlydetailsAdapter;
    ArrayList<HourlyDetailsResponce> HourlyDetailsResponces = new ArrayList<>();
    SharedPreferences userPrefs;
    SwipeRefreshLayout swipe_refresh;
    boolean approvel = false;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hourly_request1);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("UserId", "");

        back_btn = (ImageView) findViewById(R.id.back_btn);
        plus = (ImageView) findViewById(R.id.pluse);
        leave_list = (RecyclerView) findViewById(R.id.leave_list);

        try {
            approvel = getIntent().getBooleanExtra("approval",false);
        } catch (Exception e) {
            e.printStackTrace();
            approvel = false;
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HourlyLeaveRequestActivity1.this, HourlyLeaveRequestActivity2.class);
                startActivity(intent);
            }
        });

        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipe_refresh.setOnRefreshListener(HourlyLeaveRequestActivity1.this);
        swipe_refresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();

            }
        });

    }

    private void loadRecyclerViewData() {

        String networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity1.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new LeaveRequestApi().execute(Constants.gethourlydetails + "?UserId=" + userId);
        } else {
            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRefresh() {

        loadRecyclerViewData();

    }

    public class LeaveRequestApi extends AsyncTask<String, Integer, String> {

        String networkStatus, response;
//        ACProgressFlower dialog = null;


        @Override
        protected void onPreExecute() {
            HourlyDetailsResponces.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity1.this);
//            dialog = new ACProgressFlower.Builder(HourlyLeaveRequestActivity1.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//
//            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(HourlyLeaveRequestActivity1.this, "Connection error! please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(HourlyLeaveRequestActivity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.i("TAG", "onPostExecute: ");
                        try {
                            JSONObject jo = new JSONObject(result);


                            try {
                                boolean status = jo.getBoolean("Status");
                                String Message = jo.getString("Message");
                                String MessageAr = jo.getString("MessageAr");
                                JSONObject jo1 = jo.getJSONObject("Data");
                                HourlyDetailsResponces.clear();
                                JSONArray ja = jo1.getJSONArray("GetHourlyLeaveRequestDetails");
                                for (int i = 0; i < ja.length(); i++) {
                                    JSONObject jo2 = ja.getJSONObject(i);
                                    HourlyDetailsResponce HourlyDetailsResponce = new HourlyDetailsResponce();
                                    HourlyDetailsResponce.setDAPABSENCETRANS(jo2.getString("DAPABSENCETRANS"));
                                    HourlyDetailsResponce.setDAPABSENCETRANSTYPE(jo2.getString("DAPABSENCETRANSTYPE"));
                                    HourlyDetailsResponce.setTRANSDATE(jo2.getString("TRANSDATE"));
                                    HourlyDetailsResponce.setFROMTIME(jo2.getString("FROMTIME"));
                                    HourlyDetailsResponce.setTOTIME(jo2.getString("TOTIME"));
                                    HourlyDetailsResponce.setNUMOFHOURS(jo2.getString("NUMOFHOURS"));
                                    HourlyDetailsResponce.setTXLLEAVEBALANCEREAL(jo2.getString("TXLLEAVEBALANCEREAL"));
                                    HourlyDetailsResponce.setWORKFLOWSTATE(jo2.getString("WORKFLOWSTATE"));
                                    HourlyDetailsResponce.setNAME(jo2.getString("NAME"));
                                    HourlyDetailsResponce.setDATAAREAID(jo2.getString("DATAAREAID"));
                                    HourlyDetailsResponce.setWORKFLOWSTATENUM(jo2.getString("WORKFLOWSTATENUM"));
                                    HourlyDetailsResponce.setPERMISSIONLEAVETYPE(jo2.getString("PERMISSIONLEAVETYPE"));
                                    HourlyDetailsResponce.setDESCRIPTION(jo2.getString("DESCRIPTION"));


                                    HourlyDetailsResponces.add(HourlyDetailsResponce);
                                }

                                for (int i = 0; i < HourlyDetailsResponces.size(); i++) {
                                }

                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HourlyLeaveRequestActivity1.this);
                                leave_list.setLayoutManager(linearLayoutManager);
                                hourlydetailsAdapter = new HourlyDetailsAdapter(HourlyLeaveRequestActivity1.this, HourlyDetailsResponces, HourlyLeaveRequestActivity1.this, userId);
                                leave_list.setAdapter(hourlydetailsAdapter);

                                swipe_refresh.setRefreshing(false);

                            } catch (JSONException je) {
                                je.printStackTrace();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(HourlyLeaveRequestActivity1.this, "Cannot reach server", Toast.LENGTH_SHORT).show();
            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }

            super.onPostExecute(result);

        }

    }

//    private void HRGeneralRequestApi() {
//        final ACProgressFlower dialog = new ACProgressFlower.Builder(LeaveRequestActivity1.this)
//                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                .themeColor(Color.WHITE)
//                .fadeColor(Color.DKGRAY).build();
//
//        dialog.show();
//        final String networkStatus = NetworkUtil.getConnectivityStatusString(LeaveRequestActivity1.this);
//
//        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//        Call<LeaveRequestGetResponse> call = apiService.getleaveget(userId);
//
//        call.enqueue(new Callback<LeaveRequestGetResponse>() {
//
//            public void onResponse(Call<LeaveRequestGetResponse> call, Response<LeaveRequestGetResponse> response) {
//                if (response.isSuccessful()) {
//                    LeaveRequestGetResponse registrationResponse = response.body();
//                    try {
//                        //status true case
//                        if (registrationResponse.getMessage().equals("Success")) {
//
//                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LeaveRequestActivity1.this);
//                            linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
//                            hourlydetailsAdapter = new LeaveRequestAdapter(LeaveRequestActivity1.this);
////                            hourlydetailsAdapter = new LeaveRequestAdapter(LeaveRequestActivity1.this, registrationResponse.getData().getGETLeaveRequestDetails());
//                            leave_list.setAdapter(hourlydetailsAdapter);
//
////                            personal_no.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getPersonalNumber());
////                            worker_name.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerName());
////                            worker_name_ar.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getWorkerNameAR());
////                            job.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getJob());
////                            desc.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getDescription());
////                            datetime.setText("" + registrationResponse.getData().getGetPersonalInfo().get(0).getEmpstartdate());
//
//                        } else {
//
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    if (dialog != null)
//                        dialog.dismiss();
//                } else {
//                    try {
//                        Log.d("TAG", "onResponse: " + response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    if (dialog != null)
//                        dialog.dismiss();
//                    Toast.makeText(LeaveRequestActivity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            public void onFailure(Call<LeaveRequestGetResponse> call, Throwable t) {
//                Log.d("TAG", "onFailure: " + t.getMessage());
//                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    Toast.makeText(LeaveRequestActivity1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(LeaveRequestActivity1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                }
//
//                if (dialog != null)
//                    dialog.dismiss();
//            }
//        });
//    }

    public void initView() {

        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
//        String networkStatus = NetworkUtil.getConnectivityStatusString(HourlyLeaveRequestActivity1.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//            new LeaveRequestApi().execute(Constants.gethourlydetails + "?UserId=" + userId);
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//        }
        swipe_refresh.post(new Runnable() {

            @Override
            public void run() {

                swipe_refresh.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
        Log.i("TAG", "onResume: " + userId);
    }

}
