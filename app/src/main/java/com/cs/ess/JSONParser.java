package com.cs.ess;

import android.util.Base64;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JSONParser {

	static InputStream is = null;
	static JSONObject jObj = null;
	static JSONArray jArray = null;
	static String json = "";
	StringBuilder sb;

	// constructor
	public JSONParser() {

	}

	public String getJSONFromUrl(String url) {
		System.out.println(url);

		String username = "admin";
		String password = "admin";

		String unp = username+":"+password;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			// HttpPost httppost = new HttpPost(domain.trim()+"/qhms/wse_hmlogin.php?loginid="+user.trim()+"&logpass="+pass.trim());
			// HttpPost httppost = new HttpPost(domain+"/qhms/wse_hmlogin.php?loginid="+user+"&logpass="+pass);
			// HttpPost httppost = new HttpPost("http://svv.in.net/service/index.php?user=Customer&pass=cus&des=rtr%20r%20rtr%20trtrt");
			HttpGet httppost;
			httppost = new HttpGet(url);
			// httppost.setHeader( "Authorization","Basic "+"admin:admin");
			String encoded_login = Base64.encodeToString(unp.getBytes(), Base64.NO_WRAP);
			httppost.setHeader(new BasicHeader("Authorization", "Basic "+encoded_login));

			System.out.println("URL="+httppost.toString());

			// HttpPost httppost = new HttpPost("http://quantumr.info/qrms/call_service/wse_getitem.php?compkey=astres1312");
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection" + e.toString());
		}
		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-16LE"), 8);
			sb = new StringBuilder();
			sb.append(reader.readLine() + "\n");

			String line = "0";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}
		return json;
	}

//	public String getJSONFromUrl(String url1) {
//
//		// Making HTTP request
//		try {
//			URL url = new URL(url1);
//			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//			is = new BufferedInputStream(urlConnection.getInputStream());
//
//			BufferedReader reader = new BufferedReader(new InputStreamReader(
//					is, "UTF-8"), 8);
//			StringBuilder sb = new StringBuilder();
//			String line = null;
//			while ((line = reader.readLine()) != null) {
//				sb.append(line + "\n");
//			}
//			is.close();
//			json = sb.toString();
//		} catch (Exception e) {
//			Log.e("Buffer Error", "Error converting result " + e.toString());
//		}
//
//		/*// try parse the string to a JSON object
//		try {
//			jObj = new JSONObject(json);
//		} catch (JSONException e) {
//			Log.e("JSON Parser", "Error parsing data " + e.toString());
//			try {
//				jArray = new JSONArray(json);
//			} catch (JSONException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		}*/
//
//		// return JSON String
//		return json;
//
//	}
}
